import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import warships.Warship;
import warships.WarshipStaticMethods;

// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #3  Part 2
// Due Date: 22nd of March 2013
//--------------------------------------------------------------------

/**
 * Mission Control class that will allow our main method to send ships out
 * to new missions
 * 
 * @author Amit
 *
 */
public class WarshipMissionControl {

	/**
	 * Main Method in our Mission Control Class
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner kb = new Scanner(System.in); //for our user prompts
		
		// welcome message
		System.out.println("*********************************************");
		System.out.println("*        Welcome to the Mission Control!    *");
		System.out.println("*              Programmed by                *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();
		
		
		//our Sorted File:
		File sortedFile = new File("Sorted_Warship_Info.txt");
		
		//add new records to this file
		WarshipStaticMethods.addRecords(sortedFile);
		
		//Let's send our file to  display method to see the newly added content of the file
		WarshipStaticMethods.displayBufferedFile(sortedFile);
		
		//now we will save our file in a temporary array of warship objects
		//how many records does it have?
		int nbRecords = WarshipStaticMethods.getNumberOfRecords(sortedFile);
		Warship[] navalForce = WarshipStaticMethods.createFillArray(sortedFile, nbRecords);
		
		//ask user for one of the serial numbers and carry out a binary search and a Sequential search to compare
		
		boolean done = false; //flag used below for while loop
		long searchKey = 0;  //the search key user will provide
		int[] searchResult = new int[2]; //to catch our search result
		
		//let's get the key in a try-catch block to catch any input mismatch mistakes
		while (!done){ //loop will help if there's an input mismatch exceptiony
			try{
				System.out.println("\nEnter the ship's serial number that you want to recall: ");
				searchKey = kb.nextLong();
				done = true; //break out of the asking loop
			} catch (InputMismatchException e){
				kb.nextLine(); //discard all the end of line scanner junkn
				System.out.println("\nThere was a problem capturing the number, please make sure " +
						" that you are entering a serial number. Try again.");
			}
		}
		//binary search
		searchResult = WarshipStaticMethods.binarySearch(navalForce, searchKey);
		
		if (searchResult[0] == -1){
			System.out.println("\nWe conducted the search using binary search method and we had to do " + searchResult[1] + " iterations.");
			System.out.println("Sorry, no ship matching that serial number was located.");
			
		} else {
			System.out.println("\nWe found a ship matching the serial number you entered.");
			System.out.println("The search was conducted using binary method and we had to do " + searchResult[1] + " iterations.");
			System.out.println("Here is the information pertaining to this ship:");
			System.out.println(navalForce[searchResult[0]]);	
		} //close if-else
		
		//sequential search
		searchResult = WarshipStaticMethods.sequentialSearch(navalForce, searchKey);
		
		if (searchResult[0] == -1){
			System.out.println("\nWe also conducted the search using sequential method and we had to do " + searchResult[1] + " iterations.");
			System.out.println("Sorry, no ship matching that serial number was located.");
			
		} else {
			System.out.println("\nWe found a ship matching the serial number you entered.");
			System.out.println("The search was conducted using sequential method and we had to do " + searchResult[1] + " iterations.");
			System.out.println("Here is the information pertaining to this ship:");
			System.out.println(navalForce[searchResult[0]]);	
		} //close if-else
		
		//break-time
		System.out.println("\nPress enter to continue with the comparitive analysis of Binary vs. Sequential search.");
		try {
		System.in.read();
		} catch (IOException e){
			e.printStackTrace();
		}
		
		//let's find best and worst case scenarios for the binary search
		//we will create a small table and do a search with terms located on every single index
		int[][] bestWorstSearch = new int[navalForce.length + 1][2];
		
		
		for (int i = 0; i < navalForce.length; i++){
			searchResult = WarshipStaticMethods.binarySearch(navalForce, navalForce[i].getSerialNumber());
			bestWorstSearch[i][0] = searchResult[1];
		}
		//one search with a record we know doesn't exist
		searchResult = WarshipStaticMethods.binarySearch(navalForce,000000000);
		bestWorstSearch[navalForce.length][0] = searchResult[1];
		
		//now for comparison purposes, let's do a sequential search on each index as well
		for (int i = 0; i < navalForce.length; i++){
			searchResult = WarshipStaticMethods.sequentialSearch(navalForce, navalForce[i].getSerialNumber());
			bestWorstSearch[i][1] = searchResult[1];
		}
		//one search with a record we know doesn't exist
		searchResult = WarshipStaticMethods.sequentialSearch(navalForce, 000000000);
		bestWorstSearch[navalForce.length][1] = searchResult[1];
		
		//now we have everything in a table for each index number let's display our findings
		System.out.println("\n=====================================================================================");
		System.out.println("To measure the effectiveness of Binary vs. Sequential Search, we did a little research.");
		System.out.println("The results below show the index number in the first column followed by the number of iterations" +
							"\nit took Binary search and Sequential search to find the serial number located at that specific" +
							"\nindex. The final index in the table shows the number of iterations needed to do a search when " +
							"\nthe search term is not found.");
		System.out.println("\nIndex\tBinary\tSequential");
		for (int i = 0; i < bestWorstSearch.length; i++){
			System.out.print(i +"\t");
			for (int j = 0; j < bestWorstSearch[i].length; j++){
				System.out.print(bestWorstSearch[i][j] + "\t");
			}
			System.out.print("\n");
		}
		
		//let's analyze the results
		//we will use a few variables and save max/min itirations for both search types and the indices 
		//at which they occur
		int binaryMaxIt = -1, binaryMinIt = -1, seqMaxIt = -1, seqMinIt = -1;
		int indexBinaryMaxIt = 0, indexBinaryMinIt=0, indexSeqMaxIt = 0, indexSeqMinIt = 0;
		for (int i = 0; i <bestWorstSearch.length-1; i++){ //don't search in last index, it's our "not found" case
			if (binaryMaxIt == -1 || binaryMaxIt < bestWorstSearch[i][0]){
				binaryMaxIt = bestWorstSearch[i][0];
				indexBinaryMaxIt = i;
			}
			if (binaryMinIt == -1 || binaryMinIt > bestWorstSearch[i][0]){
				binaryMinIt = bestWorstSearch[i][0];
				indexBinaryMinIt = i;
			}
			if (seqMaxIt == -1 || seqMaxIt < bestWorstSearch[i][1]){
				seqMaxIt = bestWorstSearch[i][1];
				indexSeqMaxIt = i;
			}
			if (seqMinIt == -1 || seqMinIt > bestWorstSearch[i][1]){
				seqMinIt = bestWorstSearch[i][1];
				indexSeqMinIt = i;
			}
		}
		System.out.println("Our analysis show that in an array of size " + navalForce.length + " the binary search methods takes " +
				"\na maximum of " + binaryMaxIt + " iterations to find a search term (for example for a term located  at index location " +
				 indexBinaryMaxIt + "). \nWhereas the minimmum itirations take place when the term is located at index location " +
				 indexBinaryMinIt + " \nwhich is carried out using only " + binaryMinIt + " iterations. " +
				 "\nIn contrast, a sequential search will take the max iterations that is " + seqMaxIt + " " +
				 "\nfor a search term located at index " + indexSeqMaxIt + " " + " and the least iterations (" + seqMinIt + ") " +
				 "\nwhen the search term is located at " + indexSeqMinIt + ". " + "It is obvious from the tabled results " +
				 "\nthat Binary search is much more efficient then the sequential search. Especially when the search term is not " +
				 "\nfound the binary method uses " + bestWorstSearch[bestWorstSearch.length-1][0] + " iterations while the Sequential " +
				 "search uses up to "+ bestWorstSearch[bestWorstSearch.length-1][1] + " iterations.");
		
		
		//finally we will write our array to a binary file
		try {
			System.out.println("\nWriting the Warship data to a binary file: Warships.dat");
			WarshipStaticMethods.exportBinary(navalForce);
			System.out.println("Data exported.");
		} catch (IOException e){
			e.printStackTrace();
		}
		
		//test binary
		//File binaryin = new File("Warships.dat");
		//WarshipStaticMethods.testBinary(binaryin);
		
		// exit message
		System.out.println();
		System.out.println("***********************************************");
		System.out.println("*  Thanks for using Warship Mission Control   *");
		System.out.println("***********************************************");
		//close scanner
		kb.close();
		
	}//end main

}//end WarshipMissionControl class
