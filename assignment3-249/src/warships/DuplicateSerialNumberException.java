// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #3
// Due Date: 22nd of March 2013
//--------------------------------------------------------------------
package warships;

/**
 * This class extends the Exception class. This specific type of exception is 
 * only thrown when a duplicate serial number is entered for any warship object.  
 * @author Amit
 *
 */
public class DuplicateSerialNumberException extends Exception {

	/**
	 * Default constructor
	 */
	public DuplicateSerialNumberException()
	{
		super("Error... Duplicate Entry of Serial Number. The entered serial number exists for another record");
	}
	
	/**
	 * Parametrized constructor
	 * @param msg	of the type String
	 */
	public DuplicateSerialNumberException(String msg){
		super(msg);
	}


}
