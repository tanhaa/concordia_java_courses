// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #3
// Due Date: 22nd of March 2013
//--------------------------------------------------------------------
package warships;

import java.io.Serializable;
import java.text.NumberFormat;

/**
* <p>This class defines Warships that we will be sending out to missions.
* A warship is built primarily for combat and is characterized by several features such as
* it's cost, it's speed, the year it was built, the country it belongs to and has a   
* unique Serial Number.  </p>
* 
* <b>All implemented interfaces:</b></br>
* Serializable</br>
* 
* @author Amit Malhotra
*
*/
public class Warship implements Serializable {
	
	//Serial Version UID of the class
	private static final long serialVersionUID = -560574457505991217L;
	
	//attributes specific to all warships
	private double price; //Price of the Warship, expressed in Million of dollars
	private int year; //Year the Warship was created
	private int speed; //Speed of the Warship expressed in knots
	private String countryOfOrigin; //Country where the Warship was created
	private long serialNumber; //Serial Number of the Warship
	private String name; //name of the ship
	
	
	/**
	 * Default no argument constructor which calls the parametrized constructor with preset values for 
	 * Price, Weight, Speed and Country of Origin
	 */
	public Warship() {
		
		//making a call to the parametrized constructor
		this(352, 1952, 32, "Canada", 000000000, "Enterprise");
		/*
		 * Equivalent to saying: 
		 * this.price = 352;
		 * this.year = 1952;
		 * this.speed = 32;
		 * this.countryOfOrigin = "Canada";
		 * this.serialNumber = 000000000;
		 */
	}

	/**
	 * <p>Parametrized constructor for the Warship class.  Requires four parameters to be passed to 
	 * initialize all attributes of the object. These include the Price, the weight, speed and country
	 * where the warship was built. 
	 * </p>
	 * @param price		Of the type Double, indicating the Price in Million of Dollars of the Warship
	 * @param year		of the type Int, indicating the year the Warship was built in.
	 * @param speed		of the type Integer, indicating the speed in "knots" of the Warship
	 * @param country		of the type String, indicating the country where the Warship was built
	 * @param serialNumber 	of the type long, indicating the unique serial number of the warship
	 * @param name		of the type String, the name of the ship
	 */
	public Warship(double price, int year, int speed, String country, long serialNumber, String name){
		this.price = price;
		this.year = year;
		this.speed = speed;
		this.countryOfOrigin = country;
		this.serialNumber = serialNumber;
		this.name = name;
	}
	
	/**
	 * parametrized constructor to match the input file format and order of the parameters.
	 * @param serialNumber 	of the type long, indicating the unique serial number of the warship
	 * @param name		of the type String, the name of the ship
	 * @param year		of the type Int, indicating the year the Warship was built in.
	 * @param country		of the type String, indicating the country where the Warship was built
	 * @param price		Of the type Double, indicating the Price in Million of Dollars of the Warship
	 * @param speed		of the type Integer, indicating the speed in "knots" of the Warship
	 *  
	 */
	public Warship(long serialNumber, String name, int year, String country, double price, int speed){
		this.price = price;
		this.year = year;
		this.speed = speed;
		this.countryOfOrigin = country;
		this.serialNumber = serialNumber;
		this.name = name;
	}

	//mutator methods of the class

	/**
	 * Method that allows one to modify the price of the warship.
	 * 
	 * @param price		of the type Double, indicating the new price for the warship in Dollars.
	 */
	public void setPrice(double price){
		this.price = price;
	}

	/**
	 * Method that allows one to modify the built year of a warship. 
	 * 
	 * @param year		of the type int, indicating the year the warship was built in.
	 */
	public void setYear(int year){
		this.year = year;
	}
	
	
	/**
	 * Method that allows one to modify the speed of the warship.
	 * 
	 * @param speed		of the type Integer, indicating the new speed for the warship in "knots"
	 */
	public void setSpeed(int speed){
		this.speed = speed;
	}

	/**
	 * Method that allows one to modify the country of origin for the warship.
	 * 
	 * @param country		of the type String, indicating the new country for the warship.
	 */
	public void setCountry(String country){
		this.countryOfOrigin = country;
	}
	
	/**
	 * Method that allows one to modify the serial number of the warship.
	 * 
	 * @param serialNumber	of the type long, indicating the new serial number for the warship.
	 */
	public void setSerialNumber(long serialNumber){
		this.serialNumber = serialNumber;
	}
	
	/**
	 * Method that allows one to modify the name of the warship
	 * 
	 * @param name 	of the type String, indicating the new name of the ship
	 */
	public void setName(String name){
		this.name = name;
	}
	
	//accessor methods of the class 

	/**
	 * This method allows one to access the price of the warship. 
	 * 
	 * @return		Price in "dollars". Type Double.
	 */
	public double getPrice(){
		return this.price;
	}

	/**
	 * This method allows one to access the built year of the warship.
	 * 
	 * @return		Year in YYYY format. Type Int.
	 */
	public int getYear(){
		return this.year;
	}

	/**
	 * This method allows one to access the speed of the warship.
	 * 
	 * @return		Speed in "Knots".  Type Integer.
	 */
	public int getSpeed(){
		return this.speed;
	}
	
	/**
	 * This method allows one to access the country of origin of the warship.
	 * 
	 * @return		Country of Origin.  Type String.
	 */
	public String getCountry(){
		return this.countryOfOrigin;
	}
	
	/**
	 * This method allows one to get the serial number of the warship.
	 * 
	 * @return		Serial Number.  Type Long.
	 */
	public long getSerialNumber(){
		return this.serialNumber;
	}
	
	/**
	 * This method allows one to get the name of the warship
	 * 
	 * @return 	name	type String
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * <p>
	 * The equals method overrides the equals method defined in the Object class.  It requires 
	 * a parameter of the type Object to be passed to the method.  The method will:
	 * <li>
	 * <ul>Check if the other object exists</ul>
	 * <ul>Check if the other object was created as the same class</ul>
	 * <ul>Type cast the passed object is of the same type as the calling object</ul>
	 * <ul>Return true or false based on whether the two objects are equal or not</ul>
	 * </p>
	 */
	public boolean equals(Object otherObject) {  //this will "OVERRIDE" the equals,, not overload
		if (this == otherObject)
			return true; //check if the object being passed is the one calling the method
		
		if(otherObject == null)  //check if otherObject exists
			return false;
		else if (this.getClass() != otherObject.getClass())  //check if otherObject was created as the same class as our object
			return false;
		else {
			Warship otherShip = (Warship)otherObject; //type cast passed Object as Warship
			return (this.price == otherShip.price && this.year == otherShip.year 
					&& this.name == otherShip.name && this.speed == otherShip.speed 
					&& this.serialNumber == otherShip.serialNumber 
					&& this.countryOfOrigin.equals(otherShip.countryOfOrigin)); 
			//return true if everything matches	
		}
		
	}
	
	/**
	 * This method overrides the toString method defined in the Object class.  It will return a 
	 * String that describes the attributes of the object such as it's cost, weight, speed and where
	 * it was built. 
	 */
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(9); //removing the package name from front
		
		String p = NumberFormat.getCurrencyInstance().format(this.price); //format price correctly
		
		return ("This " + s + " (Serial Number: " + this.serialNumber + ") known as " + 
				this.name + " costs " + p + " and travels at a speed of " + this.speed + 
				" knots. It belongs to "  + this.countryOfOrigin + 
				" and was built in the year " + this.year + ".");
	}
}
