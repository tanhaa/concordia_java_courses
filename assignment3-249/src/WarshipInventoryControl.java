// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #3
// Due Date: 22nd of March 2013
//--------------------------------------------------------------------

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import warships.Warship;

import warships.WarshipStaticMethods;

/**
 * Inventory Control class contains our main method that will allow us to fix
 * the serial numbers in our inventory.
 * @author Amit
 *
 */
public class WarshipInventoryControl {

	/**
	 * Main Method in our Inventory Control Class
	 * @param args
	 */
	public static void main(String[] args) {

		// welcome message
		System.out.println("*********************************************");
		System.out.println("*       Welcome to the Inventory Control!   *");
		System.out.println("*              Programmed by                *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();
				
		// Ask the user for an output name and save it in a variable
		String outputName = WarshipStaticMethods.getOutputFileName();

		//create a File object that we can use at anytime with the name of
		//the file that contains our inventory
		File inputFile = new File("Initial_Warship_Info.txt");
		// find out how many records are in our Input File
		int nbRecords = WarshipStaticMethods.getNumberOfRecords(inputFile);

		// do we need to do any work?
		if (nbRecords <= 1) {
			System.out.println("There are not enough records in this file.");
			System.out.println("Program will now terminate.  Goodbye.");
			System.exit(0);
		}
		
		//we can now pass the number of records along with our input file name 
		//to our method that creates an array and fills it up with the file's content
		//Each line in the input file is read to create warship objects		
		Warship[] navalForce = WarshipStaticMethods.createFillArray(inputFile, nbRecords);
		
		//Let's go through our array and fix any duplicate serials 
		for (int i = 0; i < navalForce.length; i++){
			WarshipStaticMethods.fixDuplicateSerials(navalForce, navalForce[i]);	
		} //end for
		
		//We will dump this array of objects we created to fix our serial numbers into an output file
		//we already have the name of the file that the user provided for us and we saved it as a string "outputName"
		PrintWriter outputFile = null; 
		
		try{  //make sure we catch our FileNotFound Exception
			outputFile = new PrintWriter(outputName); //open file for output
			WarshipStaticMethods.writeToFile(navalForce, outputFile); //call our write method
		} catch (FileNotFoundException e){
			System.out.println("Encountered an error while creating the PrintWriter object with name " + outputName);
			e.printStackTrace();
		} finally {
			outputFile.close();
		}
		 
		//We will display our two files, input and output for comparison using Scanner
		Scanner input = null;
		Scanner output = null;
		try {  //make sure we catch our FileNotFound Exception
			input = new Scanner (new FileInputStream(inputFile));
			output = new Scanner (new FileInputStream(outputName));
			System.out.println("\nHere are the contents of file " + inputFile.getName() + " AFTER copying operation: ");
			System.out.println("=================================================================================");
			WarshipStaticMethods.displayFile(input);
			System.out.println("\nHere are the contents of file " + outputName + ": ");
			System.out.println("=================================================");
			WarshipStaticMethods.displayFile(output);
			
		} catch (FileNotFoundException e){
			System.out.println("Error occured while displaying the in/out files");
			e.printStackTrace();
		}
		
		// We are done! exit message
		System.out.println();
		System.out.println("***********************************************");
		System.out.println("* Thanks for using Warship Inventory Control  *");
		System.out.println("***********************************************");
				
		
	}
}
