package assignment1;
// ------------------------------------------------------------------
// Name and ID: Amit Malhotra
// Comp 249
// Assignment #1
// Due Date: 1st of February 2013
//--------------------------------------------------------------------


	/**
	 * <p>
	 * Battleship is a turn-based console game between a human and computer player.
	 * The players place ships and mines on an 8x8 grid and the
	 * object of the game is to sink all ships and mines owned
	 * by the opponent with the help of launched missiles. 
	 * </p>
	 * <p>
	 * Rules of the game:<br>
	 * <ul>
	 * <li>Each player places 6 ships and 4 mines (Human player first).</li>
	 * <li>Once the computer has placed its pieces, the game begins.</li>
	 * <li>Each player takes turns and launches a missile by providing
	 *   valid grid coordinates (eg. A1, E8)</li>
	 * <li>After each hit, the grid status is displayed showing the cells
	 *   that have been hit and if any ship and mine were present </li>
	 * <li>If a player who hits a mine loses a turn</li>
	 * <li>The player who destroys all of the opponent's ships and mines
	 *   wins the game.</li>
	 * </ul>  
	 * </p>
	 * @author Amit Malhotra (5796997)
	 */
public class Battleship {

	/**
	 * The main method runs the game.  It creates a new GameBoard object, two player objects and calls
	 * the place() and launchRocket() methods to carry out the game-play.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		//welcome message
		System.out.println("*********************************************");
		System.out.println("*       Welcome to Console Battleship!      *");
		System.out.println("*            Game Programmed by             *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();
		System.out.println("***************************************************");
		System.out.println("Game Play: This game is played between you and");
		System.out.println("a computer opponent. Each player must place 6 ");
		System.out.println("ships and 4 mines on an 8x8 Game Board.");
		System.out.println("To enter ship/mine coordinates, use the following");
		System.out.println("format:  A1, B1, H8, C8, etc. (A to H, 1 to 8)");
		System.out.println("Once all pieces are placed, each player will launch");
		System.out.println("a rocket and try to destroy all ships and mines");
		System.out.println("belonging to the opponent. However, whenever a mine");
		System.out.println("is hit, the player who hits it will lose a turn.");
		System.out.println("****************************************************");
		System.out.println("\nTime to place your pieces.  Good luck\n");
		
		
		//Setup a new game
		GameBoard newGame = new GameBoard(); //create a new game
		Player playerA = new Player(PlayerType.HUMAN); //create a HUMAN player
		Player playerB = new Player(PlayerType.COMPUTER); //create a COMPUTER player
		boolean gameOver = false;
		
		//lets place the pieces now.  
		newGame.place(playerA, ContentType.SHIP); //get playerA to place his/her ships
		newGame.place(playerA,  ContentType.MINE); //get playerA to place his/her mines
		newGame.place(playerB, ContentType.SHIP); //get playerB to place his/her ships
		newGame.place(playerB, ContentType.MINE); //get playerB to place his/her mines
		
		//let's get the game started after placing playerB (computer) pieces
		System.out.println("\nOK, the computer placed its ships and mines at random. Let's play.");
		
		
		//this loop will run until the gameOver flag is set to true.  It implements a turn-based
		//functionality where each player launches a missile when their turn comes. The loop
		//checks if the opposite player is still alive (inventory>0) and continues to play till
		//the condition is true and game is over.
		while(!gameOver){
			
			//playerA's turn
			newGame.launchRocket(playerA, playerB);
			//did playerA win the game after his/her move?
			if (!playerB.isAlive()){
				System.out.println("You win!");
				System.out.println("\nThe ships and mines were here: ");
				newGame.printBoard("full");
				gameOver = true;
				continue;
			}
			
			//playerB's turn
			newGame.launchRocket(playerB, playerA);
			//did playerB win the game after his/her move?
			if (!playerA.isAlive()){
				System.out.println("I win!");
				System.out.println("\nThe ships and mines were here: ");
				newGame.printBoard("full");
				gameOver = true;
				continue;
				
			}
			
		} //end while
		
	} //end main

} //end Battleship class
