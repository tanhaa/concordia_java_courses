package assignment1;
// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #1
// Due Date: 1st of February 2013
//--------------------------------------------------------------------


import java.util.*;  //importing all utils such as scanner and random


/**
 * This class serves to create a game-board, an object that mainly contains 
 * an 8x8 grid upon which the battleship game will be played.  
 * The board itself is basically a 2D array of cell objects. 
 * The class also contains all the methods that have a direct impact on the game-play
 */
public class GameBoard {

	//declaring scanner and random as static so we can use them in all our methods
	private static Scanner in = new Scanner(System.in); 
	private static Random generator = new Random();
	
	private Cell board[][] = new Cell[8][8]; //a 2D array of Cell objects
	private int i, j; //variables to be used in our loops
	
	
	/**
	 *  Default no argument constructor that initializes our 2D Cell Object board and 
	 *  fills it with new cell objects.  Called whenever a new battleship game begins. 
	 */
	public GameBoard(){
			
		for (i = 0; i < board.length; i++){
			for (j = 0; j < board[i].length; j++){
				board[i][j] = new Cell();
			}
		}
	}
	
	/**
	 * <p>
	 * Method to print the current status of the board. The details printed are based
	 * on the parameter passed. When "full" is passed as a parameter, the printed board shows
	 * the placement of all ships and mines of both players. With "secret" passed as a 
	 * parameter, only the revealed ships, mines and cells that have been hit are shown.
	 * </p><p>  
	 * Legend of the board:<br>
	 * <ul>
	 * <li><b>S</b> = Ship owned by the computer</li>
	 * <li><b>M</b> = Mine owned by the computer</li>
	 * <li><b>s</b> = Ship owned by the human player</li>
	 * <li><b>m</b> = mine owned by the human player</li>
	 * <li><b>*</b>	 = a location that has been previously revealed and has no content</li>
	 * </ul></p>
	 * @param s		can ONLY take two values: "full" or "secret"
	 *  
	 */
	public void printBoard(String s){
		
		//setup the column header with ABCDEFGH
		System.out.print("\t  ");
		for (i = 0; i < board.length; i++){
			System.out.print((char)(65+i) + " ");  //type cast 65+i to a char
		}
		System.out.println();
		
		//Setup the board for printing
		for (i = 0; i < board.length; i++){
			//Setup the row header 12345678
			System.out.print("\t" + (i+1) + " ");
			//inner loop to print the content of the 2D array
			for (j = 0; j < board[i].length; j++){
				
				ContentType pieceType = board[i][j].getPiece(); //get the value of the piece from the cell class
				PlayerType playerType = board[i][j].getOwnerType(); //get the owner of the piece from the cell class
				
				//if-else to verify whether we want the "secret" board to print
				//or the "completed un-hidden" board.
				if (s.equalsIgnoreCase("secret")) //using equalsIgnoreCases to account for SECRET as well as secret
				{
					switch(pieceType)  //switch based on the type of piece that's on that particular cell
					{
						case NONE:  //if there's no piece, check if it's hit, if it is, show a * 
									if (board[i][j].isHit()){
										System.out.print("* ");
									} else {
									System.out.print("- ");
									}
									break;
						case SHIP:  //if there is a ship, check if it's a human or computer ship and show appropriate symbol only if the cell is hit
									if (playerType == PlayerType.HUMAN && board[i][j].isHit()){
										System.out.print("s ");
									} else if (playerType == PlayerType.COMPUTER && board[i][j].isHit()){
										System.out.print("S ");
									} else {
										System.out.print("- ");
									}
									break;
						case MINE:  //if there's a mine, check if it's a human or computer mine and show appropriate symbol only if the cell is hit
									if (playerType == PlayerType.HUMAN && board[i][j].isHit()){
										System.out.print("m ");
									} else if (playerType == PlayerType.COMPUTER && board[i][j].isHit()){
										System.out.print("M ");
									} else {
										System.out.print("- ");
									}
									break;
					} //end switch
					
				} else {   	//print the full board, notice lack of check to see if s was "full" 
							//so the method will print full board regardless of what the value of "s" is. 
					
					switch(pieceType)  
					{
						case NONE:  //print out a "-" if no piece is present on the cell
									System.out.print("- ");
									break;
						case SHIP:  //check to see if owner of the ship is human, if yes, print s, else print S
									if (playerType == PlayerType.HUMAN){
									System.out.print("s ");
									} else {
									System.out.print("S ");
									}
									break;
						case MINE:  //check to see if owner of the mine is human, if yes, print m, else print M
									if (playerType == PlayerType.HUMAN){
										System.out.print("m ");
									} else {
										System.out.print("M ");
									}
									break;
					} //end switch 
					
				} //end if-else
				
				
			} //close internal loop
			System.out.println();
		}//close external loop
	} //end printBoard method
	
	/**
	 * This method is used to see if the coordinates entered by the player during the gameplay were
	 * valid, that is if they were within the confines of the grid. 
	 * 
	 * @param x		Grid coordinate indicating X (ABCDEFGH)
	 * @param y		Grid coordinate indicating Y (12345678)
	 * @return		boolean (True or false)
	 */
	public boolean validCoordinates(int x, int y){
		if (y < 0 || y > 7 || x < 0 || x > 7)
			return false;
		return true;
	}
	
	/**
	 * This method is used to capture and fix the coordinates that the player will be asked for
	 * several times during the gameplay. It will also convert the player entered board coordinate to the 
	 * corresponding value on the 2D array. Example: ABCDEFGH --> 0,1,2,3,4,5,6,7
	 * 
	 * @return		An array of two values, x and y for the coordinates
	 */
	public int[] fixEnteredCoordinates(){
		//local variables
		int arr[] = new int[2]; //will be returned 
		String input = in.next(); //considering user enters "A1" for example, take the whole string and manipulate
		
		//save the first character in the inputstring, convert to uppercase and store as char
		char a = (char)input.toUpperCase().charAt(0);  
		//wrapper class Integer.parseInt method used to take the integer from the second index of the input string
		arr[0] = (Integer.parseInt(input.substring(1)))-1;
		//convert char a to an integer and save it in the array as well
		arr[1] = (int)a-65;	

		return arr;  //return x, y
	}
	
	/**
	 * This method is used to create random coordinates.  It is mainly be used to generate
	 * random coordinates x and y within our 2D array board grid for the Computer player.
	 * It is called every time a computer player is required to generate coordinates  
	 * 
	 * @return 		an array of two values, x and y, for the coordinates
	 */
	public int[] createRandomCoordinates(){
		
		int[] a = new int[2]; //return this array of two integers
		a[0]= generator.nextInt(8); //create a random number between 0 and 7
		a[1]= generator.nextInt(8); //create a random number between 0 and 7
		
		return a;
	}
	
	/**
	 * This method allows the players to place their ships and mines on the game-board.
	 * It will ask the player to enter the coordinates for his ships and mines and
	 * re-prompt if coordinate entered are invalid.  The method is used by both human
	 * and computer player.
	 *  
	 * @param player		the identity of the player object calling the method
	 * @param piece			the identity of the type of piece being placed. Accepted value SHIP OR MINE
	 */
	public void place(Player player, ContentType piece){
		int i = 0, x = -1, y = -1;  //local variables for the loops - initialize x and y to invalid array coordinates
		
		while (piece == ContentType.SHIP ? i < 6 : i < 4){ //run this loop 6 time if it's SHIP, 4 times if it's MINE
			
			//check to see who called the method, human or computer and run the appropriate code
			if (player.getPlayerType() == PlayerType.HUMAN){  //if human player called
				
				//run this loop as long as we don't have valid and unselected cell address
				while(!validCoordinates(x,y) || board[x][y].isSelected()){
					
					if (piece == ContentType.SHIP) {  //if wanting to place his ships ask coordinates
						System.out.print("\nEnter the coordinates of your ship #" + (i+1) + ":" );
					} else {
						System.out.print("\nEnter the coordinates of your mine #" + (i+1) + ":");	
					}
					
					int[] arr = fixEnteredCoordinates();  //fix the coordinates player provided
					x = arr[0]; y=arr[1];  //store them as x, y for further manipuation
					
					//check if human player selected coordinates inside the grid
					if (!validCoordinates(x,y)){
						System.out.println("\nsorry, coordinates outside the grid. try again.");
						continue;  //go back to the beginning
					}  //close if
					
					//check if the coordinates have already been selected by someone or not
					if (board[x][y].isSelected()){
						System.out.println("\nsorry, coordinates already used. try again.");
						continue;  //go back to the beginning
					}
					
				} //close inner human while loop to capture valid, unselected coordinates
				
			} else { //player calling the method is computer
				//run this loop until we generate an unselected cell value for computer
				//The coordinates randomly produced will always be valid, but with initialized values
				//of -1 each, the isSelected() method from cell class will give an out of bound error
				//check validcoordinates to short-circuit the test to avoid this exception
				while(!validCoordinates(x,y) || board[x][y].isSelected()){ 
				int arr[] = createRandomCoordinates();
				x = arr[0]; y = arr[1];
				continue;
				} //close inner COMPUTER while loop
				
			} //close if-else to check if player = human/computer
			
						
			board[x][y].changePiece(piece);  //set the right piece on the cell
			board[x][y].setOwner(player);    //set the owner of that piece as player object who called the method
			board[x][y].setSelected(true);   //set the cell selected flag as TRUE 
			
			//based on the type of ship, increase the inventory of the player object
			if (piece == ContentType.SHIP){
				player.addShip();
			} else {
				player.addMine();
			}
			
			
			i++;  //increment i (at 6 ships, loop ends, at 4 mines loop ends)
		
		} //end while loop
	
		
	} //end place()
	
	/**
	 * <p>
	 * This method will be used in a "turn-based" manner to launch the missiles during game-play.  
	 * Each player will get a turn to launch a rocket.  The player who is launching the rocket and 
	 * his opponent must be supplied as parameters when calling this method.  The method will ask for 
	 * the position of the rocket and re-prompt if invalid coordinates are provided. It will then take 
	 * appropriate action as per the rules of the game. </p>
	 * <p>
	 * <ul> 
	 * <li>If there is nothing at the location attacked, it will reveal the cell</li>
	 * <li>If there is a ship at the location, it will show the ship symbol based on who owned it</li>
	 * <li>If there is a mine at the location, it will show the mine symbol based on who owned it</li>
	 * <li>If the position has already been called, it will let the player know</li>
	 * </ul></p>  
	 * 
	 * @param player			The player object who called the method (who is launching the missile)
	 * @param opponnent			The player object who will be hit by the missile (the opponent) 
	 */
	public void launchRocket(Player player, Player opponnent){
		
		int x = -1, y = -1; //local variables set to -1 so that they have invalid grid coordinates
		
		//if player has not been penalized, make his move
		//penalty happens when the player hits a mine and it lasts one turn
		//this if will check if the player actually is allowed to make a move
		if (!player.isPenalized()){
			
			//if player is allowed to make a move, check to see if it's a human or computer
			if (player.getPlayerType() == PlayerType.HUMAN){ //if human
				while(!validCoordinates(x,y)){
					//run this loop till player provides valid coordinates to launch a rocket to.
					System.out.print("\nposition of your rocket: " ); //prompt for position
					int[] arr = fixEnteredCoordinates();  //fix coordinates
					x = arr[0]; y=arr[1];  //save our coordinates
					
					//check if human player selected coordinates inside the grid
					if (!validCoordinates(x,y)){
						System.out.println("\nsorry, coordinates outside the grid. try again.");
						continue; //go back to the begnning
					}  //close if
					
				} //close while
				
			} else { //create a random coordinate for the computer player hit 
					int arr[] = createRandomCoordinates();
					x = arr[0]; y = arr[1]; //save our coordinates
					System.out.println("position of my rocket: " + ((char)(y+65)) + (x+1)); //show where the computer is launching the missile
				
			} //close if-else
			
			ContentType pieceOnBoard = board[x][y].getPiece(); //let's see what's on that position referred by the coordinates above
			switch(pieceOnBoard) //take action based on what's in the cell
			{
			case NONE:
						if(board[x][y].isHit()){  //was it ever called before?
							System.out.println("position already called.");
						} else {
							System.out.println("nothing."); //nothing there to hit!
						}
						break;
			case SHIP:
						if(board[x][y].isHit()){  //was it ever called before?
							System.out.println("position already called.");
						} else {
							System.out.println("ship hit."); //ooooh a ship!
							board[x][y].getOwner().removeShip(); //update player inventory
							board[x][y].destroyPiece(); //set destroyed flag to true
						}
						break;
			case MINE:
						if(board[x][y].isHit()) {  //was it ever called before?
							System.out.println("position already called.");
						} else {
							System.out.println("boom! mine."); //boom!
							board[x][y].getOwner().removeMine(); //update player inventory
							board[x][y].destroyPiece();  //set destroyed flag to true
							player.penalize(true);  //oh yes, penalize player for hitting a mine, miss one turn
						}
						break;
			}
			
			board[x][y].hitCell(); //set "hit" flag on the cell to true
			if (opponnent.isAlive())
				this.printBoard("secret"); //if game isn't over yet, just print the secret board

			
		} else {
			
			player.penalize(false); //so if we skipped the players turn, let's get him out of the penalty box so he can play again
			
		} //close main if-else			
	} //close launchRocket()
}
