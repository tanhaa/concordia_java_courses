package assignment1;
// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #1
// Due Date: 1st of February 2013
//--------------------------------------------------------------------

//enumerated type to know the what type of piece the cell contains
enum ContentType{
	NONE, SHIP, MINE
	};  


	/**
	 * The cell class allows the creation of the cells (objects) on the board, which correspond
	 * to each little box on the game board. 
	 * Each cell object contains information on what type of piece or content (ship, mine, none) they carry and who owns
	 * that piece (Player 1 or Player 2).  It also has several flags that tell us whether the Cell has been 
	 * selected before, whether the piece on it has been destroyed or whether a missile has hit it or not
	 * 
	 */
public class Cell {

	//Attributes
	private boolean selected = false;  //have we ever called it before?
	private ContentType piece;  //Enum ContentType to describe the type of piece on it
	private Player owner; //Which player object owns it?
	private boolean destroyed = false;  //is that piece destroyed?
	private boolean hit = false; //has the cell been hit by a missile/

	/**
	 * Default no argument constructor that creates a cell object with no ship or mine on it
	 * and all flags set to false.
	 * 
	 */
	public Cell(){
		
		this.piece = ContentType.NONE;
		this.selected = false;
		this.destroyed = false;
		this.hit = false;
		this.owner = null;
		
	}
	
	/**
	 * This method allows us to set the "hit" flag to true whenever the cell is hit by a missile.
	 */
	public void hitCell(){
		this.hit = true;
	}

	/**
	 * This method returns whether the cell has been hit with a missile or not.
	 * @return  	boolean value true or false
	 */
	public boolean isHit(){
		return this.hit;
	}
	
	/**
	 * This method allows us to set the "selected" flag to true whenever the cell has been selected.
	 * @param flag 		pass a boolean flag, true or false
	 */
	public void setSelected(boolean flag){
		this.selected = flag;
	}
	
	/**
	 * This method returns whether the cell has been previously selected or not
	 * @return		boolean value true or false
	 */
	public boolean isSelected(){
		if (this.selected == true)
			return true;
		return false;
	}
	
	/**
	 * This method returns the type of content present in this cell (Ship, mine or nothing)
	 * @return		ContentType (Enum) returned with SHIP, MINE or NONE
	 */
	public ContentType getPiece(){
		return this.piece;
	}
	
	/**
	 * This method allows one to set the owner of this cell object as the player object being passed
	 * as a parameter. 
	 * @param owner		Accepts only Player Objects
	 */
	public void setOwner(Player owner){
		this.owner = owner;
	}
	
	/**
	 * This method returns the Player object that owns this cell object (or the piece on this cell object)
	 * @return		An object of the class Player
	 */
	public Player getOwner(){
		return owner;
	}
	
	/**
	 * This method allows us to get the "PlayerType", Human or computer or none of the Player object
	 * that owns the calling cell object (or the piece residing in this cell)
	 * @return		PlayerType Enum, values HUMAN, COMPUTER, NONE
	 */
	public PlayerType getOwnerType(){ 
		if (this.owner == null) //if nobody owns it
			return PlayerType.NONE;  //return NONE
		return this.owner.getPlayerType(); //call method from the player class
	}
	
	/**
	 * This method allows us to change the type of the piece (ContentType) placed in the calling cell object
	 * @param type		requires ContentType enum value
	 */
	public void changePiece(ContentType type){
		this.piece = type;
	}
		
	/**
	 * This method allows us to set the "destroyed" flag to true indicating the content on this piece has been destroyed
	 * 
	  */
	public void destroyPiece(){
		this.destroyed = true;
	}

	
	//this returns status if destroyed or not
	/**
	 * This method returns the value of the boolean flag "destroyed" indicating whether the content residing in the calling cell object has been destroyed or not
	 * 
	 * @return		boolean value true or false
	 */
	public boolean isDestroyed(){
		return destroyed;
	}
}
