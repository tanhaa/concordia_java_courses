package assignment1;
// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #1
// Due Date: 1st of February 2013
//--------------------------------------------------------------------

/**
 * 
 * The enumerated type gives us the choice to classify our player object
 *
 */
enum PlayerType{
	HUMAN, COMPUTER, NONE
	}; 

	/**
	 * The Player class allows the creation of player objects.
	 * Specifically speaking, player objects contain all attributes related
	 * to players, such as how many active ships and mines (inventory) the player has
	 * and the methods to do manipulations and visualize these attributes.
	 */
public class Player {


	private PlayerType type;  //Enum type giving the type of player
	private int shipsOwned, minesOwned; //Stores number of ships and mines owned by the player
	private boolean penalized; //boolean flag to see if a player has been penalized after hitting a mine in one of his/her turns
	
	/**
	 * Constructor creates a player object with predetermined values
	 * 
	 * @param type 		Enumerated PlayerType must be supplied, HUMAN or COMPUTER  
	 */
	public Player(PlayerType type){
		this.type = type; //set the type to supplied type
		this.shipsOwned = 0;  //initialize inventory to 0
		this.minesOwned = 0;
		this.penalized = false; //set penalized flag to false
	}
	
	/**
	 * Default No argument player constructor in case no type is supplied
	 */
	public Player(){
		this.type = PlayerType.NONE; //set the type to NONE
		this.shipsOwned = 0;  //initialize inventory to 0
		this.minesOwned = 0;
		this.penalized = false; //set penalized flag to false
	}
	
	/**
	 * Allows the game to increase player's inventory by one ship 
	 */
	public void addShip(){
		this.shipsOwned++; 
	}
	
	/**
	 * Allows the game to subtract ship from the player's inventory (when destroyed).
	 */
	public void removeShip(){
		this.shipsOwned--;
	}
	
	/**
	 * Allows the game to increase player's inventory by one mine 
	 */
	public void addMine(){
		this.minesOwned++;
	}
	
	/**
	 * Allows the game to subtract one mine from the player arsenal (when destroyed)  
	 */
	public void removeMine(){
		this.minesOwned--;
	}
	
	/**
	 * Sets player as penalized when player hits a mine. 
	 * 
	 * @param flag (boolean true or false value)  
	 */
	public void penalize(boolean flag){
		penalized = flag;
	}
	
	/**
	 * Provides player status if penalized or not allowing the game to know if the player loses a turn or not
	 *
	 * @return penalized (boolean true or false value)
	 */
	public boolean isPenalized(){
		return penalized;
	}
	
	/**
	 * Prints out information on the player, how many ships he has, how many mines and his PlayerType.
	 * Overrides default .toString() method
	 */
	public String toString(){
		return ("Player:" + type.toString() + "\nNumber of Ships: " + shipsOwned + "\nNumber of Mines: " + minesOwned);
	}
	
	/**
	 * Returns the PlayerType attribute of the object
	 * 
	 * @return  	PlayerType Enum, HUMAN, COMPUTER, NONE
	 */
	public PlayerType getPlayerType(){
		return this.type;
	}

	/**
	 * This method checks if the player inventory is greater than 0, if yes, it returns true that the player is still alive.
	 * Method can be used to test for "game-over" condition.
	 * 
	 * @return 	boolean true or false value
	 */
	public boolean isAlive(){
		if (shipsOwned == 0 && minesOwned == 0)
			return false;  //return false if player's inventory is 0
		return true;
	}
}
