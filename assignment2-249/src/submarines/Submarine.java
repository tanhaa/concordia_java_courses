// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #2
// Due Date: 1st of March 2013
//--------------------------------------------------------------------

package submarines;
import java.text.NumberFormat;

import warships.Warship;

/**
 * This class defines a submarine, a warship that can operate indepedently underwater. This class
 * extends the Warship class. 
 *
 */
public class Submarine extends Warship {

	/**
	 * The enumerated type gives us the choice to classify our Submarines power source
	 */
	public static enum PowerSource{ //declared as public static inside the Submarine class so we can access it outside the package
		NUCLEAR, DIESEL
		};

	//attributes of Submarine class
	private PowerSource sourceOfPower; //the source of power it uses (an EnumType)
	private int nbMissiles; //number of missiles it has
	private String sensorSuite; //the type of sensor it carries, such as passive/active sonars, radars, periscopes

	
	//constructors
	
	/**
	 * Default constructor of the submarine class.  Makes a call to super constructor defined in Warship class. 
	 */
	public Submarine() {
		super(); //call to superconstructor
		this.sourceOfPower = PowerSource.NUCLEAR;
		this.nbMissiles = 5;
		this.sensorSuite = "Periscope";
	}
	
	/**
	 * <p>Parametrized constructor for the Submarine class.  Calls the super constructor defined
	 * in the Warship class.
	 * </p>
	 * @param price		Of the type Double, indicating the Price in Million of Dollars of the Submarine
	 * @param weight		of the type Float, indicating the weight in "tons" of the Submarine
	 * @param speed		of the type Integer, indicating the speed in "knots" of the Submarine
	 * @param country		of the type String, indicating the country where the Submarine was built
	 * @param source	of the type PowerSource, an enum type indicating the source of power for the submarine
	 * @param missiles		of the type Integer, indicating the number of missiles the submarine carries
	 * @param sensor	of the type String, indicating the type of sensors the submarine is equipped with.
	 */
	public Submarine(double price, float weight, int speed, String country, PowerSource source, int missiles, String sensor) {
		super(price, weight, speed, country); //call to superconstructor defined in Warship class
		this.sourceOfPower = source;
		this.nbMissiles = missiles;
		this.sensorSuite = sensor;
	}

	//accessor methods
	
	/**
	 * This method allows one to access the Power Source that powers the submarine.
	 * 
	 * @return	Source of Power (of the enum type PowerSource)
	 */
	public PowerSource getPowerSource(){
		return this.sourceOfPower;
	}
	
	/**
	 * This method allows one to access the Number of Missiles the submarine has.
	 * 
	 * @return	Number of Missiles (of the type Integer)
	 */
	public int getNbMissiles(){
		return this.nbMissiles;
	}
	
	/**
	 * This method allows one to access the type of Sensor the submarine has.
	 * 
	 * @return	Sensor Suite (of the type String)
	 */
	public String getSensor(){
		return this.sensorSuite;
	}
	
	//mutator methods
	
	/**
	 * This method allows one to modify the source of power the submarine uses.
	 * 
	 * @param source	of the type PowerSource, an enum type
	 */
	public void setPowerSource(PowerSource source){
		this.sourceOfPower = source;
	}
	
	/**
	 * This method allows one to modify the number of missiles the submarine has.
	 * 
	 * @param missiles		of the type integer, sets the number of missiles
	 */
	public void setNbMissiles(int missiles){
		this.nbMissiles = missiles;
	}
	
	/**
	 * This method allows one to modify the sensor suite the submarine is equipped with.
	 * 
	 * @param sensor	of the type String, sets the sensor suite of the submarine such as passive/active sonars, radars, periscopes
	 */
	public void setSensor(String sensor){
		this.sensorSuite = sensor;
	}
	
	
	/**
	 * The toString method outputs the Source of Power, Number of Missiles and Sensor Suite along with 
	 * a descripton similar to the Warship.toString(). Follow description below.
	 */
	@Override
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(11); //removing the package name from front
		
		String w = NumberFormat.getIntegerInstance().format(this.getWeight()); //format weight correctly
		String p = NumberFormat.getIntegerInstance().format(this.getPrice()); //format price correctly
		
		return ("This " + s + " costs $" + p + "M and weighs " + w + 
				" tons and travels at a speed of " + this.getSpeed() + " knots. It belongs to " 
				+ this.getCountry() + ". It is powered by a " + this.sourceOfPower + " power source" +
						" and uses a sensor suite comprised of " + this.sensorSuite + ". It also" +
								" carries " + this.nbMissiles + " missiles." );
	}


	/**
	 * The Equals method overrides the equals method defined in the parent warship class. It checks 
	 * if the passing object exists, uses the parent's equal method to test the equality of the
	 * inherited attributes, tests the equality of the class and then verifies all of the objects
	 * own attributes for equality.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {  //if the passed object is the calling object return true
			return true;
		}
		if (!super.equals(obj)) { //make a call to the parents equal to check the inherited attributes 
			return false;
		}
		if (getClass() != obj.getClass()) { //if the passed object is of the same class or not
			return false;
		}
		Submarine other = (Submarine) obj; //type cast passed object as a Submarine
		
		//check if our passed object is truly the same as our calling object
		if (nbMissiles != other.nbMissiles) { 
			return false;
		}
		if (sensorSuite == null) {
			if (other.sensorSuite != null) {
				return false;
			}
		} else if (!sensorSuite.equals(other.sensorSuite)) {
			return false;
		}
		if (sourceOfPower != other.sourceOfPower) {
			return false;
		}
		//we didn't return false yet? It matches, return true
		return true;
	}

	

}
