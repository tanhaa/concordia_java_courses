// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #2
// Due Date: 1st of March 2013
//--------------------------------------------------------------------

package carriers;

import java.text.NumberFormat;

/**
 * This class defines a super carrier which is the largest type of Aircraft Carrier. It extends the
 * AircraftCarrier class and consequently inherits all characteristics of a Warship class.  
 *
 */
public class SuperCarrier extends AircraftCarrier {
	
	//Attributes of SuperCarrier
	private int nbBombers;  //Number of Bombers it can carry

	/**
	 * The default constructor for the SuperCarrier class.  Makes a call to the default constructor
	 * defined in the Aircraft class. 
	 */
	public SuperCarrier() {
		super(); //call to super constructor of the AircraftCarrier class
		this.nbBombers = 5;
	}

	/**
	 * The parametrized constructor for the SuperCarrier class.  The constructor makes a call
	 * to the parametrized constructor defined in the AircraftCarrier class. 
	 * 
	 * @param price		of the type double, indicating the Price of the SuperCarrier (in Dollars).
	 * @param weight		of the type float, indicating the Weight of the SuperCarrier (in Tons). 
	 * @param speed		of the type integer, indicating the speed of the SuperCarrier (in Knots).
	 * @param country		of the type String, indicating the country where the carrier was built.
	 * @param fighters	of the type integer, indicating the number of fighter planes the carrier can support.
	 * @param helicopters		of the type integer, indicating the number of helicopters the carrier can support.
	 * @param bombers		of the type integer, indicating the number of bombers the super carrier can support.
	 */
	public SuperCarrier(double price, float weight, int speed, String country, int fighters, int helicopters, int bombers) {
		super(price, weight, speed, country, fighters, helicopters); //call to super constructor of the AircraftCarrier class
		this.nbBombers = bombers;
	}
	
	//accessor methods
	

	/**
	 * This method allows us to access the number of bombers our super carrier can accomodate 
	 * @return number of bombers of the type integer
	 */
	public int getNbOfBombers(){
		return this.nbBombers;
	}
	
	//mutator methods
	
	/**
	 * This method allows us to modify the number of bombers our super carrier can accomodate
	 * @param bombers (of the type integer)
	 */
	public void setNbOfBombers(int bombers){
		this.nbBombers = bombers;
	}
	

	/**
	 * This method allows us to output the status of our Super Carrier Object
	 */
	@Override
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(9); //removing the package name from front
		
		String w = NumberFormat.getIntegerInstance().format(this.getWeight()); //format weight correctly
		String p = NumberFormat.getIntegerInstance().format(this.getPrice()); //format price correctly
		
		
		return ("This " + s + " costs $" + p + "M and weighs " + w + 
				" tons and travels at a speed of " + this.getSpeed() + " knots. It belongs to " 
				+ this.getCountry() + ". It can accomodate " + this.nbFighterPlanes + " Fighter Planes" +
						", " + this.nbHelicopters + " Helicopters and " + this.nbBombers + 
						" Bombers.");
	}



	/**
	 * The Equals method overrides the equals method defined in the parent warship class. It checks 
	 * if the passing object exists, uses the parent's equal method to test the equality of the
	 * inherited attributes, tests the equality of the class and then verifies all of the objects
	 * own attributes for equality.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) { //calling and passing objects are the same?
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) { //same types?
			return false;
		}
		SuperCarrier other = (SuperCarrier) obj; //type cast the object as SuperCarrier
		//check if the specific attributes match
		if (nbBombers != other.nbBombers) {
			return false;
		}
		//similar?
		
		return true;
	}
	
}
