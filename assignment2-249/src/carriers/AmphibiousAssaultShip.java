// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #2
// Due Date: 1st of March 2013
//--------------------------------------------------------------------

package carriers;

import java.text.NumberFormat;

/**
 * This class defines an Amphibious Assault Ship, a type of Aircraft Carrier built to land and
 * support ground forces on enemy territory. This class extends AircraftCarrier class and consequently
 * also inherits all attributes from the Warship class. 
 *
 */
public class AmphibiousAssaultShip extends AircraftCarrier {
	
	//attributes of the AmphibiousAssaultShip
	private int nbLandingCrafts; 
	
	/**
	 * The default constructor for the SuperCarrier class.  Makes a call to the default constructor
	 * defined in the Aircraft class. 
	 */
	public AmphibiousAssaultShip() {
		super(); //call to super constructor of the Aircraft carrier class
		this.nbLandingCrafts = 5;
	}

	/**
	 * The parametrized constructor for the SuperCarrier class.  The constructor makes a call
	 * to the parametrized constructor defined in the AircraftCarrier class. 
	 * 
	 * @param price		of the type double, indicating the Price of the AmphibiousAssaultShip (in Dollars).
	 * @param weight		of the type float, indicating the Weight of the AmphibiousAssaultShip (in Tons). 
	 * @param speed		of the type integer, indicating the speed of the AmphibiousAssaultShip (in Knots).
	 * @param country		of the type String, indicating the country where the AmphibiousAssaultShip was built.
	 * @param fighters	of the type integer, indicating the number of fighter planes the ship can support.
	 * @param helicopters		of the type integer, indicating the number of helicopters the ship can support.
	 * @param landingCrafts	of the type integer, indicating the number of Amphibious Landing crafts the ship can support.
	 */
	public AmphibiousAssaultShip(double price, float weight, int speed, String country, int fighters, int helicopters, int landingCrafts) {
		super(price, weight, speed, country, fighters, helicopters); //call to super constructor of the Aircraft Carrier class
		this.nbLandingCrafts = landingCrafts;
	}
	
	//accessor methods
	
	/**
	 * This method allows one to access the number of Landing Crafts the carrier can accomodate
	 * @return  number of landing crafts (of the type integer)
	 */
	public int getNbOfLandingCrafts(){
		return this.nbLandingCrafts;
	}
	
	//mutator methods
	
	/**
	 * This method allows one to modify the number of Landing Crafts the carrier can accomodate
	 * @param landingCrafts  (of the type integer)
	 */
	public void setNbOfLandingCrafts(int landingCrafts){
		this.nbLandingCrafts = landingCrafts;
	}

	/**
	 * This method outputs the status of the Amphibious Assault Ship
	 */
	@Override
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(9); //removing the package name from front
		
		String w = NumberFormat.getIntegerInstance().format(this.getWeight()); //format weight correctly
		String p = NumberFormat.getIntegerInstance().format(this.getPrice()); //format price correctly
		
		return ("This " + s + " costs $" + p + "M and weighs " + w + 
				" tons and travels at a speed of " + this.getSpeed() + " knots. It belongs to " 
				+ this.getCountry() + ". It can accomodate " + this.nbFighterPlanes + " Fighter Planes" +
						", " + this.nbHelicopters + " Helicopters and " + this.nbLandingCrafts + 
						" Landing Crafts.");
	}


	/**
	 * The Equals method overrides the equals method defined in the parent warship class. It checks 
	 * if the passing object exists, uses the parent's equal method to test the equality of the
	 * inherited attributes, tests the equality of the class and then verifies all of the objects
	 * own attributes for equality.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {  //calling object and passing object are the same?
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) { //are the types same?
			return false;
		}
		AmphibiousAssaultShip other = (AmphibiousAssaultShip) obj; //type cast the generic object as our calling object
		//now check the attributes
		if (nbLandingCrafts != other.nbLandingCrafts) {
			return false;
		}
		//similar?
		return true;
	}


	
}
