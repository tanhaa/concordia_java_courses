// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 352
// Assignment #2
// Due Date: 9th of March 2014
//--------------------------------------------------------------------

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.EmptyStackException;

public class Stack<T> {  //type can be user defined at creation
	
	//parameters
	//we will use an array
	static int hiddenIncCounter = 0;
	
	private Object[] stkArray;
	private int size = 0; //this will keep track of the size of our stack and our last element
	private int capacity = 0; //this will keep track of our capacity
	private char expansionRule = 'd'; //default expansion rule set at d for doubling

	/**
	 * Default Constructor creates a stack that can carry 10 objects with an expansion rule 
	 * that is setup to double the underlying array whenever the array reaches 90% or more capacity
	 * 
	 */
	public Stack(){
		this.stkArray = new Object[10];
		this.size = 0;
		this.capacity = 10;
		this.expansionRule = 'd'; //making sure it's the default
		
	}
	
	/**
	 * This method checks to see if our stack is empty and return true or false accordingly
	 * @return boolean true or false
	 */
	public boolean isEmpty(){
		if(size==0){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method pushes the object T to our stack and places it on top of the stack
	 * It will increase the size of the stack at the same time
	 * @param t (generic object T)
	 */
	public void push(T t){
		
		checkCapacity(); //increase our capacity if needed
		stkArray[size] = t;
		this.size++; //increase our size by 1
	}
	
	/**
	 * /**
	 * This method pops the object on top of the stack and returns it to the caller
	 *
	 * @return the popped element of type T
	 * @throws EmptyStackException
	 */
	@SuppressWarnings("unchecked")
	public T pop() throws EmptyStackException {
		
		T popped;
		
		if(isEmpty()){
			throw new EmptyStackException();
		} else {
			popped = (T)this.stkArray[size-1];
			this.stkArray[size-1] = null; //set the element index we just returned to null
			this.size--; //reduce our size
		}
		
		return popped;		
	}
	
	/**
	 * This will set our expansion rule to the given parameter character c
	 * @param c  must be character 'd' for double or 'c' for constant expansion
	 * @return boolean true or false if it succeeded to set the expansion rule or not
	 */
	public boolean setExpansionRule(char c){
		char cLowerCase = Character.toLowerCase(c);
		
		if (cLowerCase == 'd' || cLowerCase == 'c'){
			this.expansionRule = cLowerCase;
			return true;
		} else {
			System.out.println("Invalid argument given, please indicate whether you want to apply double or constant rule to the expansion using 'd' or 'c'");
			System.out.println("Expansion Rule will remain at default");
			return false;
		}
		
	}
	
	/**
	 * This will check the capacity of our underlying array.  If the capacity is at 
	 * 90% or more, it will increase the capacity according to the expansion rule, that is 
	 * either the capacity will be doubled or increased by a constant size 10
	 *  	
	 */
	private void checkCapacity(){
		
		if(this.size == 0){
			return;  //exit is our size is 0
		}
		
		//if size is at 90% of the capacity, we will increase the capacity.
		double capacityPercentage = (this.size * 100.0)/this.capacity;
				
		//initialize our new parameters
		
		int newCapacity = 0;
		Object[] newStkArray;
		
		//check if our capacity is at or above 90%
		if(capacityPercentage >=90){
			if(this.expansionRule == 'd'){ //double our array as rule?
				newCapacity = this.capacity*2;
				newStkArray = new Object[newCapacity];
			
			} else { //we are increasing our array by a constant
				newCapacity = this.capacity + 10;
				newStkArray = new Object[newCapacity];
			
			}
					
			//now we will copy everything from our old array to our new array
			
			for (int i = 0; i < this.stkArray.length; i++){
				//this will only copy the references in our new array, that's what we want
				//we don't need a deep copy here because we do want to keep our stack
				newStkArray[i] = stkArray[i];
			}
				
			this.capacity = newCapacity; //set our capacity to new capacity
			//point / reference our stkArray to our new array so that old array is garbage collected
			this.stkArray = newStkArray;
			newStkArray = null; //point our temp array to null	
			hiddenIncCounter++; //increase our hidden increment counter
		}	
	}
	
	/**
	 * This method will truncate our stack to its exact size, removing all empty indices in our
	 * underlying array and making a new array that will be of exact size as the number
	 * of elements in our stack.
	 * 
	 */
	public void truncate(){
		
		int newCapacity = this.size;
		
		if(isEmpty()){
			//Stack is already empty, let's just set the capacity and array back to 10
			newCapacity = 10;
			
		}
		
		Object[] newStkArray = new Object[newCapacity];
		
		for (int i = 0; i< newStkArray.length; i++){
			//this will copy the references in our new array
			newStkArray[i] = this.stkArray[i];
		}
		
		//set our newcapacity and our new array
		this.capacity = newCapacity;
		this.stkArray = newStkArray;
		newStkArray = null;
	}
	
	/**
	 * /**
	 * This method returns a reference to the element on top of the stack without removing it. 
	 * @return
	 * @throws EmptyStackException
	 */
	public T peek() throws EmptyStackException {
		if(isEmpty()){
			throw new EmptyStackException();
		} else {
			return (T)stkArray[size-1];
		}
		
	}
	/**
	 * This method will return the current capacity of our stack
	 * @return capacity as an int
	 */
	public int getCapacity(){
		
		return this.capacity;
	}
	
	/**
	 * This method will return the size of our stack
	 * @return size as an int
	 */
	public int getSize(){
		return this.size;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String returnString = "CurrentStack: [";
		for (int i = 0; i < stkArray.length; i++){
			if(stkArray[i]!=null){
				returnString = returnString + " " + stkArray[i];
			}
		}
		returnString = returnString + " ] \nCurrent Size = " + this.size + "\tTotal Capacity = " + 
			this.capacity;
		if (this.expansionRule == 'd'){
			returnString = returnString + "\tDoubling in Size";
		} else {
			returnString = returnString + "\tIncreasing size by constant 10";
		}
		return returnString;
		
	}

	public static void breakTime(){
		
		System.out.println("\nPress enter to visualize the time analysis");
		try {
		System.in.read();
		} catch (IOException e){
			e.printStackTrace();
		}		
	} //end breakTime()
}

