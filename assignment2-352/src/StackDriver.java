import java.io.IOException;

// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 352
// Assignment #2
// Due Date: 9th of March 2014
//--------------------------------------------------------------------


public class StackDriver {

	/**
	 * Our driver for the Stack class.  Testing and Analysing
	 * @param args
	 */
	public static void main(String[] args) {
		
		//welcome message
		System.out.println("*********************************************");
		System.out.println("*        Welcome to the Stack Driver!       *");
		System.out.println("*              Programmed by                *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();

		//our parameters
		int i; //for our forloops
		long startTime; //our start time
		long elapsedTimeMillis; //elapsed time
		float elapsedTimeSec; //elapsed time in seconds
		String title, header; //our strings for pretty time table below
		//we will use these arrays below to save our time readings to show them nice and pretty
		float[] doubleArrayTime = new float[50];
		int[] doubleArraySize = new int[50];
		int[] doubleArrayCapacity = new int[50];
		float[] constantArrayTime = new float[50];
		int[] constantArraySize= new int[50];
		int[] constantArrayCapacity = new int[50];
		//we will use these two when gathering data
		int maxAmount = 0;
		int incrementalAmount = 0; 
		//our two stacks of integers
		Stack myStack = new Stack<Integer>(); 
		Stack myStackConstant = new Stack<Integer>();
		
		//Let's start testing our ADT  
		System.out.println("Creating a new stack with default capacity");
		System.out.println("This is what our stack looks like empty: " );
		System.out.println(myStack);
		
		System.out.println("\n*********************************************");
		System.out.println("We will now start pushing objects to this stack");
		System.out.println("Pushing 1, 2, 3, 4, 5 to our stack");
		myStack.push(1); myStack.push(2); myStack.push(3); myStack.push(4); myStack.push(5);
		System.out.println("Here's a look at our stack");
		System.out.println(myStack);
		System.out.println("Pushing 3 more elements");
		myStack.push(6); myStack.push(7); myStack.push(8);
		System.out.println(myStack);
		System.out.println("Popping last two elements " + myStack.pop() + " " + myStack.pop());
		System.out.println(myStack);
		System.out.println("\n*********************************************");
		System.out.println("Let's try to push our capacity now and add upto 10 elements so our array doubles");
		myStack.push(7); myStack.push(8); myStack.push(9); myStack.push(10);
		System.out.println(myStack);
		System.out.println("The Current peek is: " + myStack.peek());
		System.out.println("\n*********************************************");
		System.out.println("Now let's truncate our array back to 10");
		myStack.truncate();
		System.out.println(myStack);
		System.out.println("\n*********************************************");
		System.out.println("Changing our expansion rule now: " + myStack.setExpansionRule('c'));
		System.out.println("Adding an additional element will change our stack capacity by 10");
		myStack.push(11);
		System.out.println(myStack);
		System.out.println("The Current peek is: " + myStack.peek());
		System.out.println("Add 9 more elements to make sure it's not doubling but actually just constantly increasing");
		for (i = 12; i<22; i++){
			myStack.push(i);
		}
		System.out.println(myStack);
		System.out.println("The Current peek is: " + myStack.peek());
	
		System.out.println("\n*********************************************");
		System.out.println("Let's empty our array by popping out everything");
		for (i = 0; i<21; i++){
			System.out.print(myStack.pop() + " ");
		}
		
		System.out.println("\nIs our stack empty? " + myStack.isEmpty());
		System.out.println("Can we pop again? Should get an error but we will handle it");
		try{System.out.println(myStack.pop());
		
		} catch (Exception e) {
			System.out.println("Yay! We got an exception: " + e);
		}
		System.out.println(myStack);
		myStack.truncate(); //reset our capacity back to default 10 for the empty stack
		System.out.println(myStack);
		
		
		System.out.println("\n*********************************************");
		System.out.println("We will now Time our array increases");
		System.out.println("Please wait while the calculations are made.  This may take a few seconds");
		
		//set the expansion rules for both stacks
		myStack.setExpansionRule('d'); //for double
		myStackConstant.setExpansionRule('c'); //for constant increase
		
		maxAmount = 250000;
		incrementalAmount = 5000;
		
		//let's gather data for doubling inrement strategy
		startTime = System.currentTimeMillis(); //set our starttime to measure total time it takes
		for(i=1; i<=maxAmount; i++){
			elapsedTimeMillis = System.currentTimeMillis()-startTime;
			elapsedTimeSec = elapsedTimeMillis/1000F;
			
			myStack.push(i);
			
			//let's record our data for printing later on
			if (i % incrementalAmount == 0){
				doubleArrayTime[(i/incrementalAmount)-1] = elapsedTimeSec;
				doubleArraySize[(i/incrementalAmount)-1] = myStack.getSize();
				doubleArrayCapacity[(i/incrementalAmount)-1] = myStack.getCapacity();
			}   
		}
		System.out.println("How many increments for doubling strategy? : " + myStack.hiddenIncCounter);
		
		//let's gather data for constant incremental strategy
		startTime = System.currentTimeMillis();
		for(i=1; i<=maxAmount; i++){
			elapsedTimeMillis = System.currentTimeMillis()-startTime;
			elapsedTimeSec = elapsedTimeMillis/1000F;
			myStackConstant.push(i);
			
			//let's record our time for printint later on
			if (i % incrementalAmount == 0){
				constantArrayTime[(i/incrementalAmount)-1] = elapsedTimeSec;
				constantArraySize[(i/incrementalAmount)-1] = myStackConstant.getSize();
				constantArrayCapacity[(i/incrementalAmount)-1] = myStackConstant.getCapacity();
			}
		}
		System.out.println("How many increments for constant strategy? : " + myStackConstant.hiddenIncCounter);
		
		//let's look at how long it took for the doubling strategy
		
		Stack.breakTime();
		
		title = ("\nAlgorithm Time Analysis for Stack Doubling increase strategy\n" + 
	        	"-----------------------------------------------------------" );
		header = String.format("%-15.30s %-15.30s %10s%n", "Size", "Total Capacity", 
				"Elapsed Time(Seconds)");
		System.out.println(title);
		System.out.println(header);
		
		for(i=0; i<50; i++){	
			String line = String.format("%-15d %-15d %10f", doubleArraySize[i], 
					doubleArrayCapacity[i], doubleArrayTime[i]);
			System.out.println(line);
		}
		
		
		//let's look at how long it took for constant incremental strategy
		
		Stack.breakTime();
		
		title = ("\nAlgorithm Time Analysis for Stack Constant Incremental strategy\n" + 
	        	"-----------------------------------------------------------" );
		header = String.format("%-15.30s %-15.30s %10s%n", "Size", "Total Capacity", 
				"Elapsed Time(Seconds)");
		System.out.println(title);
		System.out.println(header);
		
		for(i=0; i<50; i++){	
			String line = String.format("%-15d %-15d %10f", constantArraySize[i], 
					constantArrayCapacity[i],
					constantArrayTime[i]);
			System.out.println(line);
		}
		
		//let's Compare times
		Stack.breakTime();

		title = ("\nAlgorithm Time Comparison for Stack Incremental strategy\n" + 
	        	"-----------------------------------------------------------" );
		header = String.format("%-30.60s %10s %10s%n", ""
				+ "Size Increment",   
				"Doubling Time", "Constant Time");
		System.out.println(title);
		System.out.println(header);
		
		for(i=1; i<50; i++){	
			String line = String.format("%-30.60s %10.3f %10.3f", 
					"From " + ((i-1)*incrementalAmount) +" to "+ (i*incrementalAmount) + " ", 
					doubleArrayTime[i]-doubleArrayTime[i-1], 
					constantArrayTime[i]-constantArrayTime[i-1]);
			System.out.println(line);
		}
		
	} //end main


} //end class
