//mixture of static methods to test code

public class StaticMethods {

	//exercise 1 chap 11 
	public static void cheers(int n){
		if (n == 1){
			System.out.println("Hurray");
		} else
		{
			System.out.println("Hip");
			cheers(n-1);
		}
	}
	
	//itirative version of the above method
	public static void cheersIti(int n){
		for (int i = 1; i < n; i++){
			System.out.println("Hip");
		}
		System.out.println("Hurray");
	}
	
	
	//exercise 2 chap 11 recursive method that takes int n as an argument and outputs 
	//numbers of stars
	public static void printStars(int n){
		if (n == 1){
			System.out.print("*");
			System.out.println();
		} else {
			System.out.print("*");
			printStars(n-1);
		}
	}
	
	//itirative version of the above method
	public static void printStarsIti(int n){
		for (int i = 1; i <=n; i++){
			System.out.print("*");
		}
		System.out.println();
	}
	
	//exercise 3 chap 11 .. recursive method output 1234 into 4321
	public static void reverseInt(int n){
		if (n<10){
			System.out.print(n);
			System.out.println();
		} else {
			System.out.print(n%10);
			reverseInt(n/10);
		}
	}
	
	//itirative version of the above method
	public static void reverseIntIti(int n){
		//count number of digits in the number
		int digitCounter = 0;
		int x = n;
		while (x != 0){
			x = x/10;
			digitCounter++;
		}
		
		for (int i = digitCounter; i > 0; i--){
			System.out.print(String.valueOf(n).charAt(i-1));
		}
	}
	
	
	//exercise 4 chap 11 .. takes an int and outputs 1,2,.....,n to the screen
	public static void expandInt(int n){
		if (n == 1){
			System.out.print(n);
		} else {
			expandInt(n-1);
			System.out.print(", " + n);
		}
	
	}
	
	
	
	//exercise 5 chap 11 takes and int and outputs n,n-1,n-2,n-3
	public static void expandIntDown(int n){
		if (n>=1)
		{
			System.out.print(n + " "); //write while the recursion winds
			expandIntDown(n - 1);
		}
	}
	
	

}
