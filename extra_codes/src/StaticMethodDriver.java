//driver to test my StaticMethods
public class StaticMethodDriver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StaticMethods.cheers(3);
		StaticMethods.printStars(5);
		StaticMethods.reverseInt(123456789);
		StaticMethods.expandInt(10);
		System.out.println("\nExpandIntDown:");
		StaticMethods.expandIntDown(10);
		System.out.println();
		StaticMethods.cheersIti(3);
		StaticMethods.printStarsIti(5);
		StaticMethods.reverseIntIti(55554234);
	}
	

}
