//This program was a question in the final exam of Comp 248 at Concordia University Fall 2012
//the question asked to make a method called "findLongest" which would find the 
//longest row in a given integer ragged array and return it's index number in a system.out.println
//along with the sum of all the digits in that row.
//the function findLongest does exactly that.  The main function below creates a random ragged
//array to test the function findLongest. 

import java.util.*;

public class FindLongest {

	public static int findLongest(int[][] a){
		int sum = 0;
		int length = 0;
		int i = 0;
		int index = -1;
		
		for (i = 0; i < a.length; i++){
			if (length < a[i].length){
				length = a[i].length;
				index = i;
			}
		}
		
		for (i = 0; i < a[index].length; i++){
			sum += a[index][i];
		}
		
		System.out.println("The longest row is at Index # " + index);
		
		return sum;
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Random rand = new Random();
		
		int[][] testArr = new int[rand.nextInt(10)+1][];
		int i = 0;
		
		
		for (i = 0; i < testArr.length; i++){
			int size = rand.nextInt(50)+100;
			testArr[i] = new int[rand.nextInt(10)+1]; 
		} //end first for loop to create a ragged array
		
		System.out.println("Our ragged array has " + testArr.length + " rows and each row has a random number of columns between 1 and 10");
		System.out.println("Here's our populated ragged array: \n");
		for (i = 0; i<testArr.length; i++){
			for (int j = 0; j<testArr[i].length; j++){
				testArr[i][j] = rand.nextInt(10);
				System.out.print(testArr[i][j] + "  ");
			} //end internal loop
			System.out.println("");
		}//end second for loop to populate the ragged array
		
		int sum = findLongest(testArr);
		System.out.println("And the sum of all the numbers in that row is: " + sum);
		

	} //end main

} //end class
