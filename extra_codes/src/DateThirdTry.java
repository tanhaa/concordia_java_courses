import java.util.*;

public class DateThirdTry {

	private String month;
	private int day;
	private int year;
	
	public void writeOutput()
	{
		System.out.println(this.month + " " + this.day + ", " + this.year);
	}
	
	public void readInput()
	{
		Scanner kb = new Scanner(System.in);
		System.out.println("Enter month, day, and year.");
		System.out.println("Do not use a comma.");
		month = kb.next();
		day = kb.nextInt();
		year = kb.nextInt();
		kb.close();
		
	}
	
	public void setDate(int newMonth, int newDay, int newYear)
	{
		this.month = monthString(newMonth);
		this.day = newDay;
		this.year= newYear;
		
	}
	
	public String monthString(int monthNumber)
	{
		switch (monthNumber)
		{
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4: 
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";
		default:
			System.out.println("Fatal Error");
			System.exit(0);
			return "Error" ; // to keep the compiler happy
				
		}
		
	}
	public int getDay()
	{
		return this.day;
	}
	public int getYear()
	{
		return this.year;
	}
	public int getMonth()
	{
		if (this.month.equalsIgnoreCase("January"))
			return 1;
		else if (this.month.equalsIgnoreCase("February"))
			return 2;
		else if (this.month.equalsIgnoreCase("March"))
			return 3;
		else if (this.month.equalsIgnoreCase("April"))
			return 4;
		else if (this.month.equalsIgnoreCase("May"))
			return 5;
		else if (this.month.equalsIgnoreCase("June"))
			return 6;
		else if (this.month.equalsIgnoreCase("July"))
			return 7;
		else if (this.month.equalsIgnoreCase("August"))
			return 8;
		else if (this.month.equalsIgnoreCase("September"))
			return 9;
		else if (this.month.equalsIgnoreCase("October"))
			return 10;
		else if (this.month.equalsIgnoreCase("November"))
			return 11;
		else if (this.month.equalsIgnoreCase("December"))
			return 12;
		else {
			System.out.println("Fatal Error");
			System.exit(0);
			return 0; // keeps compiler happy
		}
		
	}
	public int getNextYear()
	{
		int nextYear = year + 1;
		return nextYear;
	}

	public void happyGreeting()
	{
		int i;
		for (i = 1; i <= day; i++)
		{
			System.out.println("Happy Days!");
		}
	}
	public double fractionDone(int targetDay)
	{
		//double newDay = (double)day;
		return (double)day/targetDay;
	}
	public void advanceYear(int futureYears)
	{
		this.year = this.year + futureYears;
	}
	public boolean precedes(DateThirdTry otherDate)
	{
		return ( 	(this.year < otherDate.year) || 
					(this.year == otherDate.year && this.getMonth() < otherDate.getMonth()) || 
					(this.year == otherDate.year && this.month.equals(otherDate.month) && this.day < otherDate.day)	);
	}
	
	//these are default methods re-written
	public String toString()
	{
		return (month + " " + day + ", " + year);
	}
	
	public boolean equals(DateThirdTry otherDate)
	{
		return (	(this.month.equals(otherDate.month)) &&
					(this.day == otherDate.day) &&
					(this.year == otherDate.year)	);
	}
	
}
