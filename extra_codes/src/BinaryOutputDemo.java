import java.io.*;
import java.util.Scanner;

public class BinaryOutputDemo
{
   public static void main(String[] args)
   {
   	  ObjectOutputStream outputStream = null;  //declare the object outside the try so that in the finally block you do have a handle
        //if outputStream has been declared inside the try block, would it available outside? no because of the local scope
        //why initialize it to null because the constructor may throw an IO Exception, so you declare your pointer outside
        //and then you attempt to create the object inside the try block. 
      try
      {
         outputStream = new ObjectOutputStream(
         					new FileOutputStream("numbers.dat"));
         int n;
         Scanner keyboard = new Scanner(System.in);

         System.out.println("Enter nonnegative integers.");
         System.out.println("Place a negative number at the end.");

         do  //ask the user to give a bunch of positive integers and put them in the outputStream (binary file)
         {
            n = keyboard.nextInt();
            outputStream.writeInt(n);
         }
         while (n >= 0);
        
         System.out.println("Numbers and sentinel value");
         System.out.println("written to the file numbers.dat.");
      }
      catch(IOException e)
      {
         System.out.println("Problem with output to file numbers.dat.");
      }
      finally {
      	try {
      		if (outputStream != null)
      		outputStream.close( );
      	}
      	catch(IOException e) {
           System.out.println("Can't seem to close the file...");
        }
      }
   }
}