import java.util.Scanner;
public class loops {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int i;
		System.out.println("Pattern 1: ");
		for (i = 1; i <=5; i++)
		{
			for (int j = 5; j>=i; j--)
			{
				System.out.print(j + " " );
			}
			System.out.println();
		}
		System.out.println("Pattern 2: ");
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter value: ");
		int x = kb.nextInt();
		
		if (x%2 == 0)
		{
			for (i=1;i<=x/2;i++)
			{
				for(int j=x; j>=2*i; j=j-2)
				{
					System.out.print(j + " ");
				}
				System.out.println();
			}
		}
		else 
		{
			for (i=1;i<=(x/2 + 1); i++)
			{
				for (int j=x; j>=(2*i)+1; j=j-2)
				{
					System.out.print(j + " ");
					
				}
				System.out.println();
			}
		}
	}

}
