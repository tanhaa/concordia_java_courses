// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 352
// Assignment #1
// Tail recursive implementation of Tetranacci numbers
//--------------------------------------------------------------------

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;



public class TailRecursiveTet {
	
	public static double tailRecursiveTet(int n, double a, double b, double c, double d ){
		//we will require our method to be passed with some default values
		if (n <= 2) 
			return 0; //base case
		if (n == 3) 
			return a; //second base case
		else 
			return tailRecursiveTet(n-1, a+b+c+d, a, b, c); //recursive call 
		
	}
		

	public static void main(String[] args) throws FileNotFoundException {
		long startTime;  //initialize our measurement value
		int n;
		double tetVal;  //to store our results
		
		String title, header;  //for printing
		
		//our printwriter object
		PrintWriter out = new PrintWriter(new FileOutputStream(new File("outtailrecursive.txt")));
		
		title = ("\nAlgorithm Time Analysis for Tail Recursive Tetranacci Algorithm\n" + 
		        	"-----------------------------------------------------------\n" );
		header = String.format("%-15.30s %-30.30s %20s%n", "Integer", "Tetranacci Value", "Elapsed Time (seconds)");

		out.print(title);
		out.print(header);
		System.out.println(title);
		System.out.println(header);

		//we will run our tail recursive function within this loop
		//this will give us tet values for 0,5,10,15....95,100
		for(n=0; n<=100; n+=5){  
			startTime = System.currentTimeMillis();
			//capture our resulting array
			tetVal = tailRecursiveTet(n, 1,0,0,0);	//we must make the call with the base case for number 3
			
			long elapsedTimeMillis = System.currentTimeMillis()-startTime;
			float elapsedTimeSec = elapsedTimeMillis/1000F;

			//  Get elapsed time in minutes 
	        float elapsedTimeMin = elapsedTimeMillis/(60*1000F); 
	        // Get elapsed time in hours 
	        float elapsedTimeHour = elapsedTimeMillis/(60*60*1000F); 
	        // Get elapsed time in days 
	        float elapsedTimeDay = elapsedTimeMillis/(24*60*60*1000F);
	        
	        //prepare our output
	        String line = String.format("%-15d %-30.0f %20f%n", n, tetVal, elapsedTimeSec);
	        out.print(line);
	        out.flush();
	        System.out.println(line);
	      
		}  

		
		//flush and close our file 
		out.flush();
		out.close();
		
	} //end Main

} //end class