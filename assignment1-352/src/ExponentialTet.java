// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 352
// Assignment #1
// Exponential multiple recursion implementation of Tetranacci numbers
//--------------------------------------------------------------------


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;


public class ExponentialTet {
	
	public static double exponentialTet(int n)
	{
		
		//initialize variables
		double tetVal = 0;  //this is what we will return as our computed value
		
		//Here's our main body
		if (n <= 2)  //base case
		{
			tetVal = 0;
		} else if (n == 3) {  //second base case
			tetVal = 1;
		} else {
			//this is where multiple recursion calls are made
			tetVal = exponentialTet(n-1) + exponentialTet(n-2) + 
					exponentialTet(n-3) + exponentialTet(n-4);
		}
					
		return tetVal;  //our computed value
		
	} //end exponentialTet

	public static void main(String[] args) throws FileNotFoundException {
		
		//initialize variables
		long startTime;
		int n; 
		double tetVal;
		String title, header;
		
		//our printwriter object
		PrintWriter out = new PrintWriter(new FileOutputStream(new File("outexponential.txt")));
				
		//we are gonna save everything we need to print into strings to send out our pw object
		title = ("\nAlgorithm Time Analysis for exponential Tetranacci Algorithm\n" + 
		        	"-----------------------------------------------------------\n" );
		header = String.format("%-15.30s %-30.30s %20s%n", "Integer", "Tetranacci Value", "Elapsed Time(Seconds)");
		
		//output our title and header to our file
		out.print(title);
		out.print(header);
		//output to screen
		System.out.println(title);
		System.out.println(header);
		
		for(n=0; n<=45; n=n+5){
			
			startTime = System.currentTimeMillis();
			tetVal = exponentialTet(n);
			long elapsedTimeMillis = System.currentTimeMillis()-startTime;
			float elapsedTimeSec = elapsedTimeMillis/1000F;
			//  Get elapsed time in minutes 
	        float elapsedTimeMin = elapsedTimeMillis/(60*1000F); 
	        // Get elapsed time in hours 
	        float elapsedTimeHour = elapsedTimeMillis/(60*60*1000F); 
	        // Get elapsed time in days 
	        float elapsedTimeDay = elapsedTimeMillis/(24*60*60*1000F);

	        String line = String.format("%-15d %-30.0f %20f%n", n, tetVal, elapsedTimeSec);
	        out.print(line);
	        out.flush();
	        System.out.println(line);
		}
		
		out.close();
	} //end Main



} //end class ExponentialTet
