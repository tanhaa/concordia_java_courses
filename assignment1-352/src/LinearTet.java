// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 352
// Assignment #1
// Linear Recursive method for Tetranacci numbers. 
//--------------------------------------------------------------------

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;


public class LinearTet {
	
	public static double[] linearTet(int n)
	{
		double[] A = new double[4];  //we will return this array of ints with all our values
		double i, j, k, l; //initialize our floats

		if(n<=2){ //base case for less than 0
			i=0;j=0;k=0;l=0;  // set all values to zero
			A[0] = i; A[1] = j; A[2] = k; A[3] = l;
			return (A);		// this will return (1,0,0,0)
			
		} else if (n==3){  //this is the base case
			
			i=1; j=0; k=0; l=0;  //set all values to 0 except i = 1
			A[0] = i; A[1] = j; A[2] = k; A[3] = l;
			return (A);		// this will return (1,0,0,0)
			
		} else {  //our recursive case
			
			A = linearTet(n - 1);  //make a recursive call
			i = A[0]; j = A[1]; k = A[2]; l = A[3];  
			A[0] = i+j+k+l; //this will be the addition of the four numbers
			//below, we will push our array by one digit. 
			A[1] = i;
			A[2] = j;
			A[3] = k;
			return (A);		// this will return (i+j+k+l,i,j,k)
		}
	}//end linearTet
	
	public static void main(String[] args) throws FileNotFoundException {
		
		long startTime;  //initialize our measurement value
		int n;
		double[] tetVals;  //to store our results
		
		String title, header;  //for printing
		
		//our printwriter object
		PrintWriter out = new PrintWriter(new FileOutputStream(new File("outlinear.txt")));
		
		title = ("\nAlgorithm Time Analysis for Linear Tetranacci Algorithm\n" + 
		        	"-----------------------------------------------------------\n" );
		header = String.format("%-15.30s %-30.30s %20s%n", "Integer", "Tetranacci Value", "Elapsed Time (seconds)");

		out.print(title);
		out.print(header);
		System.out.println(title);
		System.out.println(header);
		
		//we will run our recursive function within this loop
		//this will give us tet values for 0,5,10,15....95,100
		for(n=0; n<=100; n+=5){  
			startTime = System.currentTimeMillis();
			//capture our resulting array
			tetVals = linearTet(n);
			
			long elapsedTimeMillis = System.currentTimeMillis()-startTime;
			float elapsedTimeSec = elapsedTimeMillis/1000F;

			//  Get elapsed time in minutes 
	        float elapsedTimeMin = elapsedTimeMillis/(60*1000F); 
	        // Get elapsed time in hours 
	        float elapsedTimeHour = elapsedTimeMillis/(60*60*1000F); 
	        // Get elapsed time in days 
	        float elapsedTimeDay = elapsedTimeMillis/(24*60*60*1000F);
	        
	        //prepare our output
	        String line = String.format("%-15d %-30.0f %20f%n", n, tetVals[0], elapsedTimeSec);
	        out.print(line);
	        out.flush();
	        System.out.println(line);
	      
		}  

		
		//flush and close our file 
		out.flush();
		out.close();
		
	} //end Main

} //end Class
