// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #4 Part 1
// Due Date: 12 of April 2013
//--------------------------------------------------------------------

package part1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import part1.Word.WordType;

/**
 * This class contains all the methods we need to run our main driver
 * to carry out Zipf's law.
 * 
 * @author Amit
 *
 */
public abstract class Zippys {
	
	//our static scanner to ask for input
	public static Scanner kb = new Scanner(System.in);
	
	
	/**
	 * This method asks client for a file name, checks if the filename exists and returns
	 * the File object with that name for further processing.
	 * 
	 * @return	an object of the type File
	 */
	public static File askInputFile(){
		
		
		boolean validName = false; // flag to check for validfilename
		String name = ""; //to store our file name the user enters
		File n = null;  //initialize our file variable
		
		// ask user for an Input file name
		System.out.print("Please Enter a name for the input file:");

		// our loop will check if the user entered a valid file name or not
		while (!validName) {
			try {
				name = kb.next();
				n = new File(name); // new File object with the name user provided
				if (!n.exists()) { // check if a file with that name already exists

					System.out.println("Error: The file " + name + " could not be found.");
					// ask again
					System.out.print("Please Enter another name for the input file:");
				} else {
					validName = true; // our flag to end our loop
				} // close if-else

			} catch (InputMismatchException e) {
				kb.nextLine(); // catch remove junk
				System.out.println("An error was encountered, please try again.");
			} catch (Exception e) {
				kb.nextLine(); // catch remove junk
				System.out.println("An error was encountered with the file name you entered, please try again.");
			}

		} // close while loop
		
		return n; //return the name of the file

	} // close askInputFile() method
	
	
	/**
	 * This method loads an object of the type File and attempts to open it using the 
	 * Scanner class for further processing. 
	 * 
	 * @param f		an object of the type File
	 * @return		an object of the type Scanner
	 */
	public static Scanner openFile(File f){
		
		Scanner inputFile = null; //declare our Scanner variable outside the try-catch block
		
		if (!f.exists()) {
			System.out.println("The file you are attempting to import does not exist.");
			System.out.println("The program will now terminate. Goodbye");
			System.exit(0);
		} else {

			try {
				// create our new Scanner Object with Input File
				//System.out.println("\nAttempting to open file: " + f.getName());
				inputFile = new Scanner(new FileInputStream(f.getName()));
				
			} catch (FileNotFoundException e) {
				System.out.println("There was a problem with the file " + f.getName());
				e.printStackTrace();
			} catch (Exception e) {
				System.out.println("There was an error reading from the file " + f.getName());
				e.printStackTrace();
			}
		} // close if-else
		
		return inputFile;  //our program must remember to close the file after working with it
	} //close loadFile()
	
	
	/**
	 * This method helps us read the words in a file and saves each word in an
	 * array list. It ensures that each word is a valid word (contains only letters).
	 * 
	 * @param inputFile		an opened file, object of the Scanner class
	 * @return an ArrayList<String> of strings
	 */
	public static ArrayList<String> readWords(Scanner inputFile){
		
		ArrayList<String> list = new ArrayList<String>(); //create a new ArrayList
		
		String word = inputFile.next();
		
		while(inputFile.hasNext()){ //check for EOF
			if (checkWordValidity(word)){ //check word validity
				list.add((String) word);
			}
			word = inputFile.next(); //read the next word
		}
		
		
		return list; //return our list
		
	}
	
	
	/**
	 * This method checks if a word being passed to it contains only letters(a-z;A-Z).
	 * @param s	String that needs to be checked
	 * @return	true if valid, false if the word contains non-letter characters
	 */
	public static boolean checkWordValidity(String s){
		
		int i = 0;
		int length = s.length();
		boolean valid = true;
		
		for (i = 0; i < length; i++){
			if (s.charAt(i) < 65){
				valid = false;
			}
			if (s.charAt(i) > 90 && s.charAt(i) < 97){
				valid = false;
			}
			if (s.charAt(i) > 122){
				valid = false;
			}
		}
		return valid;
	}
	
	/**
	 * This method creates an ArrayList of Word objects reading from another
	 * ArrayList of String objects
	 * @param tokens an ArrayList<String> of words
	 * @return an ArrayList<Word> of words. 
	 */
	public static ArrayList<Word> createWords(ArrayList<String> tokens){
		
		ArrayList<Word> wordList = new ArrayList<Word>();
		int i = 0;
		int indexExisting = 0;
		int currentFrequency = 0;
		
		//let's start off our list of word objects
		wordList.add(new Word(tokens.get(0)));

		//loop to go through and see if the next object exists in our wordList  
		for (i = 1; i < tokens.size(); i++){
			Word temp = new Word(tokens.get(i));
			//is it there and if yes, where is it so we can increase its frequency
			if(wordList.contains(temp)){
				indexExisting = wordList.indexOf(temp);
				//get the current frequency of that object in our wordList
				currentFrequency = wordList.get(indexExisting).getFrequency();
				//add 1 to it
				currentFrequency++;
				//put it back in our object
				wordList.get(indexExisting).setFrequency(currentFrequency);
			} else { //means the word is not there
				wordList.add(new Word(tokens.get(i)));
			}
			
		}
		//return our new wordList
		return wordList;
		} //end createWords()
	
	/**
	 * This method allows us to set the types for each word based
	 * on the number of time it appeared in the input.txt file as well
	 * as based on its size
	 * @param listOfWords an ArrayList of Word objects
	 */
	public static void setWordType(ArrayList<Word> listOfWords){
		for (Word word : listOfWords){
			if (word.getFrequency() >= 10 && word.getSize() <=4){
				word.setWordType(WordType.STOP);
			} else {
				word.setWordType(WordType.HAPPAX);
			} //end if-else
		} //end for loop
	} //end setWordType()
	
	/**
	 * This method will generate some statistics on our ArrayList composed
	 * of Word objects, such as total tokens, total types, number of Happax
	 * and number of Stop Words.
	 * The method returns an array of integer which contains number of word
	 * tokens at location 0, number of word types at location 1,
	 * number of Happax at location 1, number of stop words
	 * at location 2  
	 * 
	 * @param listOfWords an ArrayList<Word> of word objects
	 * @return	int[] an array 
	 */
	public static int[] getStats(ArrayList<Word> listOfWords){
		//we will create an array with our four needed stats and finally return it
		int[] stats = new int[4];
		for (int i = 0; i < stats.length; i++){
			stats[i] = 0;  //initialize our int array
		}
		
		for (Word word : listOfWords){
			stats[0] += word.getFrequency();  //Total Number of Tokens
			if(word.getWordType() == WordType.HAPPAX){
				stats[2]++; //Total number of Happax 
			} else {
				stats[3]++; //Total number of Stop Words
			}

			stats[1]++;  //Total number of word types
		}
		
		return stats;
	}//end getStats()

}//end Class
	

	

