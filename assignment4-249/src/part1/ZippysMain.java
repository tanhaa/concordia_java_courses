// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #4 Part 1
// Due Date: 12 of April 2013
//--------------------------------------------------------------------

package part1;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ZippysMain {

	public static void main(String args[]){
		
		//welcome message
		System.out.println("*********************************************");
		System.out.println("*        Welcome to Zipf's law verifier!    *");
		System.out.println("*              Programmed by                *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();
	
		//input text file
		File inputFile = null;
		//File open for reading
		Scanner openedFile = null;
		//ArrayList of words we read from the input text file
		ArrayList<String> listOfTokens = new ArrayList<String>();
		//ArrayList of Word objects created from the input text words
		ArrayList<Word> listOfWords = new ArrayList<Word>();
		
		//Here's the general algorithm used in the program
		//ask for our inputFile
		inputFile = Zippys.askInputFile();
		//open this file as a Scanner object
		openedFile = Zippys.openFile(inputFile);
		//read all the words in the file
		listOfTokens = Zippys.readWords(openedFile);
		//create our word objects from this list
		listOfWords = Zippys.createWords(listOfTokens);
		//set the type for each word
		Zippys.setWordType(listOfWords);
		//Grab our statistics 
		int[] stats = Zippys.getStats(listOfWords);
		//Let's display everything now!
		Collections.sort(listOfWords); //sort our list
		System.out.println("\n------------------------------------------");
		System.out.println("RANK\t\tFREQ\t\tWORD");
		System.out.println("------------------------------------------");
		for (int i = 0; i < listOfWords.size(); i++){
			Word myWord = listOfWords.get(i);
			System.out.println(i+1 + "\t\t" + myWord.getFrequency() + "\t\t" + myWord.getToken());
		}
		System.out.println("\n------------------------------------------");
		System.out.println();
		System.out.println("Nb of word tokens: " + stats[0]);
		System.out.println("Nb of word types: " + stats[1] + "\n");		
		System.out.println("Nb of Happax: " + stats[2]);
		System.out.printf("%% of Happax: %.0f%%%n", (float)(stats[2]*100)/stats[1]);
		System.out.printf("Happax account for: %.0f%% of the text%n", (float)(stats[2]*100)/stats[0]);
		System.out.println("\nNb of stop words: " + (stats[3]));
		System.out.printf("%% of stop words: %.2f%%%n", (float)(stats[3]*100)/stats[1]);
		System.out.printf("Stop words account for: %.2f%% of the text%n", (float)(stats[3]*100)/stats[0]);
		
			
		//let's close our file that we have opened
		openedFile.close();
		
		//exit message
		System.out.println();
		System.out.println("*********************************************");
		System.out.println("*   Thanks for using Zipf's law verifer!    *");
		System.out.println("*********************************************");
		
	}
	
} //end main