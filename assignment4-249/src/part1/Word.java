// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #4 Part 1
// Due Date: 12 of April 2013
//--------------------------------------------------------------------

package part1;

/**
 * Our class to save the Word objects. Each word will be treated as an object
 * 
 * @author Amit
 *
 */
public class Word implements Comparable<Object>{
	
	/**
	 * This enum type lets us classify the word as a specific type.  A stop 
	 * word is a word that has a length of 4 characters or less and that 
	 * appears at least 10 times in the text 
	 * @author Amit
	 *
	 */
	public enum WordType{ 
		HAPPAX, STOP, UNCLASSIFIED
		};  
	
	private String token = null;
	private int frequency = 0;
	private int size = 0;
	private WordType wordType = WordType.UNCLASSIFIED;
		

	/**
	 * Default constructor. 
	 */
	public Word() {
		token = "Null";
		size = token.length();
	}
	
	/**
	 * Parametrized Constructor. 
	 * @param s of the type String
	 */
	public Word(String s){
		token = s;
		frequency = 1;
		size = s.length();
		
	}

	/**
	 * This method returns the token value of the object Word
	 * @return the token of the type String
	 */
	public String getToken() {
		return token;
	}

	/**
	 * This method sets the token value of a word
	 * @param token the token to set of the type String
	 */
	public void setToken(String token) {
		this.token = token;
	}

	
	/**
	 * @return the frequency
	 */
	public int getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the wordType
	 */
	public WordType getWordType() {
		return wordType;
	}

	/**
	 * @param wordType the wordType to set
	 */
	public void setWordType(WordType wordType) {
		this.wordType = wordType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Words [token=" + token + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Word other = (Word) obj;
		if (token == null) {
			if (other.token != null) {
				return false;
			}
		} else if (!token.equals(other.token)) {
			return false;
		}
		return true;
	}

	/**
	 * CompareTo method overrides the object CompareTo Method and compoares 
	 * each Word in terms of Frequency and if Frequency is equal then in terms
	 * of lexicography.
	 * 
	 */
	@Override
	public int compareTo(Object otherWord) {
		Word other = (Word) otherWord;
		
		int i = other.getFrequency() - this.frequency;
		
		if (i != 0){
			//the frequency is different, then sort by frequency
			return i; 
			
		} else {
			//same frequency, sort lexicographically using String compareTo
			i = this.token.compareTo(other.getToken());
		}

		return i;
	}
}
