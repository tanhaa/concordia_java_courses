// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #4 Part 2
// Due Date: 12 of April 2013
//--------------------------------------------------------------------
package part2;

import java.util.NoSuchElementException;

/**
 * This is our Linked List class that creates a list of CellPhone objects. 
 * Contains an internal node class that contains our CellPhones.  
 * @author Amit
 *
 */
public class CellList implements Cloneable{

	
	/**
	 * The inner class CellNode creates the nodes of the linked list
	 * outer class CellList.  It contains a reference to a cell
	 * phone object and another node.  CellNode is a private inner class
	 * which means it can not be used outside of the CellList class. 
	 * This will hide all objects of the Node class and avoid a privacy
	 * leak.
	 * 
	 * @author Amit
	 *
	 */
	private class CellNode implements Cloneable {
		
		//instance variables
		private CellPhone cellPhone;
		private CellNode nodePointer;
		
		/**
		 * Default Constructor
		 */
		public CellNode() {
			this.cellPhone = null;
			this.nodePointer = null;
		}
		
		
		/** 
		 * Parametrized Constructor
		 * @param phone of the type CellPhone
		 * @param node of the type CellNode
		 */
		public CellNode(CellPhone phone, CellNode node){
			this.cellPhone = phone;
			this.nodePointer = node;
			
		}
		
		/**
		 * Makes a deep copy of the node being passed using a helper function.
		 * @throws NullPointerException if the node being passed is null
		 * @hrows ClassCastException if the node being passed is not a CellNode
		 * 
		 * @param n of the type CellNode
		 */
		public CellNode(CellNode n){
			
			//PRIVACY LEAK NOTICE:  The copy constructor can potentially cause
			//a privacy leak because it refers to other mutable objects. It should be 
			//noted that being a private inner class, we have patched this leak as 
			//our class can not be used by the outside world.  However, based on the
			//functionality of this class, a copy constructor is not needed. 
			
			if (n == null) { //does the object exist?
				System.out.println("The object you want to copy does not exist.");
				throw new NullPointerException();
			} else if (getClass() != n.getClass()){ //is it a CellPhone?
				System.out.println("The object you want to copy does not belong to same class.");
				throw new ClassCastException();
			} else { //it passed our tests
				CellNode other = (CellNode) n; //type cast our object
				//we must do a deep copy
				//the CellPhone clone method does a deep copy of a CellPhone
				n.cellPhone = (CellPhone)this.cellPhone.clone(); 
				//we will use the copyOf helper method to create 
				//a deep copy of our new nodePointer
				n.nodePointer = copyOf(this.nodePointer);
			}
		}
		/**
		 * This helper method will accept a CellNode as a parmeter and
		 * create a true deep copy of this CellNode, including a true
		 * deep copy of any CellNode that are linked to it and return
		 * this copied node which will also link to a deep copy of any node
		 * that was linked to the original node. 
		 * 
		 * @param nodePointer2 of the type CellNode
		 * 
		 * @return CellNode (copy of the node being passed)
		 */
		private CellNode copyOf(CellNode nodePointer2) {
			CellNode position = nodePointer2; //
			CellNode copyNode; //will be our new copy node returned
			CellNode end = null; //we will use it at the end of new sub-chain
			
			//create this node, a true deep copy as we can use
			//the clone method that we redefined in our CellPhone
			//object as a public clone method.
			//copy the node being passed to our copyNode (along with the 
			//CellPhone object clone that it contains
			copyNode = new CellNode((CellPhone)position.cellPhone.clone(),null);
			//mark it currently as the end
			end = copyNode;
			//grab the node that is linked by our original Node
			position = position.nodePointer;
			
			//let's run a loop till we find the end, creating deep copy of each
			//node that we find.
			while(position != null){
				//copy node at position to end of new sub-chain
				end.nodePointer = new CellNode((CellPhone)position.cellPhone.clone(), null);
				end = end.nodePointer;
				position = position.nodePointer;
			}

			return copyNode;
		}


		/**
		 * The accessor method to get the cell phone contained in the node.
		 * 
		 * @return the cellPhone
		 */
		public CellPhone getCellPhone() {
			//PRIVACY LEAK NOTIFICATION:  This method returns a direct
			//reference to an object of the class CellPhone.  This creates
			//a security risk.  A deep copy of the object should be made 
			//if one really needs to return the phone contained in this node
			//otherwise, this type of method should not exist as there is no
			//need for it.  Being a private inner class helps as we won't need to
			//call this method from our list and it won't be accessible to the 
			//world. 
			return cellPhone;
		}

		/**
		 * This mutator method allows one to set the cell phone object contained
		 * in this node to the one that's being passed as a parameter.
		 * @param cellPhone the cellPhone to set
		 * @throws NullPointerException if the object being passed is null.
		 */
		public void setCellPhone(CellPhone cellPhone) {
			if (cellPhone == null){
				System.out.println("The cellPhone you are passing does not exist.");
				throw new NullPointerException();
			}
			this.cellPhone = cellPhone;
		}

		/**
		 * This method returns the NodePointer for the following linked node.
		 * 
		 * @return the nodePointer
		 */
		public CellNode getNodePointer() {
			//Privacy Leak Notice: Methods that return direct reference to an
			//object should be handeled carefully. Because CellNode is a private inner
			//class, it helps us patch this problem of returning direct references.
			
			return nodePointer;
		}

		/**
		 * This mutator method sets the nodePointer for the following node to
		 * the parameter that's being passed. 
		 * 
		 * @param nodePointer the nodePointer to set
		 * @throws NullPointerException if the node being passed is null
		 */
		public void setNodePointer(CellNode nodePointer) {
			if(nodePointer == null){
				System.out.println("The node being passed does not exist.");
				throw new NullPointerException();
			}
			this.nodePointer = nodePointer;
		}
		
		/**
		 * The clone() method overrides the Object.clone() and follows the
		 * Java specifications by creating a true deep copy of the CellNode
		 * being cloned. 
		 */
		@Override
		public Object clone(){
			// PRIVACY LEAK NOTICE:  The method returns direct references
			// to other objects, specifically another node and should 
			// be avoided.  There is no need for a clone method to be
			// in this class. The outerclass should be able to clone 
			// the list. 
			try{
				CellNode copy = (CellNode)super.clone();
				//now we must ensure a deep copy of each object
				copy.cellPhone = (CellPhone)this.cellPhone.clone();
				//we will use our helper copyOf function to get a true
				//deep copy
				copy.nodePointer = copyOf(this.nodePointer);
				//return this true deep copy clone 
				return copy;
			} catch (CloneNotSupportedException e){
				return null;
			}	
		}
	}//end Inner Class CellNode
	
	//outer-class attributes
	private CellNode head; //points to the first node in the list object
	private int size; //indicates the size of the list
	
	/**
	 * Default Constructor
	 */
	public CellList(){
		this.head = null;
		this.size = 0;
	}
	
	/**
	 * Copy Constructor does a deep copy of the list
	 * @param obj
	 */
	public CellList(Object obj){
		//let's check if our object is valid
		checkObject(obj);
		//type cast our object if it passes
		CellList list = (CellList) obj; 
		if (list.head == null){
			this.head = null;
		} else {
			this.head = (CellNode)list.head.clone();
		}
		
		this.size = list.size;
	} //end copy constructor
	
	/**
	 * Helper function to check object validity.
	 * Avoids repetition of code.
	 * 
	 * @param obj
	 */
	private void checkObject(Object obj) {
		if (obj == null) { //does the object exist?
			System.out.println("The object you want to copy does not exist.");
			throw new NullPointerException();
		
		}
	}

	/**
	 * This method accepts a CellPhone object and adds it to the start of the 
	 * linked list.  
	 * 
	 * @param obj (of the type CellPhone)
	 */
	public void addToStart(Object obj){
		checkObject(obj);
		CellPhone phone = (CellPhone) obj; //typecast our object as phone
		this.head = new CellNode(phone, head);
		this.size++;
			
	}//end addToStart()
	
	/**
	 * This method accepts an object of the CellPhone class along with
	 * an index value from 0 to size-1 and inserts the object at that 
	 * index.  All other objects are shifted appropriately to make room
	 * for this new object. 
	 * 
	 * @param obj of the type Phone
	 * @param index location in the list to add the Phone
	 * @throws NoSuchElementException when index provided does not exist in the list
	 */
	public void insertAtIndex(Object obj, int index){
		try{
			if (index >= this.size){
				throw new NoSuchElementException();
			} else {
				checkObject(obj);
				CellPhone phone = (CellPhone) obj; //typecast our object as phone
				if (index == 0){ //special case, we want to create a new head
					this.addToStart(phone);
				} else if (index == this.size-1){ //special case, we want to insert at the second last position
					//if the list size is 5 and we call this method with 4 index
					//it means we want to insert at that position, inserting there will move
					//current node at the index 4 to index 5 as the size of the list will increase
					CellNode temp = this.head;
					while(temp.nodePointer.nodePointer != null){ //let's find the second last node
						// as the last node will have a null pointer
						temp = temp.nodePointer;
					}
					//we found the second last node, so let's add a new CellNode
					//which links to the current end of the node (between second last and last)
					temp.nodePointer = new CellNode(phone,temp.nodePointer);
					temp = null; //get rid of temp
					this.size++; //must increase our size
				} else { //adding it anywhere in the middle
					int count = 0;
					CellNode temp = this.head;
					CellNode before, after;
					while(count != index-1){ //let's find our before node
						temp = temp.nodePointer;
					}
					before = temp; //save our before node
					after = before.nodePointer; //this is our after node
					before.nodePointer = new CellNode(phone, after); //insert our node
					//note our new node is linked with the one before
					//and links to the one after
					//get rid of our temp variables
					temp = null; before = null; after = null;
					this.size++;//must increase our size
				}
			}
			
		} catch (NoSuchElementException e){
			System.out.println("Invalid Index Size");
			e.printStackTrace();
			System.exit(0);
		}
	} //end insertAtIndex()
	
	
	/**
	 * This method accepts an index parameter  and deletes the node that is
	 * at that given index.  
	 * 
	 * @param index (Integer)
	 */
	public void deleteFromIndex(int index){
		try{
			if (index >= this.size){
				throw new NoSuchElementException();
			} else {
				if (index == 0){ //special case, we want to delete our head 
					this.deleteFromStart(); //let's call our method to delete the head
				} else if (this.size-1 == index){ //special case, we want to delete at the end
					CellNode temp = this.head;
					while(temp.nodePointer.nodePointer != null){ //let's find the second last node
						//the last node will have a null pointer
						temp = temp.nodePointer;
					}
					//we found the second last node, so let's get rid of last one
					temp.nodePointer = null; //last node is gone!
					temp = null; //get rid of temp
					this.size--;//must decrease our size
				} else { //deleting it anywhere in the middle
					int count = 0;
					CellNode temp = this.head;
					CellNode before, after;
					while(count != index-1){ //let's find our before node
						temp = temp.nodePointer;
					}
					before = temp; //save our before node
					after = before.nodePointer.nodePointer; //this is our after node
					before.nodePointer = after; //get rid of the node at index
					//note our before node is now linked to the one after
					//so the one we wanted to delete is gone
					//get rid of our temp variables
					temp = null; before = null; after = null;
					this.size--;//must decrease our size
				}
			}
		} catch (NoSuchElementException e){
			System.out.println("Invalid Index Size");
			e.printStackTrace();
			System.exit(0);
		}
	} //end deleteAtIndex()
	
	/**
	 * This method deletes the first node of the list and so the second node
	 * becomes the head of the list.  If the list only has one node, then this node
	 * will turn the list empty. 
	 */
	public void deleteFromStart(){
		if (this.head == null){ //special case list is empty
			System.out.println("The List is empty, there is nothing to delete.");
		} else {
			this.head = head.nodePointer;
			this.size--;//must decrease our size
		}
	} //end deleteFromStart()
	
	/**
	 * This method will accept a phone and an index as a parameter and replace the 
	 * current node at that index. The index must be between 0 and size-1
	 * @param obj (of the class phone)
	 * @param index integer between 0 and size-1
	 */
	public void replaceAtIndex(Object obj, int index){
		if (index >= this.size){
			return;
		} else {
			checkObject(obj);
			CellPhone phone = (CellPhone) obj; //typecast our object as phone
			if (index == 0 && size >= 1) { //special case, head exists and we want to replace our head
				head = new CellNode(phone, head.nodePointer);
			} else if (index == this.size-1){ //special case, we want to replace the last node
				CellNode temp = this.head;
				while(temp.nodePointer.nodePointer != null){ //let's find the second last node
					temp = temp.nodePointer; //save it
				}
				//we found the second last node, so let's point it to new end
				temp.nodePointer = new CellNode(phone, null); 
				temp = null; //get rid of temp
			} else { //replacing anywhere in the middle
				int count = 0;
				CellNode temp = this.head;
				CellNode before, after;
				while(count != index-1){ //let's find our before node
					temp = temp.nodePointer;
				}
				before = temp; //save our before node
				after = before.nodePointer.nodePointer; //this is our after node
				before.nodePointer = new CellNode(phone, after); //replace our node
				//note our node has been replaced, it is linked by one before
				//and links to the one after
				//get rid of our temp variables
				temp = null; before = null; after = null;
			}
		}
	} //end replaceAtIndex
	
	/**
	 * This method will search for a given serial number and return the reference to the node
	 * that contains that serial number.  It will print the number of number of iterations it took
	 * the method to find the node (or how many iterations it did in the list even if it didn't find
	 * the node) if the value of "true" is passed along with the serial number. 
	 * 
	 * @param serialNum (long)
	 * @param show  (settting this to true will show the numbe of iterations it took the method to find the node)
	 * @return Reference to a CellNode object
	 */
	public CellNode find(long serialNum, boolean show){
		
		//PRIVACY LEAK THIS is an important privacy leak as the node returned here 
		//is a direct reference to a node in our list and this will compromise our list
		//as someone can destroy the rest of the link by simply setting it's pointer to null. 
		//Ways to avoid this privacy leak is by ensuring if you absolutely have to return the 
		//reference to the node that you use the clone method in the node, which creates 
		//a true deep copy and return that.  In fact this method is not needed at all
		//and should not be created in the first place. We can also make ths method private
		//so it can be used by our Contains() method. 
		
		CellNode temp = this.head;
		int counter = 0;
		if (temp == null){ //we have an empty list
			return temp;
		} else {
			//let's check our list 
			//the node we are checking is not null (we searched everywhere
			//and the serial number of the phone at that node is not what we 
			//are looking for
			while(temp != null && temp.cellPhone.getSerialNum() != serialNum){
				temp = temp.nodePointer;
				counter++; //keep track of iterations
			}
			//this loop will end when we find the serial number of when we 
			//have exhausted our search and temp now points to null.
		}
		if (show == true){
			System.out.println("Number of iterations: " + counter);
		}
		
		//return our temp variable that either points to the node which has
		//that serialnumber or is null.  Privacy Leak.
		return temp;
	}//end find()
	
	/**
	 * This method returns true or false based on whether the serial number being passed
	 * to it is present in the list or not.  It also accepts a second boolean value which
	 * when set to true will show the number of iterations that was done by the method
	 * before finding /or not finding the item. 
	 * 
	 * @param serialNum  (long)
	 * @param show  (boolean true = show iteratons, false = don't show iterations)
	 * @return boolean (true = found serial number, false = didn't find serial number)
	 */
	public boolean contains(long serialNum, boolean show){
		//we can find the node that contains this serialnumber
		//if it is in the list, the method will return a pointer
		//to the node that contains it, or null if not present
		CellNode temp = this.find(serialNum, show);
		
		if (temp == null){ //we didn't find it or our list is empty
			return false;
		} else { //we found it and temp points to a node
			return true;
		}
	}//end contains()
	
	/**
	 * This method prints the contents of the list on the standard output.
	 * 
	 */
	public void showContents(){
		//go through the list and use the toString of each phone object
		CellNode temp = head; //set our starting point
		while (temp != null){
			System.out.print(temp.cellPhone + " ---> ");
			temp = temp.nodePointer; //move to the next one
		}
		System.out.println("x");
	}

	/**
	 * This method will allow us to get the size of the list
	 * 
	 * @return size of type int
	 */
	public int getSize(){
		return size;
	}
	/**
	 * The equal method overrides the Object.equals.  
	 * When checking for equality in both lists, this method ensures
	 * that each list contains the same CellPhones in the same order. 
	 * The method equals as defined in the CellPhone class is used
	 * and two CellPhones are considered equal even if they don't
	 * have the same serial number. 
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CellList other = (CellList) obj;
		if (size != other.size) {
			return false;
		}
		
		//check every node one by one to make sure
		//they all have the same cellphones (traverse the list)
		CellNode position = head;
		CellNode otherPosition = other.head;
		
		while (position != null){
			if(!(position.cellPhone.equals(otherPosition.cellPhone)))
				return false;
			position = position.nodePointer;
			otherPosition = otherPosition.nodePointer;
		}
		
		//we didn't return false, must be equal lists 
		return true;
	}
	
}//end class CellList
