// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #4 Part 2
// Due Date: 12 of April 2013
//--------------------------------------------------------------------
package part2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CellListUtilization {

	public static void main(String args[]) {

		// welcome message
		System.out.println("*********************************************");
		System.out.println("*        Welcome to Cell List Test Driver   *");
		System.out.println("*              Programmed by                *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();
	
		//a) create two empty lists from CellLlist class
		CellList list1 = new CellList();
		CellList list2 = new CellList();
		
		//b)Open the Cell_Info.txt file
		File inputFile = new File("Cell_Info.txt");
		Scanner openedFile = CellStaticMethods.openFile(inputFile);
		//read the lines in the file and create nodes in the list
		CellStaticMethods.createList(openedFile, list1);
		//c)show the contents of our list
		System.out.println("Here is what our CellList contains: ");
		list1.showContents();
		//d)prompt the user to enter a few serial numbers and search
		//the list for these values. Display iterations
		//ask for a file name to save search data to
		File outputFile = new File("searchserials.txt");
		//ask for a series of serial numbers
		ArrayList<Long> listOfSerials = CellStaticMethods.askSerialNumber();
		//save this ArrayList to the file name selected before
		CellStaticMethods.saveToFile(outputFile, listOfSerials);
		//now we will read this file and carry out the searches one by one
		CellStaticMethods.searchFromFile(outputFile, list1);

		CellStaticMethods.breakTime();
		
		//e)Create enough objects to test each of the constructors/methods
		//of the classes (including special cases)
		System.out.println("\n--------------------------------------");
		System.out.println("We will conduct a series of tests now to test our "+
		"created classes, functions " ); 
		//Testing CellPhone class
		
		//default constructor
		System.out.println("\nTesting the Cell Phone Class");
		System.out.println("--------------------------------------");
		System.out.println("Creating a new CellPhone Object (phone1) with default constructor");
		CellPhone phone1 = new CellPhone();
		System.out.println(phone1);
		//parametrized constructor
		System.out.println("\nCreating a new CellPhone Object (phone2) with Parametrized constructor");
		CellPhone phone2 = new CellPhone(11111111,"Samsung",2012,599.99);
		System.out.println(phone2);
		//copy constructor
		System.out.println("\nCreating a new CellPhone Object (phone3) with Copy constructor");
		CellPhone phone3 = new CellPhone(phone2, 222222222);
		System.out.println(phone3);
		//Testing Equals Method
		System.out.println("\nTesting Equals on Phone2 and Phone3");
		System.out.println("--------------------------------------");
		System.out.println("Are these phones equal? " + phone2.equals(phone3));
		//Mutator methods
		System.out.println("\nTesting accessor methods on phone3: ");
		System.out.println("--------------------------------------");
		System.out.println("Get Serial Number: " + phone3.getSerialNum());
		System.out.println("Get Brand: " + phone3.getBrand());
		System.out.println("Get Year: " + phone3.getYear());
		System.out.println("Get Price: " + phone3.getPrice());
		//Accessor methods
		System.out.println("\nTesting mutator methods on phone1: ");
		System.out.println("--------------------------------------");
		phone1.setSerialNum(9999999);
		System.out.println("Get Serial Number: " + phone1.getSerialNum());
		phone1.setBrand("Nokia");
		System.out.println("Get Brand: " + phone1.getBrand());
		phone1.setYear(1977);
		System.out.println("Get Year: " + phone1.getYear());
		phone1.setPrice(159.99);
		System.out.println("Get Price: " + phone1.getPrice());
		//Retest our equals
		System.out.println("\nTesting Equals on Phone1 and Phone3 after modifications:");
		System.out.println("Are these phones equal? " + phone1.equals(phone3));
		//testing clone
		System.out.println("\nTesting Clone Method (phone 4): ");
		CellPhone phone4 = (CellPhone)phone3.clone();
		System.out.println(phone4);
		System.out.println("Are these phones equal? " + phone4.equals(phone3));
		
		CellStaticMethods.breakTime();
		
		//Testing CellLlist.java
		System.out.println("\n--------------------------------------");
		System.out.println("Testing CellList.java");
		System.out.println("--------------------------------------");
		System.out.println("As CellNode is an private inner class, we do not have " +
				"access to it from the outside world.");
		
		//create a new list passing our phone1, phone2, phone3 and phone 4 objects
		System.out.println("Create a new list, list3, using copy constructor and copy our empty list2");
		CellList list3 = new CellList(list2);
		System.out.println("Here's the content of this new list (empty):");
		list3.showContents();
		//add phone2 to the start
		System.out.println("\nTesting addToStart() method: ");
		System.out.println("--------------------------------------");
		System.out.println("Adding phone1 to start");
		list3.addToStart(phone1);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		//Testing insertAtIndex() Method
		System.out.println("\nTesting insertAtIndex() method: ");
		System.out.println("--------------------------------------");
		System.out.println("Adding phone2 to index 0 (before the list)");
		list3.insertAtIndex(phone2, 0);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Adding phone3 to index1 (right before the end");
		list3.insertAtIndex(phone3, 1);
		System.out.println("Here's the updated content of this new list1:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Adding phone4 to index 1, so the list should move appropriately");
		list3.insertAtIndex(phone4, 1);
		System.out.println("Here's the updated content of this new list):");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		
		//Testing deleteFromIndex() method
		System.out.println("\nTesting deletetFromIndex() method: ");
		System.out.println("--------------------------------------");
		System.out.println("Our list has 4 records and we can go from 0 to size-1 ");
		System.out.println("Let's start by testing it with index 0, meaning we want to delete our head (Phone2)");
		list3.deleteFromIndex(0);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Let's delete at size-1 now (size = " + list3.getSize() + ", so size-1 = 2, so phone1 should be gone)");
		list3.deleteFromIndex(2);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Now our list has only 2 items, so let's add the other two again");
		list3.addToStart(phone1); list3.addToStart(phone2);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		
		//Testing deleteFromStart() method
		System.out.println("\nTesting our deleteFromStart() method: ");
		System.out.println("--------------------------------------");
		System.out.println("We will delete from start till our list becomes empty to ensure the method works with all sizes of the list.");
		System.out.println("Deleting first item");
		list3.deleteFromStart();
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Deleting again: ");
		list3.deleteFromStart();
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Deleting again: ");
		list3.deleteFromStart();
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Deleting again: ");
		list3.deleteFromStart();
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("We have deleted all the nodes now, so next call should tell us that our list is empty.");
		System.out.println("Deleting again: ");
		list3.deleteFromStart();
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		
		
		//Testing ReplaceAtIndex() method
		System.out.println("\nTesting replaceAtIndex () method:");
		System.out.println("--------------------------------------");
		System.out.println("Our list is empty and we can't really use this method as the index has to be between 0 and size-1");
		System.out.println("So as the current size is " + list3.getSize() + " this method can only be used when there's at least one item in the list");
		System.out.println("Let's add an item now");
		list3.addToStart(phone1);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Now if we called replaceAtIndex, we would replace the head:");
		list3.replaceAtIndex(phone2, 0);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Adding a couple of new phones to the list to test the method more: ");
		list3.addToStart(phone3); list3.addToStart(phone4);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Let's try to replace the last node: ");
		list3.replaceAtIndex(phone1, 2);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Let's try to replace the middle index: ");
		list3.replaceAtIndex(phone2, 1);
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("And if we replace out of bounds (index doesn't exist, such as 4 n this case)");
		list3.replaceAtIndex(phone1, 4);
		System.out.println("We should just get the same list back");
		System.out.println("Here's the updated content of this new list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		
		//testing copy constructor and equals method
		System.out.println("\n--------------------------------------");
		System.out.println("We will now test our copy constructor on this small list (we already copied an empty list before");
		//copy our list3 using copy constructor into the list2 variable
		list2 = new CellList(list3);
		System.out.println("Here's the original list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Here's the copied list: ");
		list2.showContents();
		System.out.println("Size: " + list2.getSize());
		System.out.println("\nTesting our equals method: ");
		System.out.println("Are these lists equal? " + list2.equals(list3));
		System.out.println("Are they deep copies?  Let's delete an element in the copied list and verify both lists again");
		//deleting head from list2
		list2.deleteFromStart();
		System.out.println("Here's the original list:");
		list3.showContents();
		System.out.println("Size: " + list3.getSize());
		System.out.println("Here's the copied list: ");
		list2.showContents();
		System.out.println("Size: " + list2.getSize());
		System.out.println("Our copied lists are perfect deep copies.");
		
		System.out.println("Is any of these lists same as our first list? " + list2.equals(list1));
		
		
		// exit message
		System.out.println();
		System.out.println("*********************************************");
		System.out.println("*   Thanks for using Cell List Test Driver! *");
		System.out.println("*********************************************");
	}

}
