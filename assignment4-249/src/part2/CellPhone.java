// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #4 Part 2
// Due Date: 12 of April 2013
//--------------------------------------------------------------------

package part2;

import java.text.NumberFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This is our main class of Cell Phone Objects. Each cell
 * phone is stored in a linked list node. 
 * This class implements the Cloneable interface.
 * 
 * @author Amit
 *
 */
public class CellPhone implements Cloneable {
	
	private long serialNum = 0; //serial number of the phone
	private String brand = null; //Brand of the phone
	private int year = 0; //manufacturing year
	private double price = 0.0; 
	private Scanner kb = new Scanner(System.in);

	/**
	 * Default constructor.  Sets all attributes at 0 or null.
	 */
	public CellPhone() {
		serialNum = 0;
		brand = null;
		year = 1900;
		price = 0;
	}
	
	/**
	 * Parametrized constructor
	 * @param serialNum (long)
	 * @param brand (String)
	 * @param year (int)
	 * @param price (price)
	 */
	public CellPhone(long serialNum, String brand, int year, double price){
		this.serialNum = serialNum;
		this.brand = brand;
		this.year = year;
		this.price = price;
	}
	
	/**
	 * Copy constructor:  Copies the object being passed as a parameter and assigns
	 * the unique serialNumber being provided to it. 
	 * The serialNumber must be unique.
	 * 
	 * @param c (type Object)
	 * @param serialNum (type long)
	 */
	public CellPhone(Object c, long serialNum){
		if (c == null) { //does the object exist?
			System.out.println("The object you want to copy does not exist.");
		} else if (getClass() != c.getClass()){ //is it a CellPhone?
			System.out.println("The object you want to copy does not belong to same class.");
		} else { //it passed our tests
			CellPhone other = (CellPhone) c; //type cast our object
			this.serialNum = serialNum;
			this.brand = other.brand;
			this.price = other.price;
			this.year = other.year;	
		} //end if else
		
	}

	/**
	 * This method allows one to access the serial Number of a cell phone object
	 * @return the serialNum (long)
	 */
	public long getSerialNum() {
		return serialNum;
	}

	/**
	 * This method allows one to modify the serial Number of a cell phone object
	 * @param serialNum the serialNum (long) to set
	 */
	public void setSerialNum(long serialNum) {
		this.serialNum = serialNum;
	}

	/**
	 * This method allows one to access the Brand name of the cell phone object
	 * @return the brand (string)
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * This method allows one to modify the Brand name of the cell phone object
	 * @param brand the brand to set (String)
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * This method allows one to access the year the cell phone object was manufactured
	 * @return the year (int)
	 */
	public int getYear() {
		return year;
	}

	/**
	 * This method allows one to modify the year the cell phone object was manufactured
	 * @param year the year to set (int)
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * This method allows one to access the price of the cell phone object
	 * @return the price (int)
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * This method allows one to modify the price of the cell phone object
	 * @param price the price to set (double)
	 */
	public void setPrice(double price) {
		this.price = price;
	}


	/**
	 * The equal method overrides the equals in the object class. 
	 * Two CellPhones are considered equal when everything matches with the
	 * exception of the serial number, which is allowed to be different. 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CellPhone other = (CellPhone) obj;
		if (brand == null) {
			if (other.brand != null) {
				return false;
			}
		} else if (!brand.equals(other.brand)) {
			return false;
		}
		if (Double.doubleToLongBits(price) != Double
				.doubleToLongBits(other.price)) {
			return false;
		}
		if (year != other.year) {
			return false;
		}
		return true;
	}

	/**
	 * The clone() method overrides the Object.clone() method. 
	 * It makes a call to super.clone() following the proper described way.
	 * A call to super.clone() is made to do a bit-by-bit copy of the object and
	 * as our serial number is unique, the method asks the user cloning the object
	 * to provide a new serial number and sets it for the copy created by the 
	 * clone() method.
	 *  
	 */
	@Override
	public Object clone() {
		
		try {
			//create a copy using the super.clone() method
			//as our CellPhone class only contains immutable objects
			//we can be sure there is no "privacy leak" here.
			CellPhone copy = (CellPhone)super.clone();
			boolean done = false;
			while(!done){
				try {
					System.out.println("Please provide a new Serial Number for the copied object: ");
					copy.serialNum = kb.nextLong(); //set the serialnumber for our copy
					done = true; //break our loop
				} catch (InputMismatchException e){
					System.out.println("A problem was detected with your entry.");
					System.out.println("Please ensure you are entering a number only.");
				}
			}
			//we can return our deep copy. 
			return copy;
		} catch (CloneNotSupportedException e){
			return null; //keeps the compiler happy
		}
		
	}

	/**
	 * The toString() method overrides the Object toString().  It displays 
	 * a cell phone in the format:
	 * [Serial Number: Brand Name Price Year]
	 */
	@Override
	public String toString() {
		
		//format price correctly using NumberFormat class
		String p = NumberFormat.getCurrencyInstance().format(this.price); 
		
		return "[" + serialNum + ": " + brand + " " + p  + " " + year +  "]";
	}

	


}
