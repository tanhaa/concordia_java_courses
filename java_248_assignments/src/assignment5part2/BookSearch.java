package assignment5part2;

//------------------------------------------------------------------
//Assignment 5
//Question: Part 1 & 2
//Written By: Amit Malhotra  & Abtin Farokhfal 
//
//--BookSearch.java--
//
//This program will create a user-defined class Book which will allow users to
//to create a book object using a driver and well defined methods within this class
//The methods included will allow users to retrieve the attributes of any object
//created within this class, change those attributes, print the attributes of the class
//using print/println() methods and also check if two objects from this class
//are exactly the same or not.  The class will allow book objects to be created with 
//three different attributes:  price, ISBN and a title.
//Main function of this program will allow a user to use the book class, create
//ten objects using the different constructors available in the book class and then
//do a search for a book by title, price or a combination of both attributes and 
//display the result to the user.
//
//Key Points: 
//1) import of the following package(s):
//		i)java.text.DecimalFormat: used to format price in the toString() method 
//2) class name and file name
//3) default constructor (no-parameter constructor)
//4) custom constructor with parameters (overloading constructors)
//5) mutator and accessor methods in a user-defined class
//6) toString and equals method
//7) private attributes vs public attributes
//8) this keyword in user-defined classes
//9) program comments 
//10) use of printf to print price
//11) use of if statements and a for loop 
//12) creating object pointers using an array
//13) initializing those pointers in the array using the constructors available in the class
//14) using a for loop to do a search through the entries in the array
//------------------------------------------------------------------

import java.text.DecimalFormat;  //import DecimalFormat class to help format price
import java.util.Scanner; //import the java util scanner. 

class Book {
	
	//This is a user-defined class that allows book objects to be created
	//with three attributes: price, ISBN and title
	
	//__________________________________________
	//   Attributes of Book Class
	//__________________________________________
	
	//declare the attributes as private variables
	private String title;
	private long isbn = 0;
	private double price = 0.0;

	//__________________________________________
	//  Constructors of Book Class
	//__________________________________________
	
	public Book() 
	{ //default no-parameter constructor
	
		System.out.println("Creating object with pre-determined values......");
		this.title = "Default Book Title";
		this.isbn = 123456789;
		this.price = 19.99;
		
	} //end no-parameter constructor
	
	public Book(String title, long isbn, double price)
	{ //constructor that asks for parameters
		
		System.out.println("Creating object with provided values......");
		this.title = title;
		this.isbn = isbn;
		this.price = price;
		
	} //end initialized constructor
	
	public Book(Book otherBook)
	{ //copy constructor
		
		System.out.println("Creating object as a copy of another book ......");
		this.title = otherBook.title;
		this.isbn = otherBook.isbn;
		this.price = otherBook.price;
				
	} //end copy constructor
	
	//__________________________________________
	// Accessor methods of Book Class
	//__________________________________________
		
	//the following methods will allow a driver to retrieve the attributes of
	//an object created within this class
	
	public double getPrice() { //returns the price of the book to the user
		return this.price;
	} //end getPrice method
	
	public String getTitle() { //returns the title of the book to the user
		return this.title;
	} //end getTitle method
	
	public long getIsbn() { //returns the ISBN number of the book to the user
		return this.isbn;
	} //end getIsbn method
	
	//__________________________________________
	// Mutator methods of Book Class
	//__________________________________________
	
	//the following methods will allow a driver to set the attributes of 
	//an object created within this class
	
	public void setPrice(double price) {  //sets the price with the provided value
		this.price = price;
	} //end setPrice method
	
	public void setTitle(String title) { //sets the title with the provided value
		this.title = title;
	} //end setTitle method
	
	public void setIsbn(long isbn) { //sets the ISBN with the provided value
		this.isbn = isbn;
	} //end setIsbn method
	
	//__________________________________________
	// Other methods of Book Class
	//__________________________________________
	
	//the following methods will allow the class to be printed and compared
	
	public String toString() {
		DecimalFormat dollarFormatter = new DecimalFormat("0.00"); //DecimalFormat object created to format price
		return ("\n Title:  " + this.title + "\n ISBN#:  " + this.isbn + "\n Price:  $ " + dollarFormatter.format(this.price)); 
	} //end toString method
	
	public boolean equals(Book otherbook) { //compares our book with another book from the same class
		
		//the if statement will check if price and ISBN matches in two 
		//given objects from this class 
		if (this.price == otherbook.price && this.isbn == otherbook.isbn)
			return true;
		else
			return false;
	}

}

public class BookSearch {
	
	//The purpose of this program is to create 10 books and ask a user to search through
	//the array of these books. The user can enter a book title and a book price and search
	//through the array using either a combination of both the entries or the individual
	//search entries.  The program will then display the results to the user

	public static void main(String[] args) {
		
	//Variable declaration
	Book[] bArr = new Book[10]; //create an array with 10 empty books
	Scanner kb = new Scanner(System.in); //creating a Scanner object for input requirements
	String sSearchedBookTitle = ""; //to capture searched book title
	double dSearchedPrice = 0.0; //to capture searched book price
	String sSearchCombination = ""; //to capture combination preferences
	int i = 0; //to be used in the for loops for search
	boolean bFound = false; //set a flag if we find any matches for the user
	
	//welcome message
	System.out.println("*********************************************");
	System.out.println("*      Welcome to the Book Search Engine    *");
	System.out.println("*              Program created by           *");
	System.out.println("*       Amit Malhotra & Abtin Farokhfal     *");
	System.out.println("*********************************************");
	
	System.out.println("\n\nCreating the book objects");
	//initializing the first five books with parametrized constructor
	bArr[0] = new Book("My First Book", 112345678, 19.99);
	bArr[1] = new Book("My Second Book", 112233445, 29.99);
	bArr[2] = new Book("My Third Book", 111222333, 39.99);
	bArr[3] = new Book("My Fourth Book", 111122223, 49.99);
	bArr[4] = new Book("My Fifth Book", 111112222, 59.99);
	//initializing a book with the default constructor
	bArr[5] = new Book();
	//initializing the remaining 4 books with the copy constructor
	bArr[6] = new Book(bArr[0]);
	bArr[7] = new Book(bArr[1]);
	bArr[8] = new Book(bArr[2]);
	bArr[9] = new Book(bArr[3]);
	
	//ask user for a book title to search
	System.out.println("\nPlease enter the book title you would like to search for: "); 
	sSearchedBookTitle = kb.nextLine();  //save entered book title
	//ask user for a price to search
	System.out.println("Please enter the price you prefer: ");
	dSearchedPrice = kb.nextDouble(); //save entered price
	//ask user if he wants to do a combine search of his entered values
	System.out.println("Please enter \"yes\" if you would like to find combinations that match both your "
			+ "requirements: ");
	sSearchCombination = kb.next(); //save entered preference
	
	System.out.println("\nHere are your Search Results:");
	System.out.println("_____________________________");
	
	for (i = 0; i<bArr.length; i++)
	{ //start search for loop
	
		if (sSearchCombination.toLowerCase().equals("yes"))
		{
			
			if (bArr[i].getTitle().toLowerCase().equals(sSearchedBookTitle.toLowerCase()) && bArr[i].getPrice() == dSearchedPrice)
			{
				System.out.print("\nBook #" + (i+1) + ":");
				System.out.println("\nBook Title: " + bArr[i].getTitle());
				System.out.printf("Book Price: $ %.2f %n", bArr[i].getPrice());
				bFound = true;
			}
			
		}
		else
		{
			if (bArr[i].getTitle().toLowerCase().equals(sSearchedBookTitle.toLowerCase()) || bArr[i].getPrice() == dSearchedPrice)
			{
				System.out.print("\nBook #" + (i+1) + ":");
				System.out.println("\nBook Title: " + bArr[i].getTitle());
				System.out.printf("Book Price: $ %.2f %n", bArr[i].getPrice());
				bFound = true;
			}
				
		}	
			
	}
	
	//if we didn't find any results after running through our loop, display the following message
	if (bFound == false)
		System.out.println("\nSorry, no results were found with your search parameteres.");


	//print the exit message
		
	System.out.println("\n********************************************");
	System.out.println("Thank you for using our Book objects tester!"); 
	System.out.println("********************************************");
	kb.close();
	}

}
