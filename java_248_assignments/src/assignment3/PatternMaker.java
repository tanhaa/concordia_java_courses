package assignment3;
// ------------------------------------------------------------------
// Assignment 3
// Question: 
// Written By: Amit Malhotra  & Abtin Farokhfal 
//
// --PatternMaker.java--
//  This java program presents the user with a menu that includes 4 different patterns
//  The user must select one of the four patterns and provide how many rows that they
//  want to see in that pattern.  The program will then output the pattern that the
//  the user selected on the screen and prompt the user for another output until the 
//  user selects explicitly to exit the program.  The program will check if the user
//  provides an illegal choice (not 1 to 5) and ask them again for a proper selection.
//  In addition the program will check if the supplied rows are between a specific range
//  for the selected pattern and prompt the user again so that they can provide a proper
//  input value.  For one of the pattern, the program also needs to ensure that the 
//  pattern printed is different for even values and different for odd values.
//
// Key Points: 
//  1) import of the following package(s):
//			i)java.util: needed to use the Scanner class in order to read user input from System.in
// 	2) class name and file name 
// 	3) main method and other methods that return int, boolean or void types
//  4) use of Scanner object in order to capture a line of text via the keyboard
//  5) closing of scanner object after user input capture to prevent a resource leak
//  6) Modularity, program is split in different methods that carry out a specific task
//  7) Loops and nested loops: do-while, while, for
//  8) Information flow check using if-else statements and loops to catch invalid choices
//  9) Repetition statements till user exits the main method
// 10) Static variables
// 11) program comments 
// ------------------------------------------------------------------

//Need to import the Scanner class to capture data from the user
import java.util.Scanner;

public class PatternMaker {  //start class
	
	//The program presents a menu to the user from which a pattern can be selected  
	//to display on the screen with an input value that the user provides
	//The program is made in a modular manner where static methods are used to separate
	//all steps in the main method.  All static methods are presented first in the code 
	//below followed at the end by the main method.
	
	static Scanner kb = new Scanner(System.in); //static Scanner object to be used in all methods
	
	public static void menu() {
		
		// this method will create and display the main menu
		
		//print main-menu
        System.out.println("1) 54321\t2)     1\t3) 12345\t4)    1");
        System.out.println("   5432 \t      12\t    2345\t     123");
        System.out.println("   543  \t     123\t     345\t    12345");
        System.out.println("   54   \t    1234\t      45\t     123");
        System.out.println("   5    \t   12345\t       5\t      1");
        System.out.print("Enter your choice (5 to quit): ");
	} //end method menu
	
	public static int prompt()	{
		
		//this method will capture user's initial menu selection
		
		int userSelection;  //variable to capture user selection
		userSelection = kb.nextInt();  //save next entered integer in userSelection
		return userSelection;  //return captured user-selection	
	}//end method prompt
		
	public static boolean isValid(int userSelection)	{
		
		// this method will verify if the selection that the user made in the main
		// menu is valid or not, that is between 1 and 5 or not and return the result
		// as a true or false value

		if (userSelection > 0 && userSelection < 6 )  //check userSelection between 1 and 5
		return true;  //returns the value true
		else 
		return false;  //returns the value false
	} //end method isValid
	
	public static int rowSelect()	{
		
		//this method will ask user the amount of rows they want to display
		//in the pattern.  If the input value supplied by the user is 
		//not larger than 0 and smaller than 25, the method will prompt user again
		//and then return the number of rows
		
		int numRows = 0; //variable to capture the input value initialized to illegal value
		
		//we will use a while loop to prompt the user repeatedly
		//to get the right value from the user which must be between 2 and 25
		while (numRows < 1 || numRows > 24)
		{
			System.out.print("How many rows would you like to print? (More than 1 please) : ");
			numRows = kb.nextInt(); //sets number of rows to the value user entered
		}
	
		System.out.println(); //blank line
		return numRows;  //returns the number of rows
	} //end rowSelect
	
	public static void patternSelector(int choice)	{
	
		//This method will be used to print the pattern that the user selected
		//The choice from the menu will be passed to the method and based
		//on that choice, a switch-case statement will output pattern to the screen
		
		int numRows = 0;  //we will use this variable to capture the number of rows
		numRows = rowSelect();  //call rowSelect method to ask user for number of rows
		
		//the Switch case statement below will use the choice value passed to the method
		//at call time and print the appropriate pattern by calling that pattern's method
		//The number of rows (numRows) will be passed to the pattern being called
		switch(choice) 
		{
			case 1:
					pattern1(numRows);
					return;
			case 2:
					pattern2(numRows);
					return;
			case 3:
					pattern3(numRows);
					return;
			case 4:
					pattern4(numRows);
					return;
		}// end switch
	}//end patternSelector	

	public static void pattern1(int x)	{
		
		//This method will use a for loop to print the first triangle pattern
		//note that the pattern is same for even and odd number, no check is required
		//the number of rows must be passed to the method, denoted as "x" in the parameters
		
	    int i = 0, j = 0;  //the variables used in the for loop
	        
	    //for loop to print the pattern
	    for (i=1; i<=x; i++)  //to print the rows, less than or equal to x
	    {
	    	for (j=x; j>=i; j--) //to print the columns of digits
	    		System.out.print(j);
	          
	        System.out.println();
	    } 
	} //end pattern1

	public static void pattern2(int x)	{
		
		//This method will use a for loop to print the second triangle pattern
		//The pattern is same for even or odd numbers, no check is required
		//The number of rows must be passed to the method, denoted as "x" in the parameters
		
	    int i = 0, j = 0, k = 0; //the variables used in the for loop

	    for (i=1; i<=x; i++) //to print the rows, less than or equal to x
	    {
	    	for (j=1; j<=x-i; j++) //to print the spaces in the columns
	    		System.out.print(" ");
	          
	        for (k=1; k<=i; k++) //to print the digits
	        	System.out.print(k);

	        System.out.println();
	    }
	} //end pattern2

	public static void pattern3(int x)	{
		
		//This method will use a for loop to print the third triangle pattern
		//The pattern is same for even or odd numbers, no check is required
		//The number of rows must be passed to the method, denoted as "x" in the parameters
		
		int i = 0, j = 0, k = 0; //the variables used in the for loop

		for (i=1; i<=x; i++) //to print the rows, less than or equal to x
	    {
			for (j=1; j<i; j++) //to print the spaces in the columns
				System.out.print(" ");
	          
	        for (k=i; k<=x; k++)//to print the digits
	        	System.out.print(k);

	        System.out.println();
	    }
	} //end pattern3


	public static void pattern4(int x)	{

		//This method will use a for loop to print the diamond pattern
		//The pattern is different for even and odd number of rows and
		//therefore, an if statement will be used to check whether number of rows
		//passed to the method (as "x" in the parameter) is even or odd
		//the diamond is also printed in two parts, top and bottom, each requiring
		//a different loop
		
		int i = 0,j = 0,k = 0; //the variables used in the for loops

		if (x%2 == 0)	{ //returns true if the value is even
			
			//loop for the top part when even
	        for (i=1; i<=(x/2); i++) //to print the rows, equal to x/2
	        {
	        	for (j=(x/2)-i; j>0; j--) //to print the spaces in columns
	        		System.out.print(" ");
	                      
	            for (k=1; k<(2*i); k++) //to print the digits in columns
	                System.out.print(k);
	 
	            System.out.println();
	        }
	        
	        //loop for the bottom part when even
	        for (i=1; i<=(x/2); i++) //to print the rows equal to x/2
	        {         
	        	for (j=i-1; j>0; j--) //to print the spaces in columns
	        		System.out.print(" ");
	                      
	            for (k=1; k<=(x-i)-(i-1); k++) //to print the digits in columns
	                System.out.print(k);
	 
	            System.out.println();
	        }
		} 
		else //if number is not even, then it's odd 
		{
			//loop for the top part when odd
	        for (i=1; i<=(x/2)+(x%2); i++) //to print the rows, x/2 + remainder to get one row more than the bottom part
	        {
	        	for (j=((x/2)+(x%2))-i; j>0; j--) //to print the spaces in columns
	        		System.out.print(" ");
	                      
	            for (k=1; k<=(i+(i-1)); k++) //to print the digits in columns
	                System.out.print(k);
	 
	            System.out.println();
	        }
	        
	        //loop for the bottom part when odd
	        for (i=1; i<=(x/2); i++) //to print the rows, equal to x/2 (only the integer part)
	        {
	            for (j=i; j>0; j--) //to print the spaces in columns
	            	System.out.print(" ");
	                      
	            for (k=1; k<=(x-(2*i)); k++) //to print the digits in columns
	                System.out.print(k);
	 
	            System.out.println();
	        }

		} //end if-else
	} //end pattern4
	
	public static void exit(){
		
		//this method will be called upon when the user wants to exit
		//it will print the exit message and close the scanner object
		
		//print the exit message
  		System.out.println("\n********************************************");
		System.out.println("Thank you for using our Pattern Maker!"); 
		System.out.println("********************************************");
		kb.close();  // close Scanner object
	} //end method exit
	
	public static void main(String[] args) {
		
		//the main method will use all the above static methods
		//to run the program, capture input, print patterns
		
		int choice = 0; //variable to save user's choice
		
		//Display a welcome message to the user
		System.out.println("*********************************************");
		System.out.println("*        Welcome to the Pattern Maker       *");
		System.out.println("*              Program created by           *");
		System.out.println("*       Amit Malhotra & Abtin Farokhfal     *");
		System.out.println("*********************************************");
		
		//ask user for the choice
		System.out.println("\n\nWhich pattern do you want to print?");
		//and display the main menu
		menu();
		
		//the rest of the main method will run in a do-while loop till the user
		//chooses to exit the program.
				
		do 
		{ //start do-while loop
			
			choice = prompt();  //set the variable choice as the return of the method prompt
        
			//The while loop below will check if the option that the user selected from the 
			//menu is valid or not using the isValid method.  If the choice is invalid
			//the user will be prompted again using the prompt method and will continue 
			//to be prompted unless they provide a valid choice
			while (!isValid(choice))
			{
				System.out.println("\nWoops! " + choice + " is an illegal choice.  Try again.");
				System.out.print("Please enter a number between 1 and 5 inclusive: ");
				choice = prompt();
			} //end while isValid
			
			//we will use an if statement to see if the user wants to exit or not
			//if user does not want to exit, that is to say, if choice is not
			//equal to 5, then we will print the pattern and ask again for a new
			//choice, or else, we will continue to check the do-while loop condition
			//and exit if needed
			if (choice != 5) //if choice is not equal to 5
			{
			//Now we will use the patternSelector method and pass the user choice
			//as a parameter to print the selected pattern by the user
			patternSelector(choice);
        
			//ask user for his next pattern choice
			System.out.println("\nSo which pattern do you want to print now?");
			//and display the main menu again
			menu();
			} //end if
        
		} while(choice !=5); //exit the do-while loop if user chooses 5 to exit
		
		exit(); //run the exit method that displays a goodbye message
        

	} //end main method
	
	
} //end class
