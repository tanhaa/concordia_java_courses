package assignment5;
// ------------------------------------------------------------------
// Assignment 5
// Question: Part 3
// Written By: Amit Malhotra  & Abtin Farokhfal 
//
// --ModifyBooks.java--
// 
//  This program will create a user-defined class Book which will allow users to
//  to create a book object using a driver and well defined methods within this class
//  The methods included will allow users to retrieve the attributes of any object
//  created within this class, change those attributes, print the attributes of the class
//  using print/println() methods and also check if two objects from this class
//  are exactly the same or not.  The class will allow book objects to be created with 
//  three different attributes:  price, ISBN and a title.
//  Main function of this program will allow a user to use the book class, create
//  ten objects using the different constructors available in the book class. 
//  The function will then modify the prices of all the objects using a for loop and random generator
//  and then allow the user to search with a title or the ISBN number of the book
//  The for loop to do the search will keep track of all the locations of the books that match
//  the title and the ISBN number and will adjust the price of those books to the smallest price
//  that any of them has and display an indication that the price was modified.
//
// Key Points: 
//  1) import of the following package(s):
//		i)java.text.DecimalFormat: used to format price in the toString() method 
//  2) class name and file name
//  3) default constructor (no-parameter constructor)
//  4) custom constructor with parameters (overloading constructors)
//  5) mutator and accessor methods in a user-defined class
//  6) toString and equals method
//  7) private attributes vs public attributes
//  8) this keyword in user-defined classes
//  9) program comments 
// 10) use of printf to print price
// 11) use of if statements and a for loop 
// 12) creating object pointers using an array
// 13) initializing those pointers in the array using the constructors available in the class
// 14) using a for loop to do a search through the entries in the array
// 15) Random generator
// ------------------------------------------------------------------

import java.text.DecimalFormat;  //import DecimalFormat class to help format price
import java.util.Scanner; //import the java util scanner.
import java.util.Random; // import the java Random util


class Book {
	
	//This is a user-defined class that allows book objects to be created
	//with three attributes: price, ISBN and title
	
	//__________________________________________
	//   Attributes of Book Class
	//__________________________________________
	
	//declare the attributes as private variables
	private String title;
	private long isbn = 0;
	private double price = 0.0;

	//__________________________________________
	//  Constructors of Book Class
	//__________________________________________
	
	public Book() 
	{ //default no-parameter constructor
	
		System.out.println("Creating object with pre-determined values......");
		this.title = "Default Book Title";
		this.isbn = 123456789;
		this.price = 19.99;
		
	} //end no-parameter constructor
	
	public Book(String title, long isbn, double price)
	{ //constructor that asks for parameters
		
		System.out.println("Creating object with provided values......");
		this.title = title;
		this.isbn = isbn;
		this.price = price;
		
	} //end initialized constructor
	
	public Book(Book otherBook)
	{ //copy constructor
		
		System.out.println("Creating object as a copy of another book ......");
		this.title = otherBook.title;
		this.isbn = otherBook.isbn;
		this.price = otherBook.price;
				
	} //end copy constructor
	
	//__________________________________________
	// Accessor methods of Book Class
	//__________________________________________
		
	//the following methods will allow a driver to retrieve the attributes of
	//an object created within this class
	
	public double getPrice() { //returns the price of the book to the user
		return this.price;
	} //end getPrice method
	
	public String getTitle() { //returns the title of the book to the user
		return this.title;
	} //end getTitle method
	
	public long getIsbn() { //returns the ISBN number of the book to the user
		return this.isbn;
	} //end getIsbn method
	
	//__________________________________________
	// Mutator methods of Book Class
	//__________________________________________
	
	//the following methods will allow a driver to set the attributes of 
	//an object created within this class
	
	public void setPrice(double price) {  //sets the price with the provided value
		this.price = price;
	} //end setPrice method
	
	public void setTitle(String title) { //sets the title with the provided value
		this.title = title;
	} //end setTitle method
	
	public void setIsbn(long isbn) { //sets the ISBN with the provided value
		this.isbn = isbn;
	} //end setIsbn method
	
	//__________________________________________
	// Other methods of Book Class
	//__________________________________________
	
	//the following methods will allow the class to be printed and compared
	
	public String toString() {
		DecimalFormat dollarFormatter = new DecimalFormat("0.00"); //DecimalFormat object created to format price
		return ("\n Title:  " + this.title + "\n ISBN#:  " + this.isbn + "\n Price:  $ " + dollarFormatter.format(this.price)); 
	} //end toString method
	
	public boolean equals(Book otherbook) { //compares our book with another book from the same class
		
		//the if statement will check if price and ISBN matches in two 
		//given objects from this class 
		if (this.price == otherbook.price && this.isbn == otherbook.isbn)
			return true;
		else
			return false;
	}

}

public class ModifyBooks {
	
	//The purpose of this program is to create 10 books and ask a user to enter a book
	//title and the ISBN number.  The program will then use a random number to change
	//the prices of all the book objects.  The user will then be asked to enter a title
	//and ISBN number for a search through these arrays.  Once these books are found
	//the program will keep track of the locations of these books and adjust the pricing
	//on these books to the smallest price that the searched books have and display the
	//modifications done to the user.
	
	public static void main(String[] args) {
		
	//Variable declaration
	Book[] bArr = new Book[10]; //create an array with 10 empty books
	Scanner kb = new Scanner(System.in); //creating a Scanner object for input requirements
	Random generator = new Random(); // creating a new Random generator
	String sSearchedBookTitle = ""; //to capture searched book title
	double dSearchedIsbn = 0.0; //to capture searched book ISBN
	int i = 0; //to be used in the for loops for search
	int[] iArr = new int[bArr.length]; //create an array with the same length as the book array to save our searched indices
	int counter = 0; //a small counter that will keep track of the amount of books we find
	double dSmallestPrice = 25000.00; //to capture the smallest price and set it to a really high value
	int priceChangeFlag = 0; //flag will be used to determine if a book's price was changed to the smallest price
	
	//welcome message
	System.out.println("*********************************************");
	System.out.println("*   Welcome to the Book Modification Engin  *");
	System.out.println("*              Program created by           *");
	System.out.println("*       Amit Malhotra & Abtin Farokhfal     *");
	System.out.println("*********************************************");
	
	System.out.println("\n\nCreating the book objects");
	//initializing the first five books with parametrized constructor
	bArr[0] = new Book("My First Book", 112345678, 19.99);
	bArr[1] = new Book("My Second Book", 112233445, 29.99);
	bArr[2] = new Book("My Third Book", 111222333, 39.99);
	bArr[3] = new Book("My Fourth Book", 111122223, 49.99);
	bArr[4] = new Book("My Fourth Book", 111122223, 59.99);
	//initializing a book with the default constructor
	bArr[5] = new Book();
	//initializing the remaining 4 books with the copy constructor
	bArr[6] = new Book(bArr[0]);
	bArr[7] = new Book(bArr[1]);
	bArr[8] = new Book(bArr[2]);
	bArr[9] = new Book(bArr[3]);
	
	//Use for loop to initialize the iArr to -1 value in each index
	for (i = 0; i<iArr.length; i++)
	{
		iArr[i] = -1;
	}
	
	//Use a for loop to adjust the pricing on each of these created objects using a random number
	for (i = 0; i<bArr.length; i++ )
	{ //start for loop to adjust price
	
		int randomInt = generator.nextInt(10) + 1; //create a random number between 0 and 9 and add 1 
		bArr[i].setPrice((bArr[i].getPrice()*randomInt)+i); //
		
	} //end for loop to adjust price
	
	//ask user for a book title  & an ISBN number to search
	System.out.println("\nPlease enter a book title: "); 
	sSearchedBookTitle = kb.nextLine();  //save entered book title
	System.out.println("Please enter an ISBN: ");
	dSearchedIsbn = kb.nextDouble(); //save entered ISBN
	
	//we will use another for loop to find all books that match the title and the ISBN entered by the user
	//and save the indices we find in our iArr
	for (i = 0; i<bArr.length; i++)
	{ 
		if (bArr[i].getTitle().toLowerCase().equals(sSearchedBookTitle.toLowerCase()) && bArr[i].getIsbn() == dSearchedIsbn)
			{
				iArr[counter] = i;  //we will set the index related to the counter in iArr to i to keep track
									//of the position in bArr that had the book we searched for
				counter++;  //increment the counter
			}
	}
	
	//the value of our counter can tell us how many books we found.  We will take appropriate action
	//based on the amount of books we found.
	if (counter == 0)  //no books found
	{
		System.out.println("\nSorry, no results were found with your search parameteres."); //print a message saying no books found
	}
	else if (counter == 1) //only one book found
	{
		System.out.println("\nWe only found one book matching your request, no change in price was done.");
	}
	else //if counter is not 0 or 1, means we found more then one book, so let's find and adjust the prices
	{
		//loop to find the smallest price in the index locations we saved in iArr
		for (i = 0; i<(counter+1); i++)
		{
			if (iArr[i] != -1) //check if we actually have an bArr index saved in the iArray.
			{
				if (bArr[iArr[i]].getPrice() < dSmallestPrice)  //is the price smallest?
					dSmallestPrice = bArr[iArr[i]].getPrice(); //set it to smallest
			}
		}	
	    
		//loop to modify the price of the previously found books to smallest price found
		for (i = 0; i<(counter+1); i++)
		{
			if (iArr[i] != -1) //check if we actually have a bArr index saved in the iArray
			{
				if (bArr[iArr[i]].getPrice() > dSmallestPrice) //is the price for that object smallest
				{
					//let user know we have adjusted the price
					System.out.printf("%nPrice of book at index %d has been modified from $%.2f to $%.2f%n", iArr[i], bArr[iArr[i]].getPrice(), dSmallestPrice);
					//and adjust the price!
					bArr[iArr[i]].setPrice(dSmallestPrice);
					priceChangeFlag = 1;
				}
			}
		}
		
		//if the price of two or more matching books was the same, we want to let user know that none of the prices were changed
		if (priceChangeFlag == 0)
			System.out.println("\nAll matching books had the same price, therefore no change in price was done.");
	}
	

	//print the exit message
		
	System.out.println("\n********************************************");
	System.out.println("Thank you for using our Book objects tester!"); 
	System.out.println("********************************************");
	kb.close();
	}

}

