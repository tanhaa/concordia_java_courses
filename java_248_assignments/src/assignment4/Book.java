package assignment4;
// ------------------------------------------------------------------
// Assignment 4
// Question: Part 1
// Written By: Amit Malhotra  & Abtin Farokhfal 
//
// --Book.java--
// 
//  This program will create a user-defined class Book which will allow users to
//  to create a book object using a driver and well defined methods within this class
//  The methods included will allow users to retrieve the attributes of any object
//  created within this class, change those attributes, print the attributes of the class
//  using print/println() methods and also check if two objects from this class
//  are exactly the same or not.  The class will allow book objects to be created with 
//  three different attributes:  price, ISBN and a title. 
//
// Key Points: 
//  1) import of the following package(s):
//	i)java.text.DecimalFormat: used to format price in the toString() method 
//  2) class name and file name
//  3) default constructor (no-parameter constructor)
//  4) custom constructor with parameters (overloading constructors)
//  5) mutator and accessor methods in a user-defined class
//  6) toString and equals method
//  7) private attributes vs public attributes
//  8) this keyword in user-defined classes
//  9) program comments 
// ------------------------------------------------------------------

import java.text.DecimalFormat;  //import DecimalFormat class to help format price

public class Book {
	
	//This is a user-defined class that allows book objects to be created
	//with three attributes: price, ISBN and title
	
	//__________________________________________
	//   Attributes of Book Class
	//__________________________________________
	
	//declare the attributes as private variables
	private String title;
	private long isbn = 0;
	private double price = 0.0;

	//__________________________________________
	//  Constructors of Book Class
	//__________________________________________
	
	public Book() 
	{ //default no-parameter constructor
	
		System.out.println("Creating object with pre-determined values......");
		this.title = "Default Book Title";
		this.isbn = 123456789;
		this.price = 19.99;
		
	} //end no-parameter constructor
	
	public Book(String title, long isbn, double price)
	{ //constructor that asks for parameters
		
		System.out.println("Creating object with provided values......");
		this.title = title;
		this.isbn = isbn;
		this.price = price;
		
	} //end initialized constructor
	
	//__________________________________________
	// Accessor methods of Book Class
	//__________________________________________
		
	//the following methods will allow a driver to retrieve the attributes of
	//an object created within this class
	
	public double getPrice() { //returns the price of the book to the user
		return this.price;
	} //end getPrice method
	
	public String getTitle() { //returns the title of the book to the user
		return this.title;
	} //end getTitle method
	
	public long getIsbn() { //returns the ISBN number of the book to the user
		return this.isbn;
	} //end getIsbn method
	
	//__________________________________________
	// Mutator methods of Book Class
	//__________________________________________
	
	//the following methods will allow a driver to set the attributes of 
	//an object created within this class
	
	public void setPrice(double price) {  //sets the price with the provided value
		this.price = price;
	} //end setPrice method
	
	public void setTitle(String title) { //sets the title with the provided value
		this.title = title;
	} //end setTitle method
	
	public void setIsbn(long isbn) { //sets the ISBN with the provided value
		this.isbn = isbn;
	} //end setIsbn method
	
	//__________________________________________
	// Other methods of Book Class
	//__________________________________________
	
	//the following methods will allow the class to be printed and compared
	
	public String toString() {
		DecimalFormat dollarFormatter = new DecimalFormat("0.00"); //DecimalFormat object created to format price
		return ("\n Title:  " + this.title + "\n ISBN#:  " + this.isbn + "\n Price:  $ " + dollarFormatter.format(this.price)); 
	} //end toString method
	
	public boolean equals(Book otherbook) { //compares our book with another book from the same class
		
		//the if statement will check if price and ISBN matches in two 
		//given objects from this class 
		if (this.price == otherbook.price && this.isbn == otherbook.isbn)
			return true;
		else
			return false;
	}

}