package assignment4;
// ------------------------------------------------------------------
// Assignment 4
// Question: Part 2
// Written By: Amit Malhotra & Abtin Farokhfal 
//
// --UtilizeBooks.java--
// 
//  This program will create a driver for the user-defined Book class 
//
// Key Points: 
//  1) class name and file name
//  2) creating objects for a user-defined class
//  3) Using mutator and accessor methods defined within that class
//  4) Displaying information from the class
//  5  Use of printf in order to format price
//  6) program comments 
// ------------------------------------------------------------------

public class UtilizeBooks {

	//This program uses a few methods to manipulate the user-defined book class
	//We start by creating a few book objects and then using the methods defined
	//in the book class to access and modify the attributes of the created objects
	
	public static void main(String[] args) {
	
		//welcome message
		System.out.println("*********************************************");
		System.out.println("*    Welcome to the Utilize Book program    *");
		System.out.println("*              Program created by           *");
		System.out.println("*       Amit Malhotra & Abtin Farokhfal     *");
		System.out.println("*********************************************");
	
		//create three new book objects
		Book book1 = new Book("My biography", 123456789, 19.99);
		Book book2 = new Book("Java For Super Dummies", 123456789, 29.99);
		Book book3 = new Book("Awesome Java Classes", 112233789, 39.99);
		
		//display the book objects using the toString method
		System.out.println("\nHere are our three book objects:");
		System.out.println(book1);
		System.out.println(book2);
		System.out.println(book3);
		
		//modify the attributes of the three objects using the appropriate
		//mutator methods
		book1.setPrice(59.99);
		book2.setTitle("My biography");
		book2.setPrice(59.99);
		book3.setIsbn(123445589);
		
		//display the modifications done to the objects using the appropriate
		//accessor methods
		System.out.println("\n*********************************************\n");
		System.out.println("\nHere are the modifications made to our three book objects:\n");
		System.out.printf("Object 1 modifications: The new price is $ %.2f\n", book1.getPrice());
		System.out.printf("Object 2 modifications: The new title is \"%s\" and the new price is $ %.2f\n", book2.getTitle(), book2.getPrice());
		System.out.println("Object 3 modifications: The new ISBN number is " + book3.getIsbn());
		
		//carry out some equality checks on the objects using the equals method
		System.out.println("\n*********************************************\n");
		System.out.println("We have done some equality checks on our three objects"
				+ "\nAnd here are our results from these checks\n");
		if (book1.equals(book2))
			System.out.println("Book 1 and Book 2 are the same books!");
		else
			System.out.println("Book 1 is not the same book as Book 2!");
		
		if (book2.equals(book3))
			System.out.println("Book 2 and Book 3 are the same books!");
		else
			System.out.println("Book 2 is not the same book as Book 3!");
		
		if (book1.equals(book3))
			System.out.println("Book 1 and Book 3 are the same books!");
		else
			System.out.println("Book 1 is not the same book as Book 3!");
		
		//print the exit message
  		System.out.println("\n********************************************");
		System.out.println("Thank you for using our Book objects tester!"); 
		System.out.println("********************************************");
	

	}

}