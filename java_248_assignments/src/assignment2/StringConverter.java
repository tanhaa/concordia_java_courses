package assignment2;
// ------------------------------------------------------------------
// Assignment 2
// Question: Part 1
// Written By: Amit Malhotra & Abtin Farokhfal 
//
// --StringConverter.java--
//
// In Part 1 of Assignment 2 we were asked to create a program that would ask the user to 
// input a string separated by a "\" with the maximum length of four words and then convert
// the string in reverse order removing the symbol used as delimiter and finally output the 
// original and the converted string.
//
// Summary of program corresponding to assignment requirements:
// 	>Greet User
// 	>Prompt user to enter a line of text including 4 words separated by "\"
//	>As per the assignment guidelines, it is assumed that the input is in the requested format
//	>Extract each word delimited by "\" and store them separately
//  >Concatenate the words in reverse order to form another string
//  >Display a message with original string
//	>Display new concatenated string
//	>End with a thank you message
// 
// Key Points: 
//  1) import of the following package(s):
//			i)java.util: needed to use the Scanner class in order to read user input from System.in
// 	2) class name and file name (StringConverter)
// 	3) main method
//  4) use of Scanner object in order to capture a line of text including 4 words delimited by "\" entered via the keyboard
//  5) closing of scanner object after user input capture to prevent a resource leak
//  6) string manipulation:
//  	a) for 1st word, we use the substring method along with the indexOf() method in order to extract the word delimited by the location of the occurrence of the first "\"
//  	b) for 2nd word we start by reassigning the original string as its own substring starting with the previous location of the "\"
//  	c) then using the new substring we again use indexOf() to find the location of the next "\"
//  	d) using substring() we again use this location to extract the following word
//  	e) these steps are repeated for words 3 and 4
//  7) use of println to display various messages to the user including a welcome, prompt for input, original string, new concatenated string, and an ending message
// 	9) program comments 
// ------------------------------------------------------------------

import java.util.Scanner; //Need to import the Scanner class to capture data from the user

public class StringConverter {  //start class

	//The program asks the user to enter a "\" delimited string and 
	//outputs the string in reverse order
	
	public static void main(String[] args) {  //start main method
		
		// Declare variables
		
		String sInputString; //this will be used to capture the input string
		String sWord1, sWord2, sWord3, sWord4;  //these will be used to store the words after string manipulation
		int iSlashIndex; //used to save the indexOf number returned to find the exact location of the delimiter "\"
		
		//Create one scanner object which is needed to capture user's input
		Scanner kb = new Scanner(System.in);
		
		//Display a welcome message to the user
		System.out.println("*************************************************");
		System.out.println("*      Welcome to Digital String Converter      *");
		System.out.println("*                Program created                *");
		System.out.println("*         Amit Malhotra & Abtin Farokhfal       *");
		System.out.println("*************************************************");

		//Let's ask the user for the string
		System.out.print("\nPlease enter exactly Four Words and separate each word by a \\ symbol: ");
		//Now read the value entered and store it in the appropriate variable
		sInputString=kb.next();
		kb.close(); //close Scanner object
		
		//Display the original string that the user entered
		System.out.println("Original String: " + sInputString);
		
		//and now manipulate the string
		
		//Isolate the first word
			//get the first index in the string where "\" occurs
			iSlashIndex = sInputString.indexOf("\\");
			
			//take the substring from 0 to the first "\" location and save it as the first word
			sWord1 = sInputString.substring(0,iSlashIndex);
			
			//save the remaining three words
			sInputString = sInputString.substring(++iSlashIndex);
			
		//Isolate the second word
			//get the location of the "\" in the remaining input string
			iSlashIndex = sInputString.indexOf("\\");
			
			//take the substring from 0 to the location and save it as the second word
			sWord2 = sInputString.substring(0,iSlashIndex);
			
			//Save the remaining script 
			sInputString = sInputString.substring(++iSlashIndex);
			
		//Isolate the third and fourth word
			//get the final location of the "\" in the remaining input string
			iSlashIndex = sInputString.indexOf("\\");
			
			//take the substring from 0 to the above location and save it as the third word
			sWord3 = sInputString.substring(0,iSlashIndex);
			
			//Save the remaining script - which should be the last word
			sWord4 = sInputString.substring(++iSlashIndex);
		
		//Print the converted string
		System.out.println("Converted String: " + sWord4 + " " +sWord3+ " " +sWord2+ " " +sWord1);
		
		//Let's show a thank you and an exit message indicating the end of program
		System.out.println("\n*************************************************");
		System.out.println("Thank you for using our awesome string converter!");
		System.out.println("*************************************************");


	} //end main method

} //end class


/* Output: 

*************************************************
*      Welcome to Digital String Converter      *
*                Program created                *
*         Amit Malhotra & Abtin Farokhfal       *
*************************************************

Please enter exactly Four Words and separate each word by a \ symbol: this\is\an\example
Original String: this\is\an\example
Converted String: example an is this

*************************************************
Thank you for using our awesome string converter!
*************************************************

*/
