package assignment2;
// ------------------------------------------------------------------
// Assignment 2
// Question: Part 2
// Written By: Amit Malhotra  & Abtin Farokhfal 
//
// --PhoneticTranslate.java--
//
// In Part 2 of Assignment 2 we were asked to create a program takes the user's entry and then converts
// each character of that string into its phonetic character (NATO's international standard phonetic alphabet).
// The program then echos back the converted string, without case sensitivity, excluding entered spaces,
// and prints back none-alphanumeric characters without converting them.
//
// Summary of program corresponding to assignment requirements:
// 	>Greet User
// 	>Prompt user to enter a line of text that they would like to convert using the Phonetic alphabet.
//	>Use a switch statement to convert each letter and number into its phonetic character.
//  >If the string contains a none-alphanumeric character then print the character without converting.
//  >Each phonetic word is separated by a space.
//	>The program should convert both upper and lower case letters.
//	>End with a thank you message.
// 
// Key Points: 
//  1) import of the following package(s):
//			i)java.util: needed to use the Scanner class in order to read user input from System.in
// 	2) class name and file name (PhoneticTranslate)
// 	3) main method
//  4) use of Scanner object in order to capture a line of text via the keyboard
//  5) closing of scanner object after user input capture to prevent a resource leak
//  6) use of toLowerCase() method to convert entered string to lower case and disregard case sensitivity
//  7) use of length() method to determine length of entered string
//  8) use of while loop to loop character by character for the entered string
//  9) use of if statement to disregard entered space characters
//  10)use of switch statement to match each character of the string to its equivalent Phonetic word
//  11)use of trim() to trim leading leading space from the newly created string
//  12)use of println to display various messages to the user including a welcome, prompt for input, phonetic conversion, and a program ending message
// 	13)program comments 
// ------------------------------------------------------------------

//Need to import the Scanner class to capture data from the user
import java.util.Scanner;

public class PhoneticTranslate { //start class

	//The program asks the user to enter a string and then  
	//outputs the string in the NATO phonetic alphabet for spelling
	
	public static void main(String[] args) {  //start main method
		
		//Declare variables
		String sInputString = ""; //used to store our Input String
		int iLength = 0; //used to store the length of our input string
		String sPhonetic = ""; //used to store the phonetic equivalent of each letter
		char cTemp = 0;  //used to read the string one character at a time, temp string
		String sOutput = ""; //used to store the output string
		int i = 0; //used in our loop to increase one character in the string, initialized at 0 
		
		//Create one scanner object which is needed to capture user's input
		Scanner kb = new Scanner(System.in);
		
		//Display a welcome message to the user
				System.out.println("*********************************************");
				System.out.println("*    Welcome to Phonetic String Translator  *");
				System.out.println("*              Program created by           *");
				System.out.println("*       Amit Malhotra & Abtin Farokhfal     *");
				System.out.println("*********************************************");
				
		//Let's ask the user for a string
		System.out.print("\nEnter the message that you would like to translate: ");
		//Store the message
		sInputString = kb.nextLine();
		kb.close();
		
		//Convert string to lower case for easier manipulation
		sInputString = sInputString.toLowerCase();
		//Find out the length of the string
		iLength = sInputString.length();

		//The following loop will read each character in the string one at a time
		//until the variable i is less than the length of the string calculated above
		//and after each letter, i will be incremented
		while (i < iLength) { //start While loop
			
			cTemp = sInputString.charAt(i); //store the character at location 0 in cTemp 
			
			//Use IF to ignore any spaces as we will manage them in our output string
			//If it's not a space, then manipulate string, else move to next character
				if (cTemp != ' '){    //start If
			
					//Switch case will find the phonetic equivalent of each letter 
					//in the string, as stored in the cTemp variable and store the equivalent
					//in the sPhonetic variable preceeded by a space
					switch (cTemp) { //start Switch Case
						case 'a': sPhonetic = " Alpha";
							break;
						case 'b': sPhonetic = " Bravo";
							break;
						case 'c': sPhonetic = " Charlie";
							break;
						case 'd': sPhonetic = " Delta";
							break;
						case 'e': sPhonetic = " Echo";
							break;
						case 'f': sPhonetic = " Foxtrot";
							break;
						case 'g': sPhonetic = " Golf";
							break;
						case 'h': sPhonetic = " Hotel";
							break;
						case 'i': sPhonetic = " India";
							break;
						case 'j': sPhonetic = " Juliet";
							break;
						case 'k': sPhonetic = " Kilo";
							break;
						case 'l': sPhonetic = " Lima";
							break;
						case 'm': sPhonetic = " Mike";
							break;
						case 'n': sPhonetic = " November";
							break;
						case 'o': sPhonetic = " Oscar";
							break;
						case 'p': sPhonetic = " Papa";
							break;
						case 'q': sPhonetic = " Quebec";
							break;
						case 'r': sPhonetic = " Romeo";
							break;
						case 's': sPhonetic = " Sierra";
							break;
						case 't': sPhonetic = " Tango";
							break;
						case 'u': sPhonetic = " Uniform";
							break;
						case 'v': sPhonetic = " Victor";
							break;
						case 'w': sPhonetic = " Whiskey";
							break;
						case 'x': sPhonetic = " X-Ray";
							break;
						case 'y': sPhonetic = " Yankee";
							break;
						case 'z': sPhonetic = " Zulu";
							break;
						case '1': sPhonetic = " One";
							break;
						case '2': sPhonetic = " Two";
							break;
						case '3': sPhonetic = " Three";
							break;
						case '4': sPhonetic = " Four";
							break;
						case '5': sPhonetic = " Five";
							break;
						case '6': sPhonetic = " Six";
							break;
						case '7': sPhonetic = " Seven";
							break;
						case '8': sPhonetic = " Eight";
							break;
						case '9': sPhonetic = " Nine";
							break;
						case '0': sPhonetic = " Zero";
							break;
						//the default case will store anything that is not an alphanumeric character
						//exactly as it is in the sPhonetic variable
						default: sPhonetic = String.valueOf(cTemp);
							break;
					} //end Switch Case
				
				//concatenate the sPhonetic in the output variable
				sOutput += sPhonetic;
			
				} //end If
			
			i++; //increment i to look at the next character and go back to the loop
			
		} //end While Loop

		//Let's trim the output to remove the leading space in the output
		sOutput = sOutput.trim();
		//output the translated string
		System.out.println("\nHere's your message in its Phonetic equivalent form: ");
		System.out.println(sOutput);
		System.out.println("");
		System.out.println("You can use the above output if you want to spell your message on the phone to someone!");
		System.out.println("\n********************************************");
		System.out.println("Thank you for using our Phonetic Translator!"); //exit message
		System.out.println("********************************************");
		
		
	} //end main method

} //end class

/* Output: 

*********************************************
*    Welcome to Phonetic String Translator  *
*              Program created by           *
*       Amit Malhotra & Abtin Farokhfal     *
*********************************************

Enter the message that you would like to translate: This is test 1, of X!!!

Here's your message in its Phonetic equivalent form: 
Tango Hotel India Sierra India Sierra Tango Echo Sierra Tango One, Oscar Foxtrot X-Ray!!!

You can use the above output if you want to spell your message on the phone to someone!

********************************************
Thank you for using our Phonetic Translator!
********************************************

*/
