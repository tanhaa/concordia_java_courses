package assignment1;
// ------------------------------------------------------------------
// Assignment 1
// Question: Part 1
// MyFirstProgram.java Written By: Amit Malhotra 
// This program simply displays a welcome message to the user 
// ------------------------------------------------------------------

public class MyFirstProgram {  // Start class

	//
	//Displays a welcome message to the user
	//
	
	public static void main(String[] args) { // Start main method
		
		//Declare three string variables for name, date and time
		//This will allow modifying these variables if needed without modifying the code
	
		String sName = "Amit Malhotra";
		String sDate = "September 28, 2012";
		String sTime = "09:00 AM";
		
		//Print each of the statements below to standard output
		
		System.out.println("Welcome to My First Java Program!");
		System.out.println("This Program was written by: " + sName + ".");
		System.out.println("This assignment was done individually on " + sDate + " at " + sTime + ".");
		System.out.println("End of Program!");
		
	} //End main method

} //End MyFirstProgram class
