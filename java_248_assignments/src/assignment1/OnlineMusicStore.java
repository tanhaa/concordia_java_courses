package assignment1;
// ------------------------------------------------------------------
// Assignment 1
// Question: Part 2
// OnlineMusicStore.java Written By: Amit Malhotra 
// This program displays the amount of songs a user can buy  
// based on the amount of money they want to spend and the 
// amount of money that will remain in the user's account
// ------------------------------------------------------------------

//Need to import the Scanner class to capture data from the user
import java.util.Scanner;

public class OnlineMusicStore {  // Start Class

	//
	// The program asks the user to enter the amount they want to spend
	// and displays the amount of songs they can purchase and their remaining balance
	//
	
	public static void main(String[] args) {  //Start main method
		
		// Declare variables
		
		int iAmntPrepaid, iAmntRemaining, iNumOfSongs; //all values will be used as integers
		final int iPriceOfSong = 3;   //Declaring the price of song as a constant 
		
		//Create one scanner object which is needed to capture user's input
		Scanner keyboard = new Scanner(System.in); 
		
		//Display a welcome message to the user
		System.out.println("*********************************************");
		System.out.println("* Welcome to Digital Revolution Music Store *");
		System.out.println("*     Program created by Amit Malhotra      *");
		System.out.println("*********************************************");

		//Let's ask the user how much he wants to spend using "print"
		System.out.print("Please Enter the amount (Dollar Value only) that you want to prepay for your purchase: ->");
		
		//Now read the value entered and store it in the appropriate variable
		iAmntPrepaid = keyboard.nextInt();
		keyboard.close();
		
		//Let's show and appreciate the user for the amount he/she is prepaying
		System.out.println("Thank you for prepaying your account in the amount of $" + iAmntPrepaid + " Dollars.");
		
		//Some back-end calculations 
			//calculate the number of songs the user can buy with his balance
			iNumOfSongs = iAmntPrepaid / iPriceOfSong;
			
			//calculate the remaining amount after the purchase
			iAmntRemaining = iAmntPrepaid % iPriceOfSong;
		
		//Let's show how many songs the user can buy with the prepaid amount
		System.out.println("With your current balance in the account, you can purchase " + iNumOfSongs + " awesome song(s) from our library.");
		
		//Show user his/her remaining balance
		System.out.println("After your current purchase of " +iNumOfSongs+ " song(s), you will have a balance of $" +iAmntRemaining+ " remaining in your account.");
		
		//Let's show a thank you and an exit message indicating the end of program
		System.out.println("Digital Revolution Music Store thanks you for your business.");

	} //End main

} //End OnlineMusicStore Class
