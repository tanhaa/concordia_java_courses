//------------------------------------------------------------------
//Name and ID: Amit Malhotra (5796997), Navpreet Singh Gill 7155522
//Comp 352
//Assignment #3
//Due Date: 4th of April 2014
//--------------------------------------------------------------------

public class FlexHeapDriver {
	
	public static void main(String[] args) {
		
		//let's create a quick empty binary tree of Strings
		//though we will use only characters
		FlexHeap<String> myFH = new FlexHeap<String>();
		System.out.println(myFH);
		FlexHeap<String> my2FH = new FlexHeap<String>("A");
		System.out.println(my2FH);
		my2FH.printFlexHeap(0, 0);
		my2FH.add("C");
		my2FH.add("F");
		my2FH.add("D");
		my2FH.add("E");
		my2FH.add("B");
		System.out.println(my2FH);
		
		my2FH.printFlexHeap(0, 0);
		System.out.println("\n---------------------------------");
		System.out.println("Let's remove top element " + my2FH.remove());
		my2FH.printFlexHeap(0, 0);
		System.out.println("Hidden CTR: " + my2FH.hiddenCtr);
		my2FH.toggleHeap();
		System.out.println("Hidden CTR: " + my2FH.hiddenCtr);
		System.out.println("Size: " + my2FH.size());
		System.out.println(my2FH);
		my2FH.printFlexHeap(0, 0);
		System.out.println("Adding more elements");
		my2FH.add("A");
		my2FH.add("X");
		my2FH.add("Y");
		my2FH.add("Z");
		my2FH.add("a");
		System.out.println(my2FH);
		my2FH.printFlexHeap(0, 0);
		my2FH.hiddenCtr = 0;
		System.out.println("Hidden CTR: " + my2FH.hiddenCtr);
		my2FH.toggleHeap();
		System.out.println("Hidden CTR: " + my2FH.hiddenCtr);
		System.out.println("Size: " + my2FH.size());
		System.out.println(my2FH);
		my2FH.printFlexHeap(0, 0);
		MyArrayList<String> sortedArray = my2FH.heapSort();
		System.out.println("The heap sorted everything, here it is: ");
		System.out.println(sortedArray);
		System.out.println("Our heap is empty?");
		System.out.println(my2FH);
		
		System.out.println("Question 2:");
		FlexHeap<Integer> q2 = new FlexHeap<Integer>();
		//15 27 20 19 16 13 10 17 11 17 23 9 10 6 18
		q2.add(15); q2.add(27);q2.add(20);q2.add(19);q2.add(16);q2.add(13);
		q2.add(10); q2.add(17); q2.add(11);q2.add(17);q2.add(23);q2.add(9);q2.add(10);q2.add(6);q2.add(18);
		System.out.println(q2);
		q2.printFlexHeap(0, 0);
	}
	
}
