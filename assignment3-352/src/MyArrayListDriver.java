//------------------------------------------------------------------
//Name and ID: Amit Malhotra (5796997), Navpreet Singh Gill 7155522
//Comp 352
//Assignment #3
//Due Date: 4th of April 2014
//--------------------------------------------------------------------

import java.util.Iterator;

public class MyArrayListDriver {
	public static void main(String[] args) {
		
		//lets create a quick ArrayList of integers
		System.out.println("Let's create a new array of Integers");
		MyArrayList<Integer> myArray = new MyArrayList<Integer>();
		System.out.println("Here is the array:");
		System.out.println(myArray);
		System.out.println("Test isEmpty()");
		System.out.println(myArray.isEmpty());
		System.out.println("We will add an item to the array now");
		myArray.add(1);
		System.out.println(myArray);
		System.out.println("Test get(int i) method at index 1");
		System.out.println(myArray.get(1));
		System.out.println("------------------------------------------------------------------");
		System.out.println("Add more items");
		myArray.add(2); myArray.add(3); myArray.add(4);
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Testing add(index, element) to add at index 0, does array shift?");
		myArray.add(0,0);
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Add more items and see if it will double capacity");
		myArray.add(5); myArray.add(6);	myArray.add(7); myArray.add(8); myArray.add(9); myArray.add(10); 
		myArray.add(11);
		System.out.println("What does it look like");
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Lets remove at the last index, 11");
		System.out.println(myArray.remove(11)); 
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Lets find the element 10 and remove it");
		System.out.println(myArray.remove(new Integer(10)));
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("NOw remove more items");
		System.out.print(myArray.remove(9)); System.out.print(myArray.remove(8)); System.out.print(myArray.remove(7));
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Let's remove at an internal index to test shift, at 0");
		System.out.println(myArray.remove(0));
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Let's remove more items to see if it truncates capacity when at 25%");
		System.out.print(myArray.remove(5)); System.out.print(myArray.remove(4));
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Let's remove at an index with no element, should give NoSuchElementException, we will catch it and show");
		try{
			System.out.println(myArray.remove(10));
		}catch(Exception e){
			System.out.println(e);
		}
		System.out.println("Let's remove at an index > capacity, should give IndexOutOfBoundException, we will catch it and show it");
		try{
			System.out.println(myArray.remove(20));
		}catch(Exception e){
			System.out.println(e);
		}
		System.out.println("------------------------------------------------------------------");
		System.out.println("Testing size() to see what the current size is");
		System.out.println(myArray.size());
		System.out.println("------------------------------------------------------------------");
		System.out.println("Testing capacity() to see what the current capacity is");
		System.out.println(myArray.getCapacity());
		System.out.println("------------------------------------------------------------------");
		System.out.println("Remove item by element and not index, remove 1, should say True if succesfull");
		System.out.println(myArray.remove(new Integer(1)));
		System.out.println(myArray);
		System.out.println("------------------------------------------------------------------");
		System.out.println("Find the indexOf Element 2 now, should give 0");
		System.out.println(myArray.indexOf(new Integer(2)));
		System.out.println("Set the item at index 0 with 200, will return 2");
		System.out.println(myArray.set(0, new Integer(200)));
		System.out.println(myArray);
		System.out.println("Test our iterator now");
		Iterator<Integer> itr = myArray.iterator();
		System.out.println(itr);
		System.out.println("Let's do a while look with hasNext and print all remaining items");
		while(itr.hasNext()) {
	         Object element = itr.next();
	         System.out.print(element + " ");
	      }
		System.out.println("\nWe have an iterator, so foreach loop should work as well");
		for(Integer item: myArray){
			System.out.print(item + " ");
		}
		System.out.println("\nAll methods of ArrayList tested");
		
	}
	
}
