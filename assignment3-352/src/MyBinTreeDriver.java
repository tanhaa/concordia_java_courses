//------------------------------------------------------------------
//Name and ID: Amit Malhotra (5796997), Navpreet Singh Gill 7155522
//Comp 352
//Assignment #3
//Due Date: 4th of April 2014
//--------------------------------------------------------------------


public class MyBinTreeDriver {

	public static void main(String[] args) {
		
		//let's create a quick empty binary tree of Strings
		//though we will use only characters
		BinTree<String> myBT = new BinTree<String>();
		System.out.println(myBT);
		System.out.println("Testing isEmpty?");
		System.out.println(myBT.isEmpty())
		;
		BinTree<String> my2BT = new BinTree<String>("A");
		System.out.println(my2BT);
		my2BT.printBinaryTree(0, 0);
		my2BT.add("B");
		my2BT.add("C");
		my2BT.add("D");
		my2BT.add("E");
		my2BT.add("F");
		my2BT.add("G");
		my2BT.add("H");
		my2BT.add("I");
		my2BT.add("J");
		System.out.println(my2BT);
		my2BT.printBinaryTree(0, 0);
		System.out.println("\n---------------------------------");
		System.out.println("Testing the Find method on 'A'. Returns index (key)");
		System.out.println(my2BT.find("A"));
		System.out.println("Testing remove method, removing " + my2BT.remove(1));
		my2BT.printBinaryTree(0, 0);
		
	}

}
