import java.util.NoSuchElementException;

/**
 * THis is our Heap ADT implementation. It implements our BinTree implementation
 * so we can use all those methods.   
 *  
 * @author Amit
 *
 * @param <T>
 */
public class FlexHeap<T extends Comparable<T>>{
	
	private BinTree<T> binTree;
	private Boolean minHeap = true; //minHeap by default, setting it to false = maxHeap
	public int hiddenCtr;
	
	/**
	 * Default Constructor. This will create an empty heap. min by default
	 */
	public FlexHeap(){
		//constructor default.
		this.binTree = new BinTree<T>();
		this.minHeap = true;
	}
	
	/**
	 * Takes an element and puts it on the top of the Heap
	 * @param t
	 */
	public FlexHeap(T t){
		//put this element at the root
		this.binTree = new BinTree<T>(t);
		this.minHeap = true;
	}
	
	/**
	 * Constructor that takes an array and a boolean to make a heap out of that
	 * array.  The boolean is to indicate whether we want a min heap or not. 
	 * @param tArr a MyArrayList of elements <T>
	 * @param min  true will make a minHeap, false will make a maxHeap
	 */
	public FlexHeap(MyArrayList<T> tArr, boolean min){
		int size = tArr.size();
		this.minHeap = min;
		this.binTree = new BinTree<T>(size);
		// add all the elements in our 
		for (T t: tArr){
			//just add the value from the array to our heap
			this.binTree.add(t);
		}
		//now heapify, that is adjust only the elements that have a child
		for(int i = (int)Math.floor(size/2); i>=1; i--){
			this.moveDown(i);
		}
	}
	/**
	 * toggleHeap() transforms a min- to a max-heap or vice versa.
	 */
	public void toggleHeap(){
		if (this.minHeap){
			this.switchMaxHeap();
		} else {
			this.switchMinHeap();
			this.minHeap = true;
		}
	}
	
	/**
	 * switchMinHeap() transforms a max- to a min-heap. 
	 */
	public void switchMinHeap() {
		//turn this into a min heap
		this.minHeap = true;
		//get the size of the heap
		int size = this.binTree.size();
		//and now loop through it, starting at the lowest
		//level with children and just move them down
		//adjust them properly
		for(int i = (int)Math.floor(size/2); i>=1; i--){
			hiddenCtr++;
			this.moveDown(i);
		}
		
	}
	
	/**
	 * switchMaxHeap() transforms a min- to a max-heap.
	 */
	public void switchMaxHeap() {
		//turn this into a max heap
		this.minHeap = false;
		//get the size of the heap
		int size = this.binTree.size();
		//and now loop through it, starting at the lowest level with
		//children and adjust them just like in heapify
		for(int i = (int)Math.floor(size/2); i>=1; i--){
			this.moveDown(i);
		}
	}
	

	/**
	 * THis method will add the new element to the end of the heap
	 * and then move it up if it violates the heap property
	 * @param t
	 */
	public void add(T t){
		//we will always add at the end
		
		//lets get the index where we are adding our element
		int addedIndex = this.binTree.size()+1;
		//add the element
		this.binTree.add(t);
		//move the element up
		this.moveUp(addedIndex);
	}
	
	/**
	 * This method will compare element at index i with parent
	 * and will swap them if they violate our required heap policy
	 * @param i
	 */
	public void moveUp(int i){ 
		
		//parent is at (int)Math.floor(i/2) && root is at 1
		
		if(this.minHeap){ //minimum on top
			//check if we are below root and if element is less than parent -> swap
			while (i>1 && less(i, (int)Math.floor(i/2))){
				this.binTree.swap(i,  (int)Math.floor(i/2));
				i = (int)Math.floor(i/2);
				}
		} else {  //this is a maxHeap, max on top
			
			//check if we are below root and element is bigger than parent -> swap
			while(i>1 && !less(i, (int)Math.floor(i/2))){
				this.binTree.swap(i, (int)Math.floor(i/2));
				i = (int)Math.floor(i/2);
			}
		}
	}
	
	/**
	 * This will check if the parent element at index i violates heap policy and if so
	 * move it down to restore order
	 * @param i
	 */
	public void moveDown(int i){
		
		int treeSize = this.binTree.size();
		//check if 2*i, where the left child would be is
		//less than or equal to our size at least, so we don't go above our tree size
		
		if(minHeap){ //smaller element is parent
			
			//the left child of i is at 2*i
			while(2*i <= treeSize){
				this.hiddenCtr++;
				int childIndex = 2*i; //left child
				
				//let's compare the two child which will only happen if childIndex is 
				//less than our tree size.  If it's equal, then it's the only child
				if (childIndex < treeSize && this.less(childIndex+1, childIndex)){
					//if right is smaller than left, then set it to right child
					childIndex++;
				}
				
				//now check if the child is actually smaller than the parent 		
				if (this.less(childIndex, i)){
					//true? swap it.
					this.binTree.swap(i,  childIndex);
				} else {
					//break the loop as parent is bigger than both children
					break;
				}
				
				//so we swapped, now let's set i to this child and go down in the tree.
				i = childIndex;
				
			}
			
		} else { //it's a maxHeap, bigger element is parent
			//the left child of i is at 2*i
			while(2*i <= treeSize){
				this.hiddenCtr++;
				int childIndex = 2*i; //left child
				
				//let's compare the two child and take the bigger of two
				//which will only happen if childIndex is less than our tree size.  
				//If it's equal, then it's the only child
				if (childIndex < treeSize && this.less(childIndex, childIndex+1)){
					//if right is bigger than left, then set it to right child
					childIndex++;	
				}
				
				//now check if the child is actually bigger than the parent 		
				if (this.less(i, childIndex)){
					//true? swap it.
					this.binTree.swap(i,  childIndex);
				} else {  
					//break the loop as parent is bigger than the bigger child!
					break;
				}
				
				//so we swapped, now let's set i to this child and go down in the tree.
				i = childIndex;
			}
		}
	}
	
	
	
	/** 
	 * Checks if element at index i is less than the element at index j
	 * @param i
	 * @param j
	 * @return
	 */
	public boolean less(int i, int j){
		return this.binTree.element(i).compareTo(this.binTree.element(j)) < 0;
	}
	
	/**
	 * Removes the max/min element which sits at the top of the heap
	 * and returns it
	 * @return
	 */
	public T remove(){
		//remove the top element, min-heap or max-heap
		//and repairs the flex-heap afterwards accordingly.
		//get the lastIndex
		int lastIndex = this.binTree.size();
		
		if(lastIndex == 1){ //we are at the end
			return this.binTree.remove(1);
		}
		//swap the value in root with the value at the end
		this.binTree.swap(1, lastIndex);
		//now remove the lastIndex, its like removing an external node, simple
		T toReturn = (T) this.binTree.remove(lastIndex);
		//now we move Down
		this.moveDown(1);
		
		return toReturn;
	}
	
	/**
	 * This returns the size (amount of nodes) in the heap.
	 * @return
	 */
	public int size(){
		return this.binTree.size();
	}
	
	/**
	 * returns true if the heap is empty
	 */
	public boolean isEmpty(){
		return this.binTree.size() == 0;
	}
	
	/**
	 * This returns a MyArrayList which contains everything from the heap
	 * sorted out.  It runs in O(nlogn) time. 
	 * @return
	 */
	public MyArrayList<T> heapSort(){
		MyArrayList<T> sortedArray = new MyArrayList<T>(this.size());
		while (this.size()>=1){
			sortedArray.add(this.remove());
		}
	
		return sortedArray;
		
	}
	
	/**
	 * Returns the height of the tree
	 * @return
	 */
	public int height(){
		return this.binTree.treeHeight();
	}
	
	/**
	 * This method will print our heap in a binary tree form
	 * @param index
	 * @param level
	 */
	public void printFlexHeap(int index, int level){
		this.binTree.printBinaryTree(index, level);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(minHeap)  
			return "MinHeap: " + this.binTree;
		return "MaxHeap: " + this.binTree;
	}
}
