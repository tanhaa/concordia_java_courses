import java.io.IOException;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This is my ArrayList implementation
 * 
 * @param <T>
 */
public class MyArrayList<T> implements Iterable<T>{
	
	//the parameters
	private Object[] myArray;
	private int size = 0; //this will keep track of the size of our stack and our last element
	private int capacity = 0; //this will keep track of our capacity
	
	/**
	 * Default constructor, makes an array of capacity 10
	 */
	public MyArrayList(){
		this.myArray = new Object[11];
		this.size = 0;
		this.capacity = 11;
		
	}
	
	/**
	 * Parametrized constructor that takes an initial defined capacity as int
	 * 
	 * @param n  (capacity of the myArrayList)
	 */
	public MyArrayList(int n){
		this.myArray = new Object[n];
		this.size = 0;
		this.capacity = n;
	}
	
	
	
	/**
	 * This method checks to see if our stack is empty and return true or false accordingly
	 * @return boolean true or false
	 */
	public boolean isEmpty(){
		return (size==0);	
	}
	
	/**
	 * This method will return the size of our Array
	 * @return size as an int
	 */
	public int size(){
		return this.size;
	}
	
	/**
	 * This method will resize our underlying array with the 
	 * new size provided
	 * @param newCapacity
	 */
	private void resize(int newCapacity){
		Object[] newArray = new Object[newCapacity];
				
		//now we will copy everything from our old array to our new array
		for (int i = 0; i <= this.size; i++){
			//this will only copy the references in our new array, that's what we want
			//we don't need a deep copy here because we do want to keep our array
			if(!(myArray[i] == null))
				newArray[i] = myArray[i];
		}
			
		this.capacity = newCapacity; //set our capacity to new capacity
		//point / reference our stkArray to our new array so that old array is garbage collected
		this.myArray = newArray;
		newArray = null; //point our temp array to null	
		
	}
	
	
	/**
	 * This method will return the current capacity of our ArrayList
	 * @return capacity as an int
	 */
	public int getCapacity(){
		
		return this.capacity;
	}
	
	/**
	 * This method will add the item being passed at the
	 * last free index
	 * @param t
	 */
	public void add(T t){
		//adds the object at the end of the array
		
		//check if our capacity is at or above 75% and if yes, then resize
		
		if(((this.size * 100.0)/this.capacity) >=75){
			this.resize(this.capacity*2);
		}
		//put the element at the end
		myArray[size] = t;
		this.size++; //increase our size by 1
	}
	
	/**
	 * This method will add the item being passed at the index
	 * being provided. 
	 * @param i index where the item should be added
	 * @param t object to be added. 
	 */
	public void add(int i, T t){
		//adds the object at the given index and adjusts the 
		
		//check if our capacity is at or above 75% and if yes, then resize
		if(((this.size * 100.0)/this.capacity) >=75){
			this.resize(this.capacity*2);
		}
		
		//lets adjust our array now
		for (int j = this.size; j > i; j--){
			myArray[j] = myArray[j-1];
		}
		
		//now put the element at the index desired
		myArray[i] = t;
		
		//increase our size by 1
		this.size++; //increase our size by 1
		
	}
	
	/**
	 * This method removes the object at the index being provided and
	 * returns it to the caller
	 *
	 * @return the removed item 
	 * @throws IndexOutOfBoundsException if index is bigger than the capacity of our array
	 * @throws NoSuchElementException if index is bigger than or equal to the size
	 * of the array and there is no element at that index.
	 */
	@SuppressWarnings("unchecked")
	public T remove(int i) throws IndexOutOfBoundsException, NoSuchElementException {
		
		T removed = null;
		
		if(i>this.capacity){
			throw new IndexOutOfBoundsException();
		}
		
		if(i>=this.size && this.myArray[i] == null){ 
			//we are looking for an element where there is no element
			throw new NoSuchElementException();
		}
		
		if(this.myArray[i] != null){ //do we have a node at that loc?
			removed = (T)this.myArray[i];
			this.myArray[i] = null;
		}
		
		//we must adjust now
		for (int j = i; j<this.size; j++){
			myArray[j] = myArray[j+1];
		}
		
		this.size--; //reduce our size	
		
		//check if our capacity is at or below 25% and if yes, then resize/2
		if(((this.size * 100.0)/this.capacity) <=25){
			this.resize(this.capacity/2);
		}
		
		return removed;		
	} 
	
	public boolean remove(T t) throws IndexOutOfBoundsException {
		T removed;
		int i = indexOf(t);
		try {
			removed = remove(i);
			
			if (removed == null){
				return false;
			} else {		
				return true;
			}	
		} catch (IndexOutOfBoundsException e) {
			throw e;
		}
	}
	
	/**
	 * Returns the index of a given element.  Works in O(n). 
	 * @param t
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int indexOf(T t){
		//a==b || (a!=null && a.equals(b))
		for(int i=0; i<this.size; i++){
			T a = (T)myArray[i];
			
			if(a == t || a!= null && a.equals(t)){
				return i;
			}
		}
	
		return -1;
	}

	
	/**
	 * Returns the item at the provided index i
	 * @param i
	 * @return
	 * @throws IndexOutOfBoundsException
	 */
	@SuppressWarnings("unchecked")
	public T get(int i) throws IndexOutOfBoundsException{
		if(i >= this.capacity)
			throw new IndexOutOfBoundsException();
		
		return (T)this.myArray[i];
	}
	
	/**
	 * Replaces the existing object at index i with the given object
	 * and returns the existing object to the caller. 
	 * 
	 * @param i
	 * @param t
	 * @return
	 * @throws IndexOutOfBoundsException
	 */
	@SuppressWarnings("unchecked")
	public T set(int i, T t) throws IndexOutOfBoundsException {
		if(i>this.capacity){
			throw new IndexOutOfBoundsException();
		}
		T existingT = (T)myArray[i];
		myArray[i] = t;
		return existingT;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String returnString = "[ArrayList] Content = [";
		for(int i = 0; i<this.capacity; i++){
			if(myArray[i] != null){
				returnString = returnString + myArray[i] + " ";
			} else {
				returnString = returnString + "-" + " " ;
			}
			
		}
		returnString = returnString + " ] \nCurrent Array Size = " + this.size + "\tTotal Array Capacity = " + 
				this.capacity;
		return returnString;
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}
	
	/**
	 * This is our internal iterator class
	 * @author Amit
	 *
	 */
	private class ListIterator implements Iterator<T>
	{
		private int i = 0;
		
		public boolean hasNext(){
			return i < capacity && myArray[i] != null;
		}
		
		public void remove(){
			/*not supported*/
		}
		
		@SuppressWarnings("unchecked")
		public T next(){
			return (T)myArray[i++];
		}
	}
}
