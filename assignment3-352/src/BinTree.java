/*************************************************************************************
 * This class implements the binary tree ADT. 
 * Properties of binary trees:
 * Every node has at most two children
 * each child is labeled as a left child or a right child
 * A left child precedes a right child in the ordering of children of a node
 * A tree is proper if each node has either zero or two children, every internal
 * node has exactly two children... otherwise it's a improper tree
 * Implemented using array-list.  For every node v, p(v) is an integer
 * if v is the root of T, then p(v) = 1
 * if v is the left child of node u, then p(v) = 2p(u) 
 * if v is the right child of node u, then p(v) = 2p(u) + 1
 * @author Amit
 *
 *************************************************************************************/
import java.util.Iterator;
import java.util.NoSuchElementException;

public class BinTree<T> implements Iterable<T> {
	
	//our parameters
	private Object root = null; //this will keep track of the element at root
	/* we will use our myArrayList ADT and pass a generic to it. For key, we will
	 * use the index of that value and store the value (our generic) in our ArrayList.
	 */
	private MyArrayList<T> binArray; //our arraylist that stores our tree nodes
	private int count; //this will keep track of our total counts
	
	/**
	 * Default constructor creates a binarytree with 15 empty spots
	 */
	public BinTree(){
		//default constructor creates a new empty binary tree
		this.binArray = new MyArrayList<T>(15);
		count = 0;
	}
	
	/**
	 * Parametrized constructor takes one element and adds it as a root
	 * of a tree of capacity 15. Updates size to 1, adds root to index 1
	 * @param t generic object
	 */
	public BinTree(T t){
		//this will create a binary tree with the given element at the root
		this.binArray = new MyArrayList<T>(15);
		this.binArray.add(1, t);	
		root = t; //set it as our root, we can find it quick. Index is 1.
		count = 1; //we have one element in our tree!
	}
	
	/**
	 * Parametrized constructor takes an int n to indicate the max capacity
	 * of the new tree being created
	 * @param n integer
	 */
	public BinTree(int n){
		//this will create a binary tree with the given number
		this.binArray = new MyArrayList<T>(n);
		count = 0;
	}
	
	/**
	 * Parametrized constructor takes two parameters, an int n for the max 
	 * capacity of the tree and an element <T> t as the root of the tree
	 * @param t generic object
	 * @param n integer n
	 */
	public BinTree(T t, int n){
		//this will create a binary tree with the given number and add the 
		//the given element as root
		this.binArray = new MyArrayList<T>(n);
		this.binArray.add(1, t);
		root = t;
		count = 1;
	}
	
	public BinTree(int n, T t){
		//this will create a binary tree with the given number and add the 
		//the given element as root
		this.binArray = new MyArrayList<T>(n);
		this.binArray.add(1, t);
		root = t;
		count = 1;
	}
	
	/**
	 * Tests whether the tree has any nodes or not
	 * @return  boolean true if tree doesn't have any node 
	 */
	public boolean isEmpty(){
		return (this.count == 0);
	}
	
	/**
	 * Returns the size of the tree
	 * @return int size
	 */
	public int size(){
		return this.count;
	}
	
	/**
	 * Returns the root of the tree, raises an error if empty
	 * @return object T 
	 * @throws NoSuchElementException if root is null
	 */
	@SuppressWarnings("unchecked")
	public T root() throws NoSuchElementException{
		if (this.root == null){
			throw new NoSuchElementException();
		} else {
			return (T)this.element(1);
		}
		
	}
	
	/**
	 * Returns the parent of element v, raises an error if v is the root
	 * or if v does not exist in the tree.  This runs in O(n) as it checks
	 * through the tree and finds the index. 
	 * @param object T 
	 * @return parent of v
	 */
	public T parent(T v){
		int index = this.find(v);
		//if the index of v is odd, then it's the right child
		//if the index is even, then it's the left child 
		return (T)this.binArray.get((int) Math.floor(index/2));
	}
	
	/**
	 * Returns the parent of element at index i, raises an error if the element
	 * is the root. 
	 * 
	 * @param index
	 * @return object T
	 * @throws NoSuchElementException
	 */
	public T parent(int index) throws NoSuchElementException{
		if(index <= 1){//we want the parent of root! No Can Do.
			throw new NoSuchElementException();
		} else { //we have a valid index
			return (T)this.binArray.get((int)Math.floor(index/2));
		}
	}
	
	/**
	 * Returns the index of the parent for the given index. Raises an error if 
	 * want the parent of root. 
	 * 
	 * @param index
	 * @return integer (index)
	 * @throws NoSuchElementException
	 */
	public int parentIndex(int index) throws NoSuchElementException{
		if(index<=1){ // we want the parent of root! no can do.
			throw new NoSuchElementException();
		} else { //we have a valid index
			return (int)Math.floor(index/2);
		}
	}
	
	/**
	 * Returns the Left Child of element v, raises an error if v does not
	 * have a left child.  This runs in O(n) as it checks
	 *  through the tree and finds the index. 
	 * @param v
	 * @return leftchild of v, object T
	 */
	public T left(T v) throws NoSuchElementException{
		int index = this.find(v);
		if (this.hasLeft(index)){
			return (T)this.binArray.get(2*index);
		} else {
			throw new NoSuchElementException();
		}
		
	}
	
	/**
	 * Returns the Left Child of element at index i, raises an error if there
	 * is no left child
	 * 
	 * @param index
	 * @return Object T
	 * @throws NoSuchElementException
	 */
	public T left(int index) throws NoSuchElementException{
		if (this.hasLeft(index)){
			return (T)this.binArray.get(2*index);
		} else {
			throw new NoSuchElementException();
		}
	}

	/**
	 * Returns the Right Child of element v, raises an error if v does not
	 * have a right child.  This runs in O(n) as it checks
	 *  through the tree and finds the index. 
	 * @param v
	 * @return right child of v
	 */
	public T right(T v) throws NoSuchElementException{
		int index = this.find(v);
		if (this.hasRight(index)){
			return (T)this.binArray.get((2*index)+1);
		} else {
			throw new NoSuchElementException();
		}
		
	}
	
	/**
	 * Returns the Right Child of element at index i, raises an error if there
	 * is no right child
	 * 
	 * @param index
	 * @return Object T
	 * @throws NoSuchElementException
	 */
	public T right(int index) throws NoSuchElementException{
		if (this.hasRight(index)){
			return (T)this.binArray.get((2*index)+1);
		} else {
			throw new NoSuchElementException();
		}
	}

	
	/**
	 * Returns the element stored at a particular position denoted
	 * by our index or also our key if we associate key+value combos
	 * Will return null if the index provided is greater than tree capacity or 
	 * throw a NoSuchElementException if there's nothing there
	 * @param index
	 * @return
	 */
	public T element(int index) throws NoSuchElementException{
		if(index < 1){//we want at index 0, we have nothing there. 
			throw new NoSuchElementException();
		} else { //we have a valid index
			try{
				return (T)this.binArray.get(index);
			} catch (IndexOutOfBoundsException e){
				return null;
			}
			
		}
	}
	
	/**
	 * Returns the height of the tree. 
	 * @return
	 */
	public int treeHeight(){
		return (int)Math.floor((Math.log(this.count)/Math.log(2)));
		
	}
	
	/**
	 * Returns whether an element is at the root of a tree or not. 
	 * @param t
	 * @return
	 */
	public boolean isRoot(T t){
		return (this.element(1).equals(t));
	}
	
	/**
	 * Returns the index of an element if found, if not, throws an exception
	 * Runs in O(n) as we have to iterate in the tree to find the value
	 * @param t
	 * @return
	 * @throws NoSuchElementException
	 */
	public int find(T t) throws NoSuchElementException{
		int index = this.binArray.indexOf(t);
		
		if (index == -1){
			throw new NoSuchElementException();
		} else { 
			return index;
		}
	}
		
	/**
	 * Returns whether an element is in the tree or not
	 * @param t
	 * @return
	 */
	public boolean contain(T t){
		return (this.binArray.indexOf(t)!=-1);
	}
	
	/**
	 * Returns whether a key (index, node) has a left child or not
	 * if v is the left child of node u, then p(v) = 2p(u)
	 * @param index int (key)
	 * @return true or false
	 */
	public boolean hasLeft(int index){	
		try{
			return (this.binArray.get(2*index)!=null);
		} catch (IndexOutOfBoundsException e){ //we tried to go outside our index!
			return false;
		}
		
	}
	
	/**
	 * Returns whether a key, index, node has a right child or not
	 * if v is the right child of node u, then p(v) = 2p(u)+1
	 * @param index
	 * @return
	 */
	public boolean hasRight(int index){
		try {
			return (this.binArray.get((2*index)+1)!=null);
		} catch (IndexOutOfBoundsException e){
			return false;
		}
		
	}
	
	/**
	 * returns whether a particular node/key/index is Internal or not
	 * that is whether the node has any children
	 * @param index
	 * @return  true if it has any children
	 */
	public boolean isInternal(int index){
		return (this.hasLeft(index));
	}
	
	/**
	 * Returns whether a particular node/key/index is External or not
	 * that is whether the node has "no children"
	 * @param index
	 * @return
	 */
	public boolean isExternal(int index){
		return !(this.isInternal(index));
	}
	

	/**
	 * This will add an element at the next empty slot, which means always
	 * at the end of the tree.  
	 * @param t
	 */
	public void add(T t){
		//we must add at 1 above the count. If count is 0, we have no root, so 
		//we will simply add the root at the 1st index where we want the root to be.
		this.binArray.add(this.count+1, t); 
		this.count++;
	}
	
	/**
	 * This will remove an element at the given index.  If the given node
	 * is external, it will simply return the element at that Node and delete it
	 * If the node is internal, it will remove the node and replace it with the 
	 * LAST node that exists in the tree
	 * if the node we want to remove is external but on a lower height than max
	 * height of the tree, it will return the element and replace it with
	 * the LAST node of the tree to keep it a proper binary tree
	 * @param i
	 * @return
	 * @throws NoSuchElementException if i has is null or 0
	 */
	public T remove(int i) throws NoSuchElementException{
		
		//throw an exception if there is no element at that index.
		if(i==0 || this.binArray.get(i)==null)
			throw new NoSuchElementException();
		
		//we will remove the node that we want to return but we can't 
		//use our ArrayList remove() as that adjusts the array for us
		//so let's GET that object and adjust tree manually 
		T toReturn = (T)this.binArray.get(i);
		
		//if the node is external, then return what we removed and we are done.  
		if(this.isExternal(i)){
			
			//if we remove a node that is less than our count, we are 
			//removing at a lower height than our final node, it's still external
			//but we may have more nodes at other level
			if(i<count){ //if i is less than count, means we are at a diff level
				this.replace(i, count);
				//we will remove the node at count to adjust our arrayList size
				this.binArray.remove(count);
			} else { //it's truly an external node
				this.binArray.remove(i);
			}
			this.count--; //reduce our node count
			return toReturn; //return our node that has been removed
			
		} else { //then the node is internal of course
			while(this.isInternal(i)){ //as long as we have an internal node
				//then it must have a leftchild
				this.replace(i, (i*2)); //replace it with that child
				i = i*2; //now go see if the left child is internal as well
			}
			
			//when we come out of the loop, we are at our final external
			//node that simply needs to swap with the LAST NODE which is at index count
			this.replace(i,  count);
			//delete this last node so that our Arraylist size is adjusted
			this.binArray.remove(count); 
			this.count--;  //reduce our node count
			return toReturn; //return the node we removed
		}
	}
	
	/**
	 * This method replaces the element at index i with the element at index j
	 * but leaves the element at index j intact. 
	 * @param i
	 * @param j
	 * @throws NoSuchElementException if no element exists at j
	 */
	public void replace(int i, int j) throws NoSuchElementException{
		try{
			if(this.binArray.get(j) == null)
				throw new NoSuchElementException();
		} catch (IndexOutOfBoundsException e){
			throw new NoSuchElementException();
		}
		
		//set the value at index i with the value at index j
		//set returns the original value at index i
		//but we don't care for it, so we will simply ignore it. 
		this.binArray.set(i, (T)binArray.get(j));
		
	}
	
	/**
	 * This method will swap the values at two indices i and j with each other.
	 * @param i
	 * @param j
	 * @throws NoSuchElementException
	 */
	public void swap(int i, int j) throws NoSuchElementException{
		T u;
		T v;
		
		try{
			u = this.binArray.get(i);
			v = this.binArray.get(j);
		} catch (IndexOutOfBoundsException e){
			throw new NoSuchElementException();
		}
		
		
		if(u == null || v == null)
			throw new NoSuchElementException();
		else{
			this.binArray.set(i, v);
			this.binArray.set(j, u);
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[Binary Tree] root = " + this.element(1) + "\n" + this.binArray + "\nTotal Tree Nodes = "
				+ this.count ;
	}
	
	/**
	 * Prints a binary tree. Method adapted from: 
	 * http://stackoverflow.com/questions/4965335/how-to-print-binary-tree-diagram
	 * @param root
	 * @param level
	 */
	public void printBinaryTree(int index, int level){
		if (index==0)
			index = 1; //set the index to 1, our root is at 1
		
		try{
			if(this.binArray.get(index) == null)
		         return;
		} catch (Exception e){
			return;
		}
	    
	    
	    printBinaryTree((2*index)+1, level+1);
	    
	    if(level!=0){
	        for(int i=0;i<level-1;i++)
	            System.out.print("|\t");
	            System.out.println("|-------"+ this.element(index));
	    }
	    else
	        System.out.println(this.element(index));
	    
	    printBinaryTree((2*index), level+1);
	}

	/**
	 * Returns an iterator to go over all elements starting from the root
	 * 
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Iterator<T> iterator() {
		return new NodeIterator();
	}
	
	/**
	 * Returns an iterator for all children of the index provided
	 * @param i
	 * @return
	 */
	public Iterator<T> children(int i) {
		//TODO: fix this to return only the children of the given node.  
		return new NodeIterator(i);
	}
	
	/**
	 * This is our internal iterator class
	 * @author Amit
	 *
	 */
	private class NodeIterator implements Iterator<T>
	{	
		private int i;
		public NodeIterator(){
			this.i = 1;
		}
		
		public NodeIterator(int i){
			this.i = i;
		}
		
		public boolean hasNext(){
			return i < count && binArray.get(i) != null;
		}
		
		public void remove(){
			/*not supported*/
		}
		
		public T next(){
			return (T) binArray.get(i++);
		}
	}
	
	 //iteratorInOrder() returns an inorder iterator of the tree
	 //iteratorPreOrder() returns a preorder iterator of the tree
	 //iteratorPostOrder() returns a postorder iterator of the tree
	 //iteratorLevelOrder() returns a Level Order iterator of the tree
	
}