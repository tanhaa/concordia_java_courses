package java248.tutorial9;

public class PointTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Point p1 = new Point(0,0);
		Point p2 = new Point(2,3);
		
		System.out.println(p1);
		System.out.println(p2);
		
		p2 = p2.reverse();
		
		p1 = (p2.reverse());
		
		p1.moveBy(10);
		
		if (p1.equals(p2))
		{	System.out.println("Same");}
		else{
		System.out.println("Different");
		}
		
		System.out.println(p2);
		System.out.println(p1);
	}

}
