package java248.tutorial9;

public class Point {

	private int x; //x-coordinate
	private int y; //y-coordinate
	
	public Point(int x1, int y1){
		x = x1;
		y = y1;
		
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	public void setX(int x1){
		x = x1;
	}
	public void setY(int y1){
		y = y1;
	}
	public boolean equals(Point n){
		if (this.x == n.x && this.y == n.y)
			return true;
		return false;
	}
	public Point reverse(){
		Point p = new Point(y,x);
		return p;
	}
	public void moveBy(int v){
		x = x + v;
		y = y + v;
	}
	public String toString(){
		return "Coordinates of Point are (" + x + "," + y + ")."; 
	}
}
