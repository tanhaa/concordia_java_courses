package java248.tutorial9;

public class Temperature {

//instance variables
	private float temp = 0; //temperature vallule
	private char scale = ' '; //character for scale, c or f for celsius or farhenheit
	
//no-arg constructor
	public Temperature(){
		temp = 0;
		scale = 'C';
		
	}
	public Temperature(float t, char s){
		temp = t;
		
		//convert s to uppercase using the Character wrapper class
		s = Character.toUpperCase(s);
		//check if s is set to celsius or farhenheit
		if (s != 'C' && s != 'F')
			{s = 'C';}
		
		scale = s;
		
	}
	
	public float getTempInCelsius(){
		if (scale == 'C'){
			return temp;
		}else{
			return ((float)5/9)*(temp-32);
		}
		
	}
	
	public void setTemp(float t, char s){
		temp = t;
		
		s = Character.toUpperCase(s);
		//check if s is set to celsius or farhenheit
		if (s != 'C' || s != 'F')
			{s = 'C';}
		
		scale = s;
	}
	
	public boolean isHotter(Temperature x){
		
		return (this.getTempInCelsius() >= x.getTempInCelsius());
		
	}
	
	public String toString(){
		return "The temperature is " + temp + " " + scale + ".";
		
	}
	public float add(Temperature x){
		return this.getTempInCelsius() + x.getTempInCelsius();
	}
}