package java248.tutorial4;
import java.util.Scanner;

public class Question7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myKeyboard = new Scanner(System.in);
		myKeyboard.close();
		
		int x = 0;
		int y = 10;
		int z = 100;
		String msg;
		
		System.out.print("Enter a string: ");
		msg = myKeyboard.next();
		
		switch(msg.charAt(0))
		{
		case 'a':
		case 'b':
			System.out.println("case 1");
			x = (msg.equals("abc") ? (5 + y++) : (--y + z--));
			break;
		case 'c':
			System.out.println("case 2");
			y /= 5; //divide y by 5 and store the value back in y
		default:
			System.out.println("default");
				
		}
		System.out.println(x + " " + y + " " + z);
	}

}
