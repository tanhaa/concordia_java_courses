package java248.tutorial4;
import java.util.Scanner;
public class Question8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		short age;
		double rebate = 0;
		boolean isAStudent;
		String answer;
		int workExperience;
		
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Please enter your age and the number of years you have worked, separated by a space:  " );
		age = kb.nextShort();
		workExperience = kb.nextInt();
		
		System.out.print("Are you a student? (If Yes enter True, else enter False): ");
		isAStudent = kb.nextBoolean();
		//answer = kb.next();
		//if (answer == "Yes"){  //why doesn't this work?  isAStudent should turn true if I enter Yes
		//it doesn't work because of string comparison!  should use string isequal 
		//	isAStudent = true; 
		//} else {
		//	isAStudent = false;
		//}
		
		if (age < 10 || age > 70)
			
		{
			rebate = 20;
		} else if (age < 20 && age >=10 && isAStudent && workExperience >4) 
		{
			rebate = 15; 
		}
		System.out.println("Your rebate is: " + rebate + ".");
	}
}

