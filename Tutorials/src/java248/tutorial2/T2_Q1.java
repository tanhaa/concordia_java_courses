package java248.tutorial2;

public class T2_Q1 {

	/**
	 * @param args
	 */
	public static final String sentence = "My school name is Concordia";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int position = sentence.indexOf("school");
		String firstPart = sentence.substring(0,position);
		String afterChange = sentence.substring(position+6);
		String newString = firstPart + "university" + afterChange;
		System.out.println("The line of the text to be changed is: ");
		System.out.println(sentence);
		System.out.println("I have rephrased the line to read: ");
		System.out.println(newString);
		
	}

}
