package java248.tutorial2;
import java.util.Scanner;
public class T2_Q4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner kb = new Scanner(System.in);
		double length, width;
		System.out.print("Please enter the length and width of the rectangle separated by space: ");
		length = kb.nextDouble();
		width = kb.nextDouble();
		kb.close();
		Double area = length * width;
		System.out.println("The area of the rectangle is: " + area);
	}

}
