package java248.tutorial2;
import java.util.Scanner;
public class T2_Q2 {

	/**
	 * @param args
	 */
	public static final double TAX_RATE = 0.20;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		int salary;
		double tax, net_salary;
		System.out.print("Enter the salary: ");
		salary = keyboard.nextInt();
		keyboard.close();
		tax = salary * TAX_RATE;
		net_salary = salary-tax;
		System.out.println("Salary before Tax: " + salary);
		System.out.println("Tax: " + tax);
		System.out.println("Net salary after taxes: " + net_salary);
	}

}
