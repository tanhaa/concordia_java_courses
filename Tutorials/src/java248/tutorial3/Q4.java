package java248.tutorial3;
import java.util.Scanner;
public class Q4 {

	/**
	 * @param args
	 */
	public static final double EURO_TO_USD_RATE = 1.3044;
	public static final double USD_TO_POUND_RATE = 0.62;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter the amount in Euros that you want to convert: ");
		double dEuros = kb.nextDouble();
		double dEuroToUSD = dEuros * EURO_TO_USD_RATE;
		double dEuroToPound = (dEuros * EURO_TO_USD_RATE)*USD_TO_POUND_RATE;
		System.out.printf("Euros to USD: $%12.3f", dEuroToUSD);
		System.out.println("");
		System.out.printf("Euros to Pound: %12.3f", dEuroToPound);

	}

}
