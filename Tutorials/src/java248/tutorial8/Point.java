package java248.tutorial8;

public class Point {

//instance variables
	private int x;  //x-coordinate 
	private int y;  //y-coordinate
	
public Point(int x1, int y1)
{
	this.x = x1;
	this.y = y1;
	
}

public int getX()
{
	return this.x;
}

public int getY()
{
	return this.y;
}

public void setX(int x1)
{
	this.x = x1;
}

public void setY(int y1)
{
	this.y = y1;
}

public boolean equals(Point p)
{
	if (p == null)
			return false;
	
	if (this.x == p.x && this.y == p.y)
		return true;
	return false;		
}

public Point reverse()
{

	return new Point(y,x);
	
}
}