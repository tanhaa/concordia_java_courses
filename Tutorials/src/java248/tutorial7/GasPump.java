package java248.tutorial7;
public class GasPump {

	private int supply;
	private double price;
	
	public GasPump(){
		this.supply = 50;
		this.price = 1.5;
	}
	public GasPump(int supply, double price)
	{
		this.supply = supply;
		this.price = price;
	}
	
	public void setPrice(double price){
		this.price = price;
	}
	public double getPrice(){
		return this.price;
	}
	public void addSupply(int fuelAmount){
		int newCapacity = this.supply + fuelAmount;
		
		if (newCapacity > 5000)
		{
		this.supply = 5000;
		System.out.println("Some of your fuel went to waste as you exceeded the" + 
				" maximum capacity by " + (newCapacity - this.supply));
		}
		else
		{
			this.supply = newCapacity;
		}
	}
	public int getSupply()
	{
		return this.supply;
	}
	
	//sell fuel
	public double sellFuel(int amount)
	{
		if (amount > this.supply)
		{	
			amount = this.supply;
			this.supply = 0;
		}
		else
		{
			this.supply = this.supply-amount;
		}
		double cost = amount * this.price;
		return cost;
			
	}
	//default methods
	public String toString()
	{
		return ("This gas station has " + this.supply + " liters and sells at " + this.price + " per liter");  
	}
	public boolean equals(GasPump otherStation)
	{
		if ((this.supply == otherStation.supply) && (this.price == otherStation.price))
			return true;
		return false;
	
	}
	}
