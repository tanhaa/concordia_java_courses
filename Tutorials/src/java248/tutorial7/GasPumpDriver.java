package java248.tutorial7;
import java.util.Scanner;

public class GasPumpDriver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		GasPump shell = new GasPump(3000, 0.785);
		
		Scanner kb = new Scanner(System.in);
		System.out.print("How much gas do you want? ");
		int amountRequested = kb.nextInt();
		kb.close();
		
		double cost = shell.sellFuel(amountRequested);
		System.out.printf("Your cost is: $%2.2f %n", cost);
		shell.addSupply(500);
		int remainingSupply = shell.getSupply();
		System.out.println("Remaining supply is: " +remainingSupply);
		
		
	}

}
