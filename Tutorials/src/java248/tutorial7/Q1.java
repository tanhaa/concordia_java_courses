package java248.tutorial7;

public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n=0;
		while (!((n%3 == 1) && (n%5 == 2) && (n%7 == 3)))
		{ n++;}
		System.out.println(" The smallest positive integer whose remainder when divided by 3 is 1, by 5 is 2 and by 7 is 3 is: " + n);
				

	}

}
