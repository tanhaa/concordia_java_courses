package java248.tutorial6;
import java.util.Scanner;
public class T6Q4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter an integer: ");
		int useri = kb.nextInt();
		kb.close();
		int i, j, k;
		if (useri >= 2){
		
			
				for (i=0; i<=useri/2 + useri%2; i++)
				{
					for (j=1; j<=i; j++)
						System.out.print(" ");
					for (k=1; k<=useri-i*2; k++)
						System.out.print("*");
					System.out.println();
		 
				}
				for (i=1; i<=useri/2 + useri%2; i++)
				{
					for (j=1; j<=useri-i*2; j++)
						System.out.print(" ");
					for (k=1; k<=i; k++)
						System.out.print("*");
					System.out.println();
				}
		
		}
		else
		{
			System.out.println("Error");
			System.exit(0);
		}

	}

}
