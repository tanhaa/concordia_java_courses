package java248.tutorial6;

public class T6Q3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//External loop for the number of rows
		//Internal loop for the number of columns
		//for example Q3-a = 5 rows, 5 columns

		//a
	for (int i=1; i<=5; i++){
		for (int j = 1; j <=i; j++){
			System.out.print("*");
			}
		System.out.println();
	}
	
		//b
	System.out.println("\n\n********************PART B*****************\n\n");
	int i, j, k;
	for (i=1; i<=3; i++)
	{
		for (j = 0; j < 3-i; j++)
			System.out.print(" ");
		for (k = 1; k <= 5-2*j; k++)
			System.out.print("*");
		
			System.out.println();
		
		}
	
	for (i=1; i<=2; i++)
	{
		for (j=1; j <= i; j++)
			System.out.print(" ");
		for (j=1; j<= 5-2*i; j++)
			System.out.print("*");
		System.out.print('\n');
	}
		}
	//end
	}

	