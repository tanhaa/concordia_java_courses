package java249.tutorial8;

public class OrderedPairDriver {

	public static void main (String args[]){
		OrderedPair<Integer,String> myPair = new OrderedPair<Integer,String>();
		
//		myPair.setFirst(new Shape());
		myPair.setFirst(new Integer(1));
		myPair.setSecond(new String("So much depends on "));
		System.out.println(myPair);
	}
	
	public OrderedPairDriver() {
		// TODO Auto-generated constructor stub
	}

}

	