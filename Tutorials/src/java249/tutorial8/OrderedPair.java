package java249.tutorial8;

/**
 * Generic ordered pair class
 * @author Sameer
 *
 */
public class OrderedPair<T,S> {

	private T first;
	private S second;
	
	public OrderedPair(){
		first = null;
		second = null;
	}
	
	public OrderedPair(T firstItem, S secondItem){
		this.first = firstItem;
		this.second = secondItem;
	}
	
	public void setFirst(T val) { this.first = val;}
	public void setSecond(S val) {this.second = val;}
	
	public T getFirst() { return first;}
	public S getSecond() { return second;}
	
	public void trySomething(){
		//first.compareTo(second);
	}
	
	public boolean equals(Object otherObject){
		if (otherObject == null)
			return false;
		else if (getClass() != otherObject.getClass())
			return false;
		else
		{
			OrderedPair<T,S> otherPair = (OrderedPair<T,S>)otherObject;
			return (first.equals(otherPair.first)&& second.equals(otherPair.second));
		}
	}
	
	public String toString(){
		return ("This orderedPar contains a " + this.first.getClass() + 
				" and a " + this.second.getClass() + " with a value of "
				+ this.first + " and " + this.second); 
	}
	
	public int compareTo(Object a){
		return -1;
	}

}
