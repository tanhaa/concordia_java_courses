package java249.tutorial8;

abstract class Shape {
	
	private int height,width;

	public Shape() {
		this.height = 0;
		this.width = 0;
	}
	public Shape(int h, int w){
		this.height = h;
		this.width = w;
	}
	
	public void setHeight(int i) {
		this.height = i;
	}
	
	public void setWidth(int i){
		this.width = i;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public int compareTo(Object a){
		return -1;
	}

}
