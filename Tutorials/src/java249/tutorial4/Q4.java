package java249.tutorial4;
import java.util.Scanner;


//we are going to extend the Exception class 
class DivisionByZero extends Exception{
	public DivisionByZero(){
		super("Divide by Zero");
	}
	public DivisionByZero(String msg){
		super(msg);
	}
}

public class Q4 {

	/**
	 * 
	 * @param num	the numerator
	 * @param denum the denominator
	 * @return  int the result of a division
	 * @throws DivisionByZero  arithmeticexception
	 */
	public static int safeDivide(int num, int denum) throws DivisionByZero {
		if (denum == 0) {throw new DivisionByZero();} //create a new exception and throw it
		return num/denum;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner kb = new Scanner(System.in);
		
		int errorCount =0 ;
		while (errorCount < 2){
			try {
				
				System.out.println("Enter an int: ");
				int num = kb.nextInt();
				System.out.println("Enter another int: ");
				int denum = kb.nextInt();
				int result = safeDivide(num,denum);
				System.out.println("Result = " + result);
				
			} catch (DivisionByZero e){
				errorCount += 1;
				System.err.println(e.getMessage());
				
			} catch (java.util.InputMismatchException e){
				System.out.println(e.getMessage());
				kb.nextLine();
			} catch (Exception e){
				System.out.println(e.getMessage());
			}
			
		if (errorCount == 2){System.out.println("You don't learn! Goodbye.");}
			
	}
		
		kb.close();
	}
	}
	
	
