package java249.tutorial4;

class AException extends Exception{
public AException(String msg){
super(msg);
}
}
class BException extends Exception{
public BException(String msg){
super(msg);
}
}
public class Except {
static void aMethod() throws AException, BException {
throw(new BException("Failed again!"));
}
static void bMethod() throws AException, BException {
try { aMethod(); }
catch (BException be) { }
System.out.println("I made it");
}
public static void main(String[] args) throws BException {
try { bMethod(); }
catch (AException ae) { System.out.println(ae.getMessage()); }
finally{ System.out.println("In finally"); }
System.out.println("After finally");
}
}