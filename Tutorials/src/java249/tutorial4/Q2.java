package java249.tutorial4;

public class Q2 {

	
	public static String str;
	
	public static void main(String[] args) {
		Test4();
		
	}
	
	//IndexOutOfboundsException
	public static void Test1() {
	int[] myArray = new int[5];
	for(int i = 0; i <= myArray.length; i++) {
	myArray[i] = i;
	}
	}
	
	//ArithmeticException 
	public static void Test2() {
	double i = 1/0;
	i += i;
	}
	
	//NullPointerException
	public static void Test3() {
	System.out.println(str.length());
	}
	
	//classcast exception - integer can't be cast to string
	public static void Test4() {
	Object o = new Integer(42);
	String str = (String)o;
	System.out.println(str.length());
	}
	
	//NumberFormatException
	public static void Test5() { System.out.println(Integer.parseInt("12o")); }
	
	//returns NaN exception
	public static void Test6() { System.out.println(Math.log(-0.123)); }

}

	
