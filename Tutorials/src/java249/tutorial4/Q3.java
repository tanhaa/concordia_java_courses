package java249.tutorial4;

public class Q3 {

	class Base {
		protected void m(int i) {
		// some code
		}
		}
	class Child extends Base {
		
		//public void m(int i) throws Exception{}
		//lower viability and should not throw an exception, since the super class does not throw one
		
		public void m(char c) throws Exception{}
		//can be inserted overloading  not overriding, so can throw exception!
		
		//public void m(int i){}
		//can be added, t has higher viability and correctly overridden
		
		//protected void m(int i) throws Exception{}
		//can not be insterted, method overriding and the super class does not through exception
		
		// Method Here
		}

}
