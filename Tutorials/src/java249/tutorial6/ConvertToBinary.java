package java249.tutorial6;



//Tutorial 6 ,Question 4
/**
 * @author MaryamZ
 *
 */
public class ConvertToBinary
{

	public static void main(String args[])
	{
		System.out.println(convertToBinary(8));
	}



	public static String convertToBinary(int n) {
		 if (n < 2) 
		    return ("" + n);
		else
		  return (convertToBinary(n / 2) + (n % 2));
		        
		    }


}