package java249.tutorial6;

public class Tutorial6 {

	public static int mod (int m, int n){
		//recursive function that finds the remainder when you divide two numbers
		if (m < n) //base case
			return m;
		return mod(m-n,n); //else just call the function again
	}
	
	public static String reverseString(String s){
		//find the last letter
		if (s.length() == 0)
		{	return s;
		}
		else
		{
		return reverseString(s.substring(1)) + s.charAt(0);
		}
	}
	
	public static void main(String args[])
	{
		int x = mod(5,2);
		System.out.println(x);
		
		String s =reverseString("IamHappy");
		System.out.println(s);
	}

}
