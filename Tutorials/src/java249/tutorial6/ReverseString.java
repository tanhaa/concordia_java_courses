package java249.tutorial6;

//Tutorial 6 ,Question 2
/**
 * @author MaryamZ
 *
 */
public class ReverseString
{

	public static void main(String args[])
	{
		System.out.println(rev("Today Is A Good Day"));
	}



	public static String rev( String str ){
        if ( str.length() == 0 )
            return str;
        else
            return rev( str.substring(1) ) + str.charAt(0);
    }

}