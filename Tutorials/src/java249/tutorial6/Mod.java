package java249.tutorial6;

//Tutorial 6 ,Question 1
/**
 * @author MaryamZ
 *
 */
public class Mod
{

	public static void main(String args[])
	{
		System.out.println(mod(10,3));
	}



	public static int mod( int N, int M ){
		if ( N < M )
			return N;
		else
			return mod( N-M, M );
	}
}