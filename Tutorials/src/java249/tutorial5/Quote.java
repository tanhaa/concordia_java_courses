package java249.tutorial5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class FileNotFound extends FileNotFoundException{
	public FileNotFound(String msg){
		super(msg);
	}
}

public class Quote {
	
	public void readQuote() throws FileNotFoundException {
		Scanner InputStream;
		
		File quoteText = new File("Quote.txt");
		if(!quoteText.exists()) throw (new FileNotFound("File Doesnt exist in this directory!"));
		
		InputStream = new Scanner(quoteText);
		InputStream.useDelimiter("-");
	    
		String t = InputStream.next();	
	    String a = InputStream.nextLine();    
	    int b = InputStream.nextInt();
	    
	    System.out.println(t+a+","+b);	
	}
	
	public static void main(String [] args) {
		
		Quote newQuote = new Quote();
		
		try {
			newQuote.readQuote();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			System.out.println("No Quotes For the Day :( ");
			System.exit(0);
		}
     
	}

}
