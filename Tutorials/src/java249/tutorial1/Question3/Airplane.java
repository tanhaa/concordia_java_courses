package java249.tutorial1.Question3;
/*
 * Definition of class Airplane, Question 3
 */
public class Airplane {
	/*
	 * variables definition
	 */
	private int nbOfPassengers;
	private double weight;
	private int maxSpeed;

	// Constructor
	public Airplane(int num, double w, int ms) {
		this.nbOfPassengers = num;
		this.weight = w;
		this.maxSpeed = ms;
	}
    
	// getter and setter for nbOfPassengers
	public int getNbOfPassengers() {
		return this.nbOfPassengers;
	}
	public void setNbOfPassengers(int num) {
		this.nbOfPassengers = num;
	}

	// getter and setter for weight
	public double getWeight() {
		return this.weight;
	}
	public void setWeight(double w) {
		this.weight = w;
	}
     
	// getter and setter for maxSpeed
	public int getMaxSpeed() {
		return this.maxSpeed;
	}
	public void setMaxSpeed(int ms) {
		this.maxSpeed = ms;
	}
	
	// toString method
	public String toString(){
		return "\"" + this.nbOfPassengers + ", " + this.weight + ", " + this.maxSpeed + "\"";
	}
}
