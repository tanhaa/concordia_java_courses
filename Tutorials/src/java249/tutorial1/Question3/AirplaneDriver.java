package java249.tutorial1.Question3;
/*
 * Class to define methods getAverageWeight and findFastestAirplane and test the methods
 */
public class AirplaneDriver {

	public static void main(String[] args) {
		/*
		 * Testing the method from class Airplane, Question 3
		 */
		// create the array of objects of class Airplane 
		Airplane [] planes = new Airplane[3];
		planes[0] = new Airplane(100, 1700.0, 250); // first element of array is an object of class Airplane
		planes[1] = new Airplane(150, 2000.0, 245); // second element of array is an object of class Airplane
		planes[2] = new Airplane(125, 1900.0, 230); // third element of array is an object of class Airplane

		System.out.println("\n**************Test Question 3**************");
		double avgWeight = getAverageWeight(planes); // get the average weight of planes
		System.out.println("\nThe average weight of planes: " + avgWeight);
		
		Airplane fastestPlane = findFasterAirplane(planes); // get the object of class Airplane which has the fastest speed
		System.out.println("\nThe fastest plane is: " + fastestPlane);
		
	}

	/*
	 * method getAverageWeight calculates the average weight of all the planes 
	 * provided in an array of planes
	 */
	public static double getAverageWeight(Airplane[] a) {
	    // your code goes here
		double avgWeight = 0.0;
		for (int i = 0; i<a.length; i++){
			avgWeight += a[i].getWeight() ;
		}
		
		avgWeight = avgWeight/a.length;
		
	    return avgWeight;
	}

	/*
	 * method findFasterAirplane find the plane object which has greatest speed among the other planes 
	 * provided in an array of planes
	 */
	public static Airplane findFasterAirplane(Airplane[] a) {
	    // your code goes here
		int fastestIndex=0;
		for (int i = 0; i < a.length; i++){
			if(a[i].getMaxSpeed() > a[fastestIndex].getMaxSpeed())
				fastestIndex = i;
		}
		return a[fastestIndex];
	}

}
