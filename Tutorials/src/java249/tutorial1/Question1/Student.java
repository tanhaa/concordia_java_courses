package java249.tutorial1.Question1;

public class Student {

	/**
	 * @param args
	 */
	double quiz1, quiz2, quiz3, midterm, finalExam, overallScore;
	char finalGrade;
	
	public Student(){
		quiz1 = 0;
		quiz2 = 0;
		quiz3 = 0;
		midterm = 0;
		finalExam = 0;
		overallScore = 0;
		finalGrade = 'Z';
	}
	
	public void setQuiz1(double n){
		this.quiz1 = n;
	}
	public void setQuiz2(double n){
		this.quiz2 = n;
	}
	public void setQuiz3(double n){
		this.quiz3 = n;
	}
	public void setMidterm(double n){
		this.midterm = n;
	}
	public void setFinalExam(double n){
		this.finalExam = n;
	}
	public void calculateOverallScore(){
		double quizTotal = this.quiz1 + this.quiz2 + this.quiz3;
		double score = (((quizTotal*100)/60)*0.15)+(((this.midterm*100)/50)*0.35)+(((this.finalExam*100)/100)*0.5);
		this.overallScore = score;
		
	}
	
	public void finalLetterGrade(){
		
		if(this.overallScore >= 90)
			finalGrade='A';
		else if(this.overallScore < 90 && this.overallScore >= 80)
			finalGrade = 'B';
		else if(this.overallScore < 80 && this.overallScore >= 70)
			finalGrade = 'C';
		else if(this.overallScore < 70 && this.overallScore >= 60)
			finalGrade = 'D';
		else
			finalGrade = 'F';
			}
	
	public String toString(){
		return ("Student's final grade is: " + finalGrade + " (" + overallScore + ")");
	}
	}

