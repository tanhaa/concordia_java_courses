// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #2
// Due Date: 1st of March 2013
//--------------------------------------------------------------------
package warships;

import java.text.NumberFormat;



/**
* This class defines the base class that will be used for all specific type of battleships.
* A warship is built primarily for combat and is characterized by several features such as
* it's cost, it's weight, speed and to which country it belongs.  
*
*
* @author Amit Malhotra
*
*/
public class Warship implements Cloneable {

	//attributes specific to all warships
	private double price; //Price of the Warship, expressed in Million of dollars
	private float weight; //Weight of the warship expressed in tons
	private int speed; //Speed of the Warship expressed in knots
	private String countryOfOrigin; //Country where the Warship was created
	
	/**
	 * Default no argument constructor which calls the parametrized constructor with preset values for 
	 * Price, Weight, Speed and Country of Origin
	 */
	public Warship() {
		
		//making a call to the parametrized constructor
		this(352, 50000, 32, "Canada");
		/*
		 * Equivalent to saying: 
		 * this.price = 352;
		 * this.weight = 50000;
		 * this.speed = 32;
		 * this.countryOfOrigin = "Canada";
		 */
	}

	/**
	 * <p>Parametrized constructor for the Warship class.  Requires four parameters to be passed to 
	 * initialize all attributes of the object. These include the Price, the weight, speed and country
	 * where the warship was built. 
	 * </p>
	 * @param price		Of the type Double, indicating the Price in Million of Dollars of the Warship
	 * @param weight		of the type Float, indicating the weight in "tons" of the Warship
	 * @param speed		of the type Integer, indicating the speed in "knots" of the Warship
	 * @param country		of the type String, indicating the country where the Warship was built
	 */
	public Warship(double price, float weight, int speed, String country){
		this.price = price;
		this.weight = weight;
		this.speed = speed;
		this.countryOfOrigin = country;
	}

	//mutator methods of the class

	/**
	 * Method that allows one to modify the price of the warship.
	 * 
	 * @param price		of the type Double, indicating the new price for the warship in Dollars.
	 */
	public void setPrice(double price){
		this.price = price;
	}

	/**
	 * Method that allows one to modify the weight of the warship. 
	 * 
	 * @param weight		of the type Float, indicating the new weight for the warship in "Tons"
	 */
	public void setWeight(float weight){
		this.weight = weight;
	}
	
	
	/**
	 * Method that allows one to modify the speed of the warship.
	 * 
	 * @param speed		of the type Integer, indicating the new speed for the warship in "knots"
	 */
	public void setSpeed(int speed){
		this.speed = speed;
	}

	/**
	 * Method that allows one to modify the country of origin for the warship.
	 * 
	 * @param country		of the type String, indicating the new country for the warship.
	 */
	public void setCountry(String country){
		this.countryOfOrigin = country;
	}

	//accessor methods of the class 

	/**
	 * This method allows one to access the price of the warship. 
	 * 
	 * @return		Price in "dollars". Type Double.
	 */
	public double getPrice(){
		return this.price;
	}

	/**
	 * This method allows one to access the weight of the warship.
	 * 
	 * @return		Weight in "tons".  Type Float.
	 */
	public float getWeight(){
		return this.weight;
	}

	/**
	 * This method allows one to access the speed of the warship.
	 * 
	 * @return		Speed in "Knots".  Type Integer.
	 */
	public int getSpeed(){
		return this.speed;
	}
	
	/**
	 * This method allows one to access the country of origin of the warship.
	 * 
	 * @return		Country of Origin.  Type String.
	 */
	public String getCountry(){
		return this.countryOfOrigin;
	}
	
	/**
	 * <p>
	 * The equals method overrides the equals method defined in the Object class.  It requires 
	 * a parameter of the type Object to be passed to the method.  The method will:
	 * <li>
	 * <ul>Check if the other object exists</ul>
	 * <ul>Check if the other object was created as the same class</ul>
	 * <ul>Type cast the passed object is of the same type as the calling object</ul>
	 * <ul>Return true or false based on whether the two objects are equal or not</ul>
	 * </p>
	 */
	public boolean equals(Object otherObject) {  //this will "OVERRIDE" the equals,, not overload
		if (this == otherObject)
			return true; //check if the object being passed is the one calling the method
		
		if(otherObject == null)  //check if otherObject exists
			return false;
		else if (this.getClass() != otherObject.getClass())  //check if otherObject was created as the same class as our object
			return false;
		else {
			Warship otherShip = (Warship)otherObject; //type cast passed Object as Warship
			return (this.price == otherShip.price && this.weight == otherShip.weight 
					&& this.speed == otherShip.speed 
					&& this.countryOfOrigin.equals(otherShip.countryOfOrigin)); //return true if everything matches	
		}
		
	}
	
	/**
	 * This method overrides the toString method defined in the Object class.  It will return a 
	 * String that describes the attributes of the object such as it's cost, weight, speed and where
	 * it was built. 
	 */
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(9); //removing the package name from front
		
		String w = NumberFormat.getIntegerInstance().format(this.weight); //format weight correctly
		String p = NumberFormat.getIntegerInstance().format(this.price); //format price correctly
		
		return ("This " + s + " costs $" + p + "M and weighs " + w + 
				" tons and travels at a speed of " + this.speed + " knots. It belongs to " 
				+ this.countryOfOrigin + ".");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	
}
