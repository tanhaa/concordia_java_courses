// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #2
// Due Date: 1st of March 2013
//--------------------------------------------------------------------
package Destroyers;
import java.text.NumberFormat;

import warships.Warship;

/**
 * This class defines a destroyer, a fast and maneuverable warship, built to escort larger vessels
 * and defend them against attackers. It extends the Warship class.
 *
 */
public class Destroyer extends Warship {

	/**
	 * The enumerated type gives us the choice to classify our destroyer's attack type
	 */
    public static enum AttackType{ //defined as public to make it accessible/usable outside the package
		MULTIROLE, ANTISUBMARINE, ANTIAIRCRAFT
		}; 

	//Attributes of the Destroyer class
	private AttackType typeOfAttack; //the type of attack it has
	private int nbGuns, nbMissiles;  //the number of guns and missiles it can have
	
	/**
	 * Default constructor for the Destroyer class. Makes a call to the super constructor defined in 
	 * the Warship class.
	 */
	public Destroyer() {
		super(); //call to super constructor defined in warship class
		this.typeOfAttack = AttackType.MULTIROLE;
		this.nbGuns = 5;
		this.nbMissiles = 5;
	}
	
	/**
	 * <p>Parametrized constructor for the Destroyer class.  Makes a call to the super constructor
	 * defined in the Warship class.
	 * </p>
	 * @param price		Of the type Double, indicating the Price in Million of Dollars of the Destroyer
	 * @param weight		of the type Float, indicating the weight in "tons" of the Destroyer
	 * @param speed		of the type Integer, indicating the speed in "knots" of the Destroyer
	 * @param country		of the type String, indicating the country where the Destroyer was built
	 * @param attack  of the type AttackType, an enum type describing the type of attack the ship has
	 * @param guns		of the type Integer, indicating the number of guns the ship has
	 * @param missiles		of the type Integer, indicating the number of missiles the ship carries
	 */
	public Destroyer(double price, float weight, int speed, String country, AttackType attack, int guns, int missiles) {
		super(price, weight, speed, country); //call to the super constructor defined in Warship class
		this.typeOfAttack = attack;
		this.nbGuns = guns;
		this.nbMissiles = missiles;
	}
	
	//accessor methods
	
	/**
	 * This method allows one to access the Attack type that the destroyer uses
	 * 
	 * @return typeOfAttack (of the enum type AttackType)
	 */
	public AttackType getTypeOfAttack(){
		return this.typeOfAttack;
	}
	
	/**
	 * This method allows one to access the number of Guns the destroyer houses
	 * @return	number of guns (type integer)
	 */
	public int getNbOfGuns(){
		return this.nbGuns;
	}
	
	/**
	 * This method allows one to access the number of missiles the destroyer houses
	 * @return Number of missiles (type integer)
	 */
	public int getNbOfMissiles(){
		return this.nbMissiles;
	}
	
	//mutator methods
	/**
	 * This method allows one to modify the type of attack the destroyer uses
	 * @param attack of the type AttackType, an enum type
	 */
	public void setTypeOfAttack(AttackType attack){
		this.typeOfAttack = attack;
	}
	
	/**
	 * This method allows one to modify the number of Guns the destroyer uses
	 * @param guns of the type integer
	 */
	public void setNbOfGuns(int guns){
		this.nbGuns = guns;
	}
	
	/**
	 * This method allows one to modify the number of missiles the destroyer uses.
	 * @param missiles of the type integer
	 */
	public void setNbOfMissiles(int missiles){
		this.nbMissiles = missiles;
	}

	/**
	 * This method outputs Type of Attack, Guns and Missiles in addition to the Warship.toString()
	 */
	@Override
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(11); //removing the package name from front
		
		String w = NumberFormat.getIntegerInstance().format(this.getWeight()); //format weight correctly
		String p = NumberFormat.getIntegerInstance().format(this.getPrice()); //format price correctly
		
		return ("This " + s + " costs $" + p + "M and weighs " + w + 
				" tons and travels at a speed of " + this.getSpeed() + " knots. It belongs to " 
				+ this.getCountry() + ". It uses a " + this.typeOfAttack + " type of Attack" +
						" and houses " + this.nbGuns+ " guns and " + 
				this.nbMissiles + " missiles." );
	}



	/**
	 * The Equals method overrides the equals method defined in the parent warship class. It checks 
	 * if the passing object exists, uses the parent's equal method to test the equality of the
	 * inherited attributes, tests the equality of the class and then verifies all of the objects
	 * own attributes for equality.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {  //check if the object calling is the same as the one we are comparing to
			return true;
		}
		if (!super.equals(obj)) {  //call to the equals method of parent to check if inherited attributes match
			return false;
		}
		if (getClass() != obj.getClass()) { //check if the types match
			return false;
		}
		Destroyer other = (Destroyer) obj; //type cast the object being passed as Destroyer
		//and verify if all attributes match
		if (nbGuns != other.nbGuns) {
			return false;
		}
		if (nbMissiles != other.nbMissiles) {
			return false;
		}
		if (typeOfAttack != other.typeOfAttack) {
			return false;
		}
		//haven't returned false yet?  Then they must match!
		return true;
	}
	
	
}
