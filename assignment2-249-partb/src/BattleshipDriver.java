


import submarines.Submarine;
import submarines.Submarine.PowerSource;
import warships.Warship;
import Destroyers.Destroyer;
import Destroyers.Destroyer.AttackType;
import carriers.AircraftCarrier;
import carriers.AmphibiousAssaultShip;
import carriers.SuperCarrier;


public class BattleshipDriver implements Cloneable {

	/**
	 * This main method in our driver will test out our Warship class and it's children
	 * @param args
	 */
	public static void main(String[] args) {
		
		// welcome message
		System.out.println("*********************************************");
		System.out.println("*        Welcome to the Warshp Driver!      *");
		System.out.println("*              Programmed by                *");
		System.out.println("*              Amit Malhotra                *");
		System.out.println("*********************************************");
		System.out.println();
	
		//a) create various objects from  the 6 classes 
		
		//create three warships
		Warship warship1 = new Warship(); //using default constructor
		Warship warship2 = new Warship(200, 20000, 15, "USA" ); //using parametrized constructor
		Warship warship3 = new Warship(200, 20000, 15, "USA" ); //using parametrized constructor
		//create three Submarines
		Submarine submarine1 = new Submarine(); //using default constructor
		Submarine submarine2 = new Submarine(250, 21000, 13, "India", PowerSource.NUCLEAR, 2, "Periscope"); //using parametrized constructor
		Submarine submarine3 = new Submarine(250, 21000, 13, "India", PowerSource.NUCLEAR, 2, "Periscope"); //using parametrized constructor
		//create two Destroyers
		Destroyer destroyer1 = new Destroyer(); //using default constructor
		Destroyer destroyer2 = new Destroyer(300, 22000, 14, "France", AttackType.ANTIAIRCRAFT, 5,5); //using parametrized constructor
		//create three AircraftCarriers
		AircraftCarrier aircraftCarrier1 = new AircraftCarrier(); //using default constructor
		AircraftCarrier aircraftCarrier2 = new AircraftCarrier(200, 19000,14, "USA", 4, 2 ); //using parametrized constructor
		AircraftCarrier aircraftCarrier3 = new AircraftCarrier(200, 19000,14, "USA", 4, 2 ); //using parametrized constructor
		//create two AmphibiousAssaultShips
		AmphibiousAssaultShip aaShip1 = new AmphibiousAssaultShip();//using default constructor
		AmphibiousAssaultShip aaShip2 = new AmphibiousAssaultShip(210,19500,14,"India",3,3,5); //using parametrized constructor
		//create three SuperCarriers
		SuperCarrier superCarrier1 = new SuperCarrier();//using default constructor
		SuperCarrier superCarrier2 = new SuperCarrier(220,18300,17,"France",2,2,5); //using parametrzed constructor
		SuperCarrier superCarrier3 = new SuperCarrier(220,18300,17,"France",2,2,5); //using parametrzed constructor
		
		System.out.println("A few types of Warships were created and will be displayed below using their respectively defined toString() methods.");
		//Print the objects using the toString method
		System.out.println("\nHere are the three Warships: ");
		System.out.println(warship1);
		System.out.println(warship2);
		System.out.println(warship3);
		System.out.println("\nHere are the three Submarines: ");
		System.out.println(submarine1);
		System.out.println(submarine2);
		System.out.println(submarine3);
		System.out.println("\nHere are the two Destroyers: ");
		System.out.println(destroyer1);
		System.out.println(destroyer2);
		System.out.println("\nHere are the three Aircraft Carriers: ");
		System.out.println(aircraftCarrier1);
		System.out.println(aircraftCarrier2);
		System.out.println(aircraftCarrier3);
		System.out.println("\nHere are the two Amphibious Assault Ships: ");
		System.out.println(aaShip1);
		System.out.println(aaShip2);
		System.out.println("\nHere are the three Super Carriers: ");
		System.out.println(superCarrier1);
		System.out.println(superCarrier2);
		System.out.println(superCarrier3);
		
		System.out.println("\nLet's test some of these objects for equality!");
		//testing objects for equality
		if(warship1.equals(warship2)){
			System.out.println("Warship1 is the same as Warship2");
		}else {
			System.out.println("Warship1 is not the same as Warship2");
		}
		if(warship2.equals(warship3)){
			System.out.println("Warship2 is the same as Warship3");
		}else {
			System.out.println("Warship2 is not the same as Warship3");
		}
		if(submarine1.equals(submarine3)){
			System.out.println("Submarine1 is the same as Submarine3");
		}else {
			System.out.println("Submarine1 is not the same as Submarine3");
		}
		if(submarine2.equals(submarine3)){
			System.out.println("Submarine2 is the same as Submarine3");
		}else {
			System.out.println("Submarine2 is not the same as Submarine3");
		}if(submarine1.equals(warship3)){
			System.out.println("Submarine1 is the same as Warship3");
		}else {
			System.out.println("Submarine1 is not the same as Warship3");
		}
		if(destroyer1.equals(destroyer2)){
			System.out.println("Destroyer1 is the same as Destroyer2");
		}else {
			System.out.println("Destroyer1 is not the same as Destroyer2");
		}
		if(aircraftCarrier1.equals(aircraftCarrier2)){
			System.out.println("Aircraft Carrier1 is the same as Aircraft Carrier2");
		}else {
			System.out.println("Aircraft Carrier1 is not the same as Aircraft Carrier2");
		}
		if(aaShip1.equals(aaShip2)){
			System.out.println("Amphibious Assault Ship1 is the same as Amphibious Assault Ship2");
		}else {
			System.out.println("Amphibious Assault Ship1 is not the same as Amphibious Assault Ship2");
		}
		
		//creating an array of 10 warships
		Warship[] navalForce = new Warship[10];
		navalForce[0] = warship1;
		navalForce[1] = warship2;
		navalForce[2] = warship3;
		navalForce[3] = destroyer1;
		navalForce[4] = submarine1;
		navalForce[5] = submarine2;
		navalForce[6] = submarine3;
		navalForce[7] = aircraftCarrier1;
		navalForce[8] = aaShip1;
		navalForce[9] = superCarrier2; 
		
		//filling the array with objects
		navalForce[0] = new Warship(); //using default constructor
		navalForce[1] = new Warship(199, 20200, 13, "INDIA" ); //using parametrized constructor
		navalForce[2] = new Warship(201, 22000, 15, "RUSSIA" ); //using parametrized constructor
		navalForce[3] = new Destroyer(300, 22300, 12, "France", AttackType.ANTIAIRCRAFT, 5,5); //using parametrized constructor
		navalForce[4] = new Submarine(150, 21000, 13, "India", PowerSource.NUCLEAR, 2, "Periscope"); //using parametrized constructor
		navalForce[5] = new Submarine(450, 27000, 19, "USA", PowerSource.NUCLEAR, 2, "Periscope"); //using parametrized constructor
		navalForce[6] = new Submarine(350, 21000, 12, "UK", PowerSource.NUCLEAR, 2, "Periscope"); //using parametrized constructor
		navalForce[7] = new AircraftCarrier(200, 19000,14, "USA", 4, 2 ); //using parametrized constructor
		navalForce[8] = new AmphibiousAssaultShip(210,19500,14,"UAE",3,3,5); //using parametrized constructor
		navalForce[9] = new SuperCarrier(220,18300,17,"Sweden",2,2,5); //using parametrzed constructor
	
		//let's create a var to save our index of the lowest priced warship
		int LowestPricedIndex = -1;
		double lowestPrice = 0;
		for (int i=0; i < navalForce.length; i++){
			if (lowestPrice == 0 || lowestPrice >= navalForce[i].getPrice())
			{
				lowestPrice = navalForce[i].getPrice();
				LowestPricedIndex = i;
			}
		}
		
		System.out.println();
		System.out.println("Eureka! We found the lowest priced Warship!");
		System.out.println("Here are the details as provided: ");
		System.out.println(navalForce[LowestPricedIndex]);
		System.out.println("It was stored at the Index Location number " + LowestPricedIndex + " in our array " +
				"of Warship Objects");
		
		// exit message
		System.out.println();
		System.out.println("*********************************************");
		System.out.println("*   Thanks for using to the Warshp Driver   *");
		System.out.println("*********************************************");
		
		
		} //end main
	
		

} //end class
