// ------------------------------------------------------------------
// Name and ID: Amit Malhotra (5796997)
// Comp 249
// Assignment #2
// Due Date: 1st of March 2013
//--------------------------------------------------------------------

package carriers;  //part of the carriers package

import java.text.NumberFormat;

import warships.Warship;
//importing warship package 

/**
 * This class extends the Warship class and defines a new type of Warships called the Aircraft Carriers.  
 * These warships are built to deploy and recover aircrafts. 
 *
 */
public class AircraftCarrier extends Warship {
	
	//attributes of the Aircraft Carrier
	private int nbFighterPlanes, nbHelicopters; //the number of fighter planes and the helicopters it can carry

	/**
	 * The parametrized constructor for the AircraftCarrier class.  The constructor makes a call
	 * to the parametrized constructor defined in the Warship class. 
	 * 
	 * @param price		of the type double, indicating the Price of the AircraftCarrier (in Dollars).
	 * @param weight		of the type float, indicating the Weight of the AircraftCarrier (in Tons). 
	 * @param speed		of the type integer, indicating the speed of the AircraftCarrier (in Knots).
	 * @param country		of the type String, indicating the country where the carrier was built.
	 * @param fighterPlanes	of the type integer, indicating the number of fighter planes the carrier can support.
	 * @param helicopters		of the type integer, indicating the number of helicopters the carrier can support.
	 */
	public AircraftCarrier(double price, float weight, int speed, String country, int fighterPlanes, int helicopters) {
		super(price, weight, speed, country); //call to super constructor of the Warship class
		this.nbFighterPlanes = fighterPlanes;
		this.nbHelicopters = helicopters;
	}

	/**
	 * The default constructor for the AircraftCarrier class.  Makes a call to the default constructor
	 * defined in the Warship class. 
	 */
	public AircraftCarrier() {
		super(); //call to super constructor of the Warship class
		this.nbFighterPlanes = 5;
		this.nbHelicopters = 5;
		
	}
	
	//accessor methods
	/**
	 * This method allows one to access the Number of Fighter planes the carrier houses
	 * @return  number of fighter planes of the type int
	 */
	public int getNbOfFighterPlanes(){
		return this.nbFighterPlanes;
	}
	
	/**
	 * This method allows one to access the Number of Helicopter the carrier can carry
	 * @return  number of helicopters of the type int
	 */
	public int getNbOfHelicopters(){
		return this.nbHelicopters;
	}
	
	//mutator methods
	/**
	 * This method allows one to modify the number of fighter planes the carrier can carry
	 * @param fighterPlanes (type integer)
	 */
	public void setNbOfFighterPlanes(int fighterPlanes){
		this.nbFighterPlanes = fighterPlanes;
	}
	
	/**
	 * This method allows one to modify the number of Helicopters the carrier can carry
	 * @param helicopters (type integer)
	 */
	public void setNbOfHelicopters(int helicopters){
		this.nbHelicopters = helicopters;
	}

	/**
	 * This method outputs all the attributes of the carrier
	 */
	@Override
	public String toString(){
		Class myClass = this.getClass(); //let's save the class type of the object
		String s = myClass.getName(); //the getName method lets us get the name of the class
		s = s.substring(9); //removing the package name from front
		
		String w = NumberFormat.getIntegerInstance().format(this.getWeight()); //format weight correctly
		String p = NumberFormat.getIntegerInstance().format(this.getPrice()); //format price correctly
		
		return ("This " + s + " costs $" + this.getPrice() + " M and weighs " + this.getWeight() + 
				" tons and travels at a speed of " + this.getSpeed() + " knots. It belongs to " 
				+ this.getCountry() + ". It can accomodate " + this.nbFighterPlanes + " Fighter Planes" +
						" and " + this.nbHelicopters + " Helicopters." );
	}


	/**
	 * The Equals method overrides the equals method defined in the parent warship class. It checks 
	 * if the passing object exists, uses the parent's equal method to test the equality of the
	 * inherited attributes, tests the equality of the class and then verifies all of the objects
	 * own attributes for equality.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) { //is the object calling same as the one being passed?
			return true;
		}
		if (!super.equals(obj)) { //check our inherited attrbutes for equality
			return false;
		}
		if (getClass() != obj.getClass()) { //do we have the same types?
			return false;
		}
		AircraftCarrier other = (AircraftCarrier) obj; //type cast as AircraftCarrier
		//and compare the attributes
		if (nbFighterPlanes != other.nbFighterPlanes) {
			return false;
		}
		if (nbHelicopters != other.nbHelicopters) {
			return false;
		}
		//were they similar finally?
		return true;
	}
}
	